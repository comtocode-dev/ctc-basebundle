# Com to Code BaseBundle

ctc-basebundle is a compilation of every "basic" feature the company use on its eZ Platform's project.
This bundle is fully compatible for v2.x and *almost* fully compatible for v1.x

## Features

* Basic database to use with pre-filled contentTypes and some example pages
    * Content
        * Contact form
        * Content
        * Folder
        * Homepage
        * Landing Page
    * Media
        * File
        * Image
        * Call-to action link
        * Youtube video
    * Project
        * Global configuration
    * Languages
        * English (eng-GB)
        * French (fre-FR)
* Global configuration from the Back-Office inserted into Front-Office's content rendering
* Sass compilation from twig template on the fly via the `renderSass` twig helper
* Twig helpers:
    * isBoolean : return `true` if the parameters is a boolean
    * typeOf : return the type of the object
    * jsonDecode : php json_decode() call sent into twig
    * getChildren : return an `array` containing contents under the location sent in parameter
    * getChildrenCount : return itemns count under the location sent in parameter
    * getLanguageName : get current item language name
* eZ Platform's ezRichText custom tags (not supported on v1.x)
    * youtube
    * slideshare
* Custom shell commands in order to update local files and database based on preprod's ones.

## Installation

Make sure you already installed an eZ Platform project in your folder.
Then, update `composer.json` by adding the projcet repository:

```
"repositories": [
    {
        "type": "git",
        "url": "git@gitlab.com:comtocode-dev/ctc-basebundle.git"
    },
]
```

You will need to run the following command `composer require comtocode/ctc-basebundle`

Once the installation is complete, just activate the bundle in your `AppKernel.php`:

```
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
        ...
        new CTC\BaseBundle\CTCBaseBundle(),
```

If order to enable custom templates, go to your `app/config/config.yml` and add the following config to the existed `assetic` and `twig`:

```
assetic:
    ...
    bundles:
        ...
        - CTCBaseBundle
```

## Commands

If you want to use the update command, update it with your local values first.
Make sure you installed `expect` : `sudo apt-get install expect`
Then, please run `bash vendor/comtocode/ctc-basebundle/Resources/bin/(eZ|wp)/update_env.sh 0|1|2|-s=/path/to/file` in your project

* 0 : all commands
* 1 : getting database
* 2 : getting medias
* --source (-s): loading customized parameters
* --help (-h): display script description