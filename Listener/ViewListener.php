<?php

namespace CTC\BaseBundle\Listener;

use eZ\Publish\API\Repository\Repository;
use eZ\Publish\Core\MVC\Symfony\Event\PreContentViewEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class ViewListener
 * @package CTC\BaseBundle\Listener
 */
class ViewListener
{
    /**
     * @var \eZ\Publish\API\Repository\Repository
     */
    protected $repository;
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ViewListener constructor.
     * Constructs our listener and loads it with access to the eZ Publish repository and config
     *
     * @param Repository $repository
     * @param ContainerInterface $container
     * @param LoggerInterface $logger
     */
    public function __construct(Repository $repository, ContainerInterface $container, LoggerInterface $logger)
    {
        //Add these to the class so we have them when the event method is triggered
        $this->repository = $repository;
        $this->container = $container;
        $this->logger = $logger;
    }

    /**
     * @param PreContentViewEvent $event
     *
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException
     */
    public function onPreContentView(PreContentViewEvent $event)
    {
        try {
            // Get global configuration content 
            $globalConfigContentId = $this->container->getParameter("ctc_base.global_configuration_content_id");
            if (!$globalConfigContentId) {
                $globalConfigLocationId = $this->container->getParameter("ctc_base.global_configuration_location_id");
                $globalConfigLocation = $this->repository->getLocationService()->loadLocation($globalConfigLocationId);
                $globalConfigContentId = $globalConfigLocation->contentInfo->id;
            }
            $globalConfigContent = $this->repository->getContentService()->loadContent($globalConfigContentId);
            // Loading content into each views
            $contentView = $event->getContentView();
            $contentView->addParameters(array('globalConfigContent' => $globalConfigContent));
        } catch (\InvalidArgumentException $e) {
            $this->logger->error($e->getMessage());
        } catch (\eZ\Publish\API\Repository\Exceptions\NotFoundException $e) {
            $this->logger->error($e->getMessage());
        }
    }
}