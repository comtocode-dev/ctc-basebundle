<?php

namespace CTC\BaseBundle\Twig;

use eZ\Publish\API\Repository\Repository;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use Symfony\Component\Intl\Intl;

/**
 * Class CTCFunctions
 * @package CTC\BaseBundle\Twig
 */
class CTCFunctions extends \Twig_Extension
{
    /**
     * @var null|Container
     */
    private $container;
    /**
     * @var Repository
     */
    protected $repository;

    /**
     * CTCFunctions constructor.
     *
     * @param Container $container
     * @param Repository $repository
     */
    public function __construct(Container $container, Repository $repository)
    {
        $this->container = $container;
        $this->repository = $repository;
    }

    /**
     * @param $var
     *
     * @return bool
     */
    public function isBoolean($var)
    {
        return is_bool($var);
    }

    /**
     * @param $var
     * @param null $type_test
     *
     * @return bool
     */
    public function typeOf($var, $type_test = null)
    {
        switch ($type_test) {
            case 'array':
                return is_array($var);
                break;
            case 'bool':
                return is_bool($var);
                break;
            case 'float':
                return is_float($var);
                break;
            case 'int':
                return is_int($var);
                break;
            case 'numeric':
                return is_numeric($var);
                break;
            case 'object':
                return is_object($var);
                break;
            case 'scalar':
                return is_scalar($var);
                break;
            case 'string':
                return is_string($var);
                break;
            case 'datetime':
                return ($var instanceof \DateTime);
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * Getting content children of a dedicated $locationId
     *
     * @param $locationId
     * @param $offset
     * @param $pageSize
     *
     * @return array
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException
     */
    public function getChildren($locationId, $offset = 0, $pageSize = 0)
    {
        $location = $this->repository->getLocationService()->loadLocation($locationId);
        $childrenCount = $this->repository->getLocationService()->getLocationChildCount($location);
        $childrenList = array();
        if ($childrenCount > 0) {
            $filterArray = array(
                new Criterion\ParentLocationId($locationId),
                new Criterion\Visibility(Criterion\Visibility::VISIBLE),
            );
            // Creating search request for subitems
            $query = new LocationQuery();
            $query->filter = new Criterion\LogicalAnd($filterArray);
            $query->limit = $pageSize == 0 ? $childrenCount : $pageSize;
            $query->offset = $offset;
            // Sort Clause from Back Office
            $query->sortClauses = $location->getSortClauses();
            $childrenLocationList = $this->repository->getSearchService()->findLocations($query);
            foreach ($childrenLocationList->searchHits as $locationItem) {
                if (!$locationItem->valueObject->hidden && !$locationItem->valueObject->invisible) {
                    $childrenList[] = $this->repository->getContentService()->loadContent($locationItem->valueObject->contentId);
                }
            }
        }

        return $childrenList;
    }

    /**
     * @param $locationId
     *
     * @return int
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException
     */
    public function getChildrenCount($locationId)
    {
        // Creating search request for subitems
        $query = new LocationQuery();
        $query->filter = new Criterion\LogicalAnd(array(
            new Criterion\ParentLocationId($locationId),
            new Criterion\Visibility(Criterion\Visibility::VISIBLE),
        ));
        // Prevent fetching any results, just getting count
        $query->limit = 0;

        return $this->repository->getSearchService()->findLocations($query)->totalCount;
    }

    /**
     * @param $encodedVal
     *
     * @return mixed
     */
    public function jsonDecode($encodedVal)
    {
        return json_decode($encodedVal);
    }

    /**
     * @param $langueCode
     *
     * @return null|string
     */
    public function getLanguageName($langueCode)
    {
        return Intl::getLanguageBundle()->getLanguageName($langueCode);
    }

    /**
     * @return array|\Twig_SimpleFilter[]
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('checkBoolean', array($this, 'isBoolean')),
            new \Twig_SimpleFilter('typeOf', array($this, 'typeOf')),
            new \Twig_SimpleFunction('json_decode', array($this, 'jsonDecode'))
        );
    }

    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('getChildren', array($this, 'getChildren')),
            new \Twig_SimpleFunction('getChildrenCount', array($this, 'getChildrenCount')),
            new \Twig_SimpleFunction('languageName', array($this, 'getLanguageName'))
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'CTCFunctions';
    }
}