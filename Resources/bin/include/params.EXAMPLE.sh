#!/bin/bash

#########################################################################
##     File used in order to update params based on current project    ##
#########################################################################

# Global values
separator=_                             # please replace if needed
timestamp=$(date +"%Y-%m-%d_%H-%M-%S")  # please replace if needed
project_type=ez_wp                      # please replace
new_server_struct=false                 # please replace if needed

# Local values
project_folder=PROJECT_FOLDER           # please replace
path=/var/www/$project_folder           # please replace if needed
mysql_user=MYSQL_USER                   # please replace
mysql_passwd=MYSQL_PASSWORD             # please replace if needed
mysql_db=$project_folder                # please replace if needed
project_domain=PROJECT_DOMAIN           # please replace

# Dist values
dist_server=DIST_SERVER                 # please replace
dist_user=DIST_USER                     # please replace
dist_passwd=DIST_PASSWORD               # please replace
dist_project=$project_folder            # please replace if needed
dist_path=/var/www/$dist_project        # please replace if needed
dist_mysql_host=DIST_MYSQL_HOST         # please replace
dist_mysql_port=DIST_MYSQL_HOST         # please replace
dist_mysql_db=DIST_MYSQL_DB             # please replace
dist_mysql_user=DIST_MYSQL_USER         # please replace
dist_mysql_passwd=DIST_MYSQL_PASSWORD   # please replace
dist_project_domain=DIST_PROJECT_DOMAIN # please replace
