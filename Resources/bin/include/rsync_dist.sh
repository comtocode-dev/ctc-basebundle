#!/usr/bin/expect -f

#########################################################################
## File used in getAssets command, can be found in update_env.sh       ##
## Allow user to get specific 'var' folder content from distant server ##
#########################################################################

# Getting variables
set project_folder [lrange $argv 0 0]
set project_path [lrange $argv 1 1]
set dist_server [lrange $argv 2 2]
set dist_path [lrange $argv 3 3]
set dist_passwd [lrange $argv 4 4]
set project_type [lrange $argv 5 5]
set dist_user [lrange $argv 6 6]
set timeout -1

if { "$project_type" == "wp" } {
    spawn rsync -chavzP $dist_user@$dist_server:$dist_path/wp-content/uploads $project_path/wp-content/
        expect "password:"
        send "$dist_passwd\r"
        expect eof
} else {
    spawn rsync -chavzP $dist_user@$dist_server:$dist_path/web/var/site $project_path/web/var/
        expect "password:"
        send "$dist_passwd\r"
        expect eof
}