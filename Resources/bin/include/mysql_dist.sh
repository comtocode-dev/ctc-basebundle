#!/usr/bin/expect -f

#########################################################################
## File used in getAssets command, can be found in update_env.sh       ##
## Allow user to get specific 'var' folder content from distant server ##
#########################################################################

# Getting variables
set dist_server  [lrange $argv 0 0]
set dist_user  [lrange $argv 1 1]
set dist_passwd  [lrange $argv 2 2]
set dist_mysql_port  [lrange $argv 3 3]
set dist_mysql_user  [lrange $argv 4 4]
set dist_mysql_passwd [lrange $argv 5 5]
set dist_mysql_host [lrange $argv 6 6]
set dist_mysql_db [lrange $argv 7 7]
set separator _
set timestamp [lrange $argv 8 8]
set dist_path [lrange $argv 9 9]
set timeout -1
set new_server_struct [lrange $argv 10 10]

if { "$new_server_struct" == "true" } {
    spawn ssh $dist_user@$dist_server
        expect "password:"
        send "$dist_passwd\r"
        expect "$ "
        send "mysqldump -u$dist_mysql_user -p$dist_mysql_passwd $dist_mysql_db -h$dist_mysql_host > /tmp/$dist_user$separator$timestamp.sql\r"
        send "exit\r"
        expect eof

    spawn scp $dist_user@$dist_server:/tmp/$dist_user$separator$timestamp.sql .
        expect "password:"
        send "$dist_passwd\r"
        expect eof
} else {
    spawn ssh $dist_user@$dist_server
        expect "password:"
        send "$dist_passwd\r"
        expect "$ "
        send "docker exec $dist_mysql_host /usr/bin/mysqldump -u$dist_mysql_user -p$dist_mysql_passwd $dist_mysql_db > $dist_user$separator$timestamp.sql\r"
        send "exit\r"
        expect eof

    spawn scp $dist_user@$dist_server:$dist_path/$dist_user$separator$timestamp.sql .
        expect "password:"
        send "$dist_passwd\r"
        expect eof
}