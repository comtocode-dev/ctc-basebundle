#!/bin/bash

#########################################################################
## File used in order to update local dev env based on preprod's datas ##
#########################################################################

# Global values
src_params=false
COMMAND_EZ=false
separator=_                             # please replace if needed
timestamp=$(date +"%Y-%m-%d_%H-%M-%S")  # please replace if needed
project_type=ez_wp                      # please replace
new_server_struct=false                 # please replace if needed

# Local values
project_folder=PROJECT_FOLDER           # please replace
path=/var/www/$project_folder           # please replace if needed
mysql_user=MYSQL_USER                   # please replace
mysql_passwd=MYSQL_PASSWORD             # please replace if needed
mysql_db=$project_folder                # please replace if needed
project_domain=PROJECT_DOMAIN           # please replace

# Dist values
dist_server=DIST_SERVER                 # please replace
dist_user=DIST_USER                     # please replace
dist_passwd=DIST_PASSWORD               # please replace
dist_project=$project_folder            # please replace if needed
dist_path=/var/www/$dist_project        # please replace if needed
dist_mysql_host=DIST_MYSQL_HOST         # please replace
dist_mysql_port=DIST_MYSQL_HOST         # please replace
dist_mysql_db=DIST_MYSQL_DB             # please replace
dist_mysql_user=DIST_MYSQL_USER         # please replace
dist_mysql_passwd=DIST_MYSQL_PASSWORD   # please replace
dist_project_domain=DIST_PROJECT_DOMAIN # please replace

##
## Display script header
##
function showHeader
{
    echo -e "\e[33m|||||||||||||||||||||||||||||\e[0m"
    echo -e "\e[33m||                         ||\e[0m"
    echo -e "\e[33m||   Initialisation Env    ||\e[0m"
    echo -e "\e[33m||                         ||\e[0m"
    echo -e "\e[33m|||||||||||||||||||||||||||||\e[0m\n\r"
}

##
## Explain what commands do
##
function helpCommands
{
    echo -e "Cette commande est utilisée pour mettre à jour les ressources d'un projet"
    echo -e " "
    echo -e "Avec celle-ci, il est possible de récupérer les différents assets et"
    echo -e "dump de base de données depuis un environnement donnée"
    echo -e " "
    echo -e "Quelle commande souhaitez-vous executer ?\n\r"
    echo -e "  \e[32m-0-\e[0m Toutes les commandes"
    echo -e "  \e[32m-1-\e[0m Base de données"
    echo -e "  \e[32m-2-\e[0m Assets"
    echo -e " "
    echo -e "Il est nécessaire de renseigner les paramètres utilisés dans ce script"
    echo -e "Pour cela, basez-vous sur '\e[33minclude/params.EXAMPLE.sh\e[0m'"
    echo -e "puis exécutez '\e[33mbash Resources/bin/eZ/update_env.sh -s=/path/to/file/params.sh\e[0m'"
    echo -e " "
}

##
## Moving script path execution on the desired path
##
function initiatePosition
{
    cd $path
    echo -e "\e[33mPositionnement dans le repertoire \e[32m$path\e[0m"
}

##
## Getting dist db and updating local with it
##
function getDatabase
{
    echo -e "\e[33mRécupération de la base de donnée dans \e[32m$path\e[0m"
    expect -f vendor/comtocode/ctc-basebundle/Resources/bin/include/mysql_dist.sh $dist_server $dist_user $dist_passwd $dist_mysql_port $dist_mysql_user $dist_mysql_passwd $dist_mysql_host $dist_mysql_db $timestamp $dist_path $new_server_struct
    if [ "$project_type" == "wp" ]
    then
        # Editing domain in sql file
        sed -i -e "s|\$dist_project_domain|\$project_domain|g" $dist_user$separator$timestamp.sql
    fi
    mysql -u$mysql_user -p$mysql_passwd $mysql_db < $dist_user$separator$timestamp.sql
    rm $dist_user$separator$timestamp.sql
    echo -e "\e[33m-- Database --    \e[32mOK\e[0m"
}

##
## Project assets synchronization
##
function getAssets
{
    echo -e "\e[33mRécupération des assets dans \e[32m$path\e[0m"
    expect -f vendor/comtocode/ctc-basebundle/Resources/bin/include/rsync_dist.sh $project_folder $path $dist_server $dist_path $dist_passwd $project_type $dist_user
    echo -e "\e[33m-- Assets --    \e[32mOK\e[0m"
}

##
## Global launcher
##
function allCommands
{
    getDatabase
    getAssets
}

##
## Select the desired command
##
function commandSelector
{
    initiatePosition
    echo -e " "
    if [ "$1" -eq "0" ]
    then
        allCommands
    elif [ "$1" -eq "1" ]
    then
        getDatabase
    elif [ "$1" -eq "2" ]
    then
        getAssets
    else
        echo -e "\e[31mVeuillez choisir une option des options disponibles.\e[0m"
    fi
}

##
## Basic script execution, will call needed function in order to properly run
##
START=$(date +%s)
showHeader

# Getting params
for i in "$@"
do
    case $i in
        -s=*|--source=*)
            src_params="${i#*=}"
            shift # past argument=value
            ;;
        0)
            COMMAND_EZ=0
        ;;
        1)
            COMMAND_EZ=1
        ;;
        2)
            COMMAND_EZ=2
        ;;
        *)
            helpCommands
            exit 1
        ;;
    esac
done

# Loading params from input
if ! [ "$src_params" == false ];
then
  source $src_params
else
    echo -e "Veuillez renseigner les paramètres de votre projet via l'option '--source|-s'"
    exit 1
fi

if [ "$COMMAND_EZ" == false  ]
then
    helpCommands
    echo -e "Veuillez choisir une de ces possibilites puis [Entree]: \c"
    read COMMAND_EZ
    commandSelector $COMMAND_EZ
else
    commandSelector $1
fi
END=$(date +%s)
echo -e "Execution time : $((END-START)) sec."

if [ "$COMMAND_EZ" -eq "0" ]
then
    echo -e " "
    echo -e "\e[33mInitialisation des sources \e[32m$path\e[0m"
    bash bin/init_command.sh 0
fi