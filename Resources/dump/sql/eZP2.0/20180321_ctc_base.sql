-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 10.0.0.2    Database: delice_network
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ezapprove_items`
--

DROP TABLE IF EXISTS `ezapprove_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezapprove_items` (
  `collaboration_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_process_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezapprove_items`
--

LOCK TABLES `ezapprove_items` WRITE;
/*!40000 ALTER TABLE `ezapprove_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezapprove_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezbasket`
--

DROP TABLE IF EXISTS `ezbasket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezbasket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `productcollection_id` int(11) NOT NULL DEFAULT '0',
  `session_id` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ezbasket_session_id` (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezbasket`
--

LOCK TABLES `ezbasket` WRITE;
/*!40000 ALTER TABLE `ezbasket` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezbasket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezbinaryfile`
--

DROP TABLE IF EXISTS `ezbinaryfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezbinaryfile` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT '0',
  `download_count` int(11) NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL DEFAULT '',
  `mime_type` varchar(255) NOT NULL DEFAULT '',
  `original_filename` varchar(255) NOT NULL DEFAULT '',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contentobject_attribute_id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezbinaryfile`
--

LOCK TABLES `ezbinaryfile` WRITE;
/*!40000 ALTER TABLE `ezbinaryfile` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezbinaryfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcobj_state`
--

DROP TABLE IF EXISTS `ezcobj_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcobj_state` (
  `default_language_id` bigint(20) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(45) NOT NULL DEFAULT '',
  `language_mask` bigint(20) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ezcobj_state_identifier` (`group_id`,`identifier`),
  KEY `ezcobj_state_lmask` (`language_mask`),
  KEY `ezcobj_state_priority` (`priority`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcobj_state`
--

LOCK TABLES `ezcobj_state` WRITE;
/*!40000 ALTER TABLE `ezcobj_state` DISABLE KEYS */;
INSERT INTO `ezcobj_state` VALUES (2,2,1,'not_locked',3,0),(2,2,2,'locked',3,1);
/*!40000 ALTER TABLE `ezcobj_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcobj_state_group`
--

DROP TABLE IF EXISTS `ezcobj_state_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcobj_state_group` (
  `default_language_id` bigint(20) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(45) NOT NULL DEFAULT '',
  `language_mask` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ezcobj_state_group_identifier` (`identifier`),
  KEY `ezcobj_state_group_lmask` (`language_mask`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcobj_state_group`
--

LOCK TABLES `ezcobj_state_group` WRITE;
/*!40000 ALTER TABLE `ezcobj_state_group` DISABLE KEYS */;
INSERT INTO `ezcobj_state_group` VALUES (2,2,'ez_lock',3);
/*!40000 ALTER TABLE `ezcobj_state_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcobj_state_group_language`
--

DROP TABLE IF EXISTS `ezcobj_state_group_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcobj_state_group_language` (
  `contentobject_state_group_id` int(11) NOT NULL DEFAULT '0',
  `description` longtext NOT NULL,
  `language_id` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(45) NOT NULL DEFAULT '',
  `real_language_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contentobject_state_group_id`,`real_language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcobj_state_group_language`
--

LOCK TABLES `ezcobj_state_group_language` WRITE;
/*!40000 ALTER TABLE `ezcobj_state_group_language` DISABLE KEYS */;
INSERT INTO `ezcobj_state_group_language` VALUES (2,'',3,'Lock',2);
/*!40000 ALTER TABLE `ezcobj_state_group_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcobj_state_language`
--

DROP TABLE IF EXISTS `ezcobj_state_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcobj_state_language` (
  `contentobject_state_id` int(11) NOT NULL DEFAULT '0',
  `description` longtext NOT NULL,
  `language_id` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`contentobject_state_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcobj_state_language`
--

LOCK TABLES `ezcobj_state_language` WRITE;
/*!40000 ALTER TABLE `ezcobj_state_language` DISABLE KEYS */;
INSERT INTO `ezcobj_state_language` VALUES (1,'',3,'Not locked'),(2,'',3,'Locked');
/*!40000 ALTER TABLE `ezcobj_state_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcobj_state_link`
--

DROP TABLE IF EXISTS `ezcobj_state_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcobj_state_link` (
  `contentobject_id` int(11) NOT NULL DEFAULT '0',
  `contentobject_state_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contentobject_id`,`contentobject_state_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcobj_state_link`
--

LOCK TABLES `ezcobj_state_link` WRITE;
/*!40000 ALTER TABLE `ezcobj_state_link` DISABLE KEYS */;
INSERT INTO `ezcobj_state_link` VALUES (1,1),(4,1),(10,1),(11,1),(12,1),(13,1),(14,1),(41,1),(42,1),(45,1),(49,1),(50,1),(51,1),(52,1),(53,1),(54,1),(55,1),(56,1),(57,1),(58,1),(59,1),(60,1),(61,1),(62,1),(63,1),(64,1);
/*!40000 ALTER TABLE `ezcobj_state_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcollab_group`
--

DROP TABLE IF EXISTS `ezcollab_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcollab_group` (
  `created` int(11) NOT NULL DEFAULT '0',
  `depth` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_open` int(11) NOT NULL DEFAULT '1',
  `modified` int(11) NOT NULL DEFAULT '0',
  `parent_group_id` int(11) NOT NULL DEFAULT '0',
  `path_string` varchar(255) NOT NULL DEFAULT '',
  `priority` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezcollab_group_depth` (`depth`),
  KEY `ezcollab_group_path` (`path_string`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcollab_group`
--

LOCK TABLES `ezcollab_group` WRITE;
/*!40000 ALTER TABLE `ezcollab_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcollab_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcollab_item`
--

DROP TABLE IF EXISTS `ezcollab_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcollab_item` (
  `created` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `data_float1` float NOT NULL DEFAULT '0',
  `data_float2` float NOT NULL DEFAULT '0',
  `data_float3` float NOT NULL DEFAULT '0',
  `data_int1` int(11) NOT NULL DEFAULT '0',
  `data_int2` int(11) NOT NULL DEFAULT '0',
  `data_int3` int(11) NOT NULL DEFAULT '0',
  `data_text1` longtext NOT NULL,
  `data_text2` longtext NOT NULL,
  `data_text3` longtext NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modified` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `type_identifier` varchar(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcollab_item`
--

LOCK TABLES `ezcollab_item` WRITE;
/*!40000 ALTER TABLE `ezcollab_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcollab_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcollab_item_group_link`
--

DROP TABLE IF EXISTS `ezcollab_item_group_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcollab_item_group_link` (
  `collaboration_id` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_read` int(11) NOT NULL DEFAULT '0',
  `last_read` int(11) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`collaboration_id`,`group_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcollab_item_group_link`
--

LOCK TABLES `ezcollab_item_group_link` WRITE;
/*!40000 ALTER TABLE `ezcollab_item_group_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcollab_item_group_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcollab_item_message_link`
--

DROP TABLE IF EXISTS `ezcollab_item_message_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcollab_item_message_link` (
  `collaboration_id` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL DEFAULT '0',
  `message_type` int(11) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  `participant_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcollab_item_message_link`
--

LOCK TABLES `ezcollab_item_message_link` WRITE;
/*!40000 ALTER TABLE `ezcollab_item_message_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcollab_item_message_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcollab_item_participant_link`
--

DROP TABLE IF EXISTS `ezcollab_item_participant_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcollab_item_participant_link` (
  `collaboration_id` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_read` int(11) NOT NULL DEFAULT '0',
  `last_read` int(11) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  `participant_id` int(11) NOT NULL DEFAULT '0',
  `participant_role` int(11) NOT NULL DEFAULT '1',
  `participant_type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`collaboration_id`,`participant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcollab_item_participant_link`
--

LOCK TABLES `ezcollab_item_participant_link` WRITE;
/*!40000 ALTER TABLE `ezcollab_item_participant_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcollab_item_participant_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcollab_item_status`
--

DROP TABLE IF EXISTS `ezcollab_item_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcollab_item_status` (
  `collaboration_id` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_read` int(11) NOT NULL DEFAULT '0',
  `last_read` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`collaboration_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcollab_item_status`
--

LOCK TABLES `ezcollab_item_status` WRITE;
/*!40000 ALTER TABLE `ezcollab_item_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcollab_item_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcollab_notification_rule`
--

DROP TABLE IF EXISTS `ezcollab_notification_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcollab_notification_rule` (
  `collab_identifier` varchar(255) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcollab_notification_rule`
--

LOCK TABLES `ezcollab_notification_rule` WRITE;
/*!40000 ALTER TABLE `ezcollab_notification_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcollab_notification_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcollab_profile`
--

DROP TABLE IF EXISTS `ezcollab_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcollab_profile` (
  `created` int(11) NOT NULL DEFAULT '0',
  `data_text1` longtext NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `main_group` int(11) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcollab_profile`
--

LOCK TABLES `ezcollab_profile` WRITE;
/*!40000 ALTER TABLE `ezcollab_profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcollab_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcollab_simple_message`
--

DROP TABLE IF EXISTS `ezcollab_simple_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcollab_simple_message` (
  `created` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `data_float1` float NOT NULL DEFAULT '0',
  `data_float2` float NOT NULL DEFAULT '0',
  `data_float3` float NOT NULL DEFAULT '0',
  `data_int1` int(11) NOT NULL DEFAULT '0',
  `data_int2` int(11) NOT NULL DEFAULT '0',
  `data_int3` int(11) NOT NULL DEFAULT '0',
  `data_text1` longtext NOT NULL,
  `data_text2` longtext NOT NULL,
  `data_text3` longtext NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_type` varchar(40) NOT NULL DEFAULT '',
  `modified` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcollab_simple_message`
--

LOCK TABLES `ezcollab_simple_message` WRITE;
/*!40000 ALTER TABLE `ezcollab_simple_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcollab_simple_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcomment`
--

DROP TABLE IF EXISTS `ezcomment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcomment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` bigint(20) NOT NULL,
  `created` int(11) NOT NULL,
  `modified` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `session_key` varchar(32) DEFAULT NULL,
  `ip` varchar(100) NOT NULL,
  `contentobject_id` int(11) NOT NULL,
  `parent_comment_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `email` varchar(75) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `text` longtext NOT NULL,
  `status` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_session_key_ip` (`user_id`,`session_key`,`ip`),
  KEY `content_parentcomment` (`contentobject_id`,`language_id`,`parent_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcomment`
--

LOCK TABLES `ezcomment` WRITE;
/*!40000 ALTER TABLE `ezcomment` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcomment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcomment_notification`
--

DROP TABLE IF EXISTS `ezcomment_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcomment_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contentobject_id` int(11) NOT NULL,
  `language_id` bigint(20) NOT NULL,
  `send_time` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `comment_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcomment_notification`
--

LOCK TABLES `ezcomment_notification` WRITE;
/*!40000 ALTER TABLE `ezcomment_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcomment_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcomment_subscriber`
--

DROP TABLE IF EXISTS `ezcomment_subscriber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcomment_subscriber` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `enabled` int(11) NOT NULL DEFAULT '1',
  `hash_string` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcomment_subscriber`
--

LOCK TABLES `ezcomment_subscriber` WRITE;
/*!40000 ALTER TABLE `ezcomment_subscriber` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcomment_subscriber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcomment_subscription`
--

DROP TABLE IF EXISTS `ezcomment_subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcomment_subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `subscription_type` varchar(30) NOT NULL,
  `content_id` int(11) NOT NULL,
  `language_id` bigint(20) NOT NULL DEFAULT '0',
  `subscription_time` int(11) NOT NULL,
  `enabled` int(11) NOT NULL DEFAULT '1',
  `hash_string` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcomment_subscription`
--

LOCK TABLES `ezcomment_subscription` WRITE;
/*!40000 ALTER TABLE `ezcomment_subscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcomment_subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontent_language`
--

DROP TABLE IF EXISTS `ezcontent_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontent_language` (
  `disabled` int(11) NOT NULL DEFAULT '0',
  `id` bigint(20) NOT NULL DEFAULT '0',
  `locale` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ezcontent_language_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontent_language`
--

LOCK TABLES `ezcontent_language` WRITE;
/*!40000 ALTER TABLE `ezcontent_language` DISABLE KEYS */;
INSERT INTO `ezcontent_language` VALUES (0,2,'eng-GB','English (United Kingdom)'),(0,4,'fre-FR','Français (France)');
/*!40000 ALTER TABLE `ezcontent_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentbrowsebookmark`
--

DROP TABLE IF EXISTS `ezcontentbrowsebookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentbrowsebookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `node_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezcontentbrowsebookmark_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentbrowsebookmark`
--

LOCK TABLES `ezcontentbrowsebookmark` WRITE;
/*!40000 ALTER TABLE `ezcontentbrowsebookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcontentbrowsebookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentbrowserecent`
--

DROP TABLE IF EXISTS `ezcontentbrowserecent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentbrowserecent` (
  `created` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `node_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezcontentbrowserecent_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentbrowserecent`
--

LOCK TABLES `ezcontentbrowserecent` WRITE;
/*!40000 ALTER TABLE `ezcontentbrowserecent` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcontentbrowserecent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentclass`
--

DROP TABLE IF EXISTS `ezcontentclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentclass` (
  `always_available` int(11) NOT NULL DEFAULT '0',
  `contentobject_name` varchar(255) DEFAULT NULL,
  `created` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(50) NOT NULL DEFAULT '',
  `initial_language_id` bigint(20) NOT NULL DEFAULT '0',
  `is_container` int(11) NOT NULL DEFAULT '0',
  `language_mask` bigint(20) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  `modifier_id` int(11) NOT NULL DEFAULT '0',
  `remote_id` varchar(100) NOT NULL DEFAULT '',
  `serialized_description_list` longtext,
  `serialized_name_list` longtext,
  `sort_field` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '1',
  `url_alias_name` varchar(255) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`version`),
  KEY `ezcontentclass_version` (`version`),
  KEY `ezcontentclass_identifier` (`identifier`,`version`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentclass`
--

LOCK TABLES `ezcontentclass` WRITE;
/*!40000 ALTER TABLE `ezcontentclass` DISABLE KEYS */;
INSERT INTO `ezcontentclass` VALUES (1,'<short_name|name>',1024392098,14,1,'folder',2,1,2,1448831672,14,'a3d405b81be900468eb153d774f4f0d2','a:0:{}','a:1:{s:6:\"eng-GB\";s:6:\"Folder\";}',1,1,NULL,0),(0,'<short_title|title>',1024392098,14,2,'article',2,1,3,1082454989,14,'c15b600eb9198b1924063b5a68758232',NULL,'a:2:{s:6:\"eng-GB\";s:7:\"Article\";s:16:\"always-available\";s:6:\"eng-GB\";}',1,1,NULL,0),(1,'<name>',1024392098,14,3,'user_group',2,1,3,1048494743,14,'25b4268cdcd01921b808a0d854b877ef',NULL,'a:2:{s:6:\"eng-GB\";s:10:\"User group\";s:16:\"always-available\";s:6:\"eng-GB\";}',1,1,NULL,0),(1,'<first_name> <last_name>',1024392098,14,4,'user',2,0,3,1082018364,14,'40faa822edc579b02c25f6bb7beec3ad',NULL,'a:2:{s:6:\"eng-GB\";s:4:\"User\";s:16:\"always-available\";s:6:\"eng-GB\";}',1,1,NULL,0),(1,'<name>',1031484992,8,5,'image',2,0,3,1048494784,14,'f6df12aa74e36230eb675f364fccd25a',NULL,'a:2:{s:6:\"eng-GB\";s:5:\"Image\";s:16:\"always-available\";s:6:\"eng-GB\";}',1,1,NULL,0),(1,'<name>',1052385472,14,12,'file',2,0,3,1052385669,14,'637d58bfddf164627bdfd265733280a0',NULL,'a:2:{s:6:\"eng-GB\";s:4:\"File\";s:16:\"always-available\";s:6:\"eng-GB\";}',1,1,NULL,0),(1,'<title>',1521560243,14,13,'global',2,0,2,1521633781,14,'5d2c38c4d4011b93e1d42f4be54ce581','a:1:{s:6:\"eng-GB\";s:55:\"Global configuration of the website (logos, colors ...)\";}','a:1:{s:6:\"eng-GB\";s:20:\"Global configuration\";}',2,1,'<title>',0),(1,'',1521562232,14,14,'__new__3ff6cbd3001986a6ed7f0f62f5f1e347',2,0,2,1521562232,14,'b19c7f3ac19d46b461d5aaee067b3085','a:0:{}','a:1:{s:6:\"eng-GB\";s:16:\"New Content Type\";}',2,0,'',1),(1,'<title>',1521562232,14,15,'homepage',2,1,2,1521633912,14,'0e93e6b4da08c82f6b9c3ee570b706e5','a:0:{}','a:1:{s:6:\"eng-GB\";s:8:\"Homepage\";}',2,0,'<title>',0),(1,'<title>',1521562764,14,16,'content',2,1,2,1521633464,14,'9639da2e3b8d77f737d6cc16b3ad01e0','a:0:{}','a:1:{s:6:\"eng-GB\";s:7:\"Content\";}',2,0,'<title>',0),(1,'<title>',1521563140,14,17,'landing_page',2,1,2,1521633889,14,'9b3e1ff3a2a06050565de853bd02036a','a:0:{}','a:1:{s:6:\"eng-GB\";s:12:\"Landing Page\";}',2,0,'<title>',0),(1,'<title>',1521563581,14,18,'contact_form',2,0,2,1521633365,14,'2d1ee0c46439fc124c70a713699425f5','a:0:{}','a:1:{s:6:\"eng-GB\";s:12:\"Contact form\";}',2,0,'<title>',0),(1,'',1521563882,14,19,'__new__87667a7f1465e597c9c02f3d0d0c5d44',2,0,2,1521563882,14,'9da68a01d86b2578afcc7de6ef5413ba','a:0:{}','a:1:{s:6:\"eng-GB\";s:16:\"New Content Type\";}',2,0,'',1),(1,'<title>',1521563959,14,20,'strip',2,0,2,1521637681,14,'1d02b3d8058ad17f78bae62def3f5637','a:0:{}','a:1:{s:6:\"eng-GB\";s:5:\"Strip\";}',2,0,'<title>',0),(1,'<title>',1521618511,14,21,'youtube',2,0,2,1521637873,14,'31d50cfc2d45415f4944147dcd5391c8','a:0:{}','a:1:{s:6:\"eng-GB\";s:13:\"Youtube video\";}',2,0,'<title>',0),(1,'<title>',1521618902,14,22,'link',2,0,2,1521636220,14,'97f43a0911b8743372919a4e7a6d41be','a:0:{}','a:1:{s:6:\"eng-GB\";s:19:\"Call-to-action link\";}',2,0,'<title>',0);
/*!40000 ALTER TABLE `ezcontentclass` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentclass_attribute`
--

DROP TABLE IF EXISTS `ezcontentclass_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentclass_attribute` (
  `can_translate` int(11) DEFAULT '1',
  `category` varchar(25) NOT NULL DEFAULT '',
  `contentclass_id` int(11) NOT NULL DEFAULT '0',
  `data_float1` double DEFAULT NULL,
  `data_float2` double DEFAULT NULL,
  `data_float3` double DEFAULT NULL,
  `data_float4` double DEFAULT NULL,
  `data_int1` int(11) DEFAULT NULL,
  `data_int2` int(11) DEFAULT NULL,
  `data_int3` int(11) DEFAULT NULL,
  `data_int4` int(11) DEFAULT NULL,
  `data_text1` varchar(50) DEFAULT NULL,
  `data_text2` varchar(50) DEFAULT NULL,
  `data_text3` varchar(50) DEFAULT NULL,
  `data_text4` varchar(255) DEFAULT NULL,
  `data_text5` longtext,
  `data_type_string` varchar(50) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(50) NOT NULL DEFAULT '',
  `is_information_collector` int(11) NOT NULL DEFAULT '0',
  `is_required` int(11) NOT NULL DEFAULT '0',
  `is_searchable` int(11) NOT NULL DEFAULT '0',
  `placement` int(11) NOT NULL DEFAULT '0',
  `serialized_data_text` longtext,
  `serialized_description_list` longtext,
  `serialized_name_list` longtext NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`version`),
  KEY `ezcontentclass_attr_ccid` (`contentclass_id`)
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentclass_attribute`
--

LOCK TABLES `ezcontentclass_attribute` WRITE;
/*!40000 ALTER TABLE `ezcontentclass_attribute` DISABLE KEYS */;
INSERT INTO `ezcontentclass_attribute` VALUES (1,'',2,0,0,0,0,255,0,0,0,'New article','','','','','ezstring',1,'title',0,1,1,1,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:5:\"Title\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(1,'',1,NULL,NULL,NULL,NULL,255,0,NULL,NULL,'Folder',NULL,NULL,NULL,NULL,'ezstring',4,'name',0,1,1,1,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:4:\"Name\";}',0),(1,'',3,0,0,0,0,255,0,0,0,'','','','',NULL,'ezstring',6,'name',0,1,1,1,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:4:\"Name\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(1,'',3,0,0,0,0,255,0,0,0,'','','','',NULL,'ezstring',7,'description',0,0,1,2,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:11:\"Description\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(1,'',4,0,0,0,0,255,0,0,0,'','','','','','ezstring',8,'first_name',0,1,1,1,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:10:\"First name\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(1,'',4,0,0,0,0,255,0,0,0,'','','','','','ezstring',9,'last_name',0,1,1,2,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:9:\"Last name\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(0,'',4,0,0,0,0,0,0,0,0,'','','','','','ezuser',12,'user_account',0,1,0,3,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:12:\"User account\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(1,'',5,0,0,0,0,150,0,0,0,'','','','',NULL,'ezstring',116,'name',0,1,1,1,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:4:\"Name\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(1,'',5,0,0,0,0,10,0,0,0,'','','','',NULL,'ezrichtext',117,'caption',0,0,1,2,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:7:\"Caption\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(1,'',5,0,0,0,0,10,0,0,0,'','','','',NULL,'ezimage',118,'image',0,0,0,3,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:5:\"Image\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(1,'',1,NULL,NULL,NULL,NULL,5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezrichtext',119,'short_description',0,0,1,3,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:17:\"Short description\";}',0),(1,'',2,0,0,0,0,10,0,0,0,'','','','','','ezrichtext',120,'intro',0,1,1,4,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:5:\"Intro\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(1,'',2,0,0,0,0,20,0,0,0,'','','','','','ezrichtext',121,'body',0,0,1,5,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:4:\"Body\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(0,'',2,0,0,0,0,0,0,0,0,'','','','','','ezboolean',123,'enable_comments',0,0,0,6,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:15:\"Enable comments\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(1,'',12,0,0,0,0,0,0,0,0,'New file','','','',NULL,'ezstring',146,'name',0,1,1,1,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:4:\"Name\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(1,'',12,0,0,0,0,10,0,0,0,'','','','',NULL,'ezrichtext',147,'description',0,0,1,2,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:11:\"Description\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(1,'',12,0,0,0,0,0,0,0,0,'','','','',NULL,'ezbinaryfile',148,'file',0,1,0,3,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:4:\"File\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(1,'',2,0,0,0,0,255,0,0,0,'','','','','','ezstring',152,'short_title',0,0,1,2,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:11:\"Short title\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(1,'',2,0,0,0,0,0,0,0,0,'','','','','','ezauthor',153,'author',0,0,0,3,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:6:\"Author\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(1,'',2,0,0,0,0,0,0,0,0,'','','','','','ezobjectrelation',154,'image',0,0,1,7,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:5:\"Image\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(1,'',1,NULL,NULL,NULL,NULL,100,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',155,'short_name',0,0,1,2,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:10:\"Short name\";}',0),(1,'',1,NULL,NULL,NULL,NULL,20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezrichtext',156,'description',0,0,1,4,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:11:\"Description\";}',0),(1,'',4,0,0,0,0,10,0,0,0,'','','','','','eztext',179,'signature',0,0,1,4,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:9:\"Signature\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(1,'',4,0,0,0,0,10,0,0,0,'','','','','','ezimage',180,'image',0,0,0,5,NULL,NULL,'a:2:{s:6:\"eng-GB\";s:5:\"Image\";s:16:\"always-available\";s:6:\"eng-GB\";}',0),(1,'content',13,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',181,'title',0,1,1,1,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:5:\"Title\";}',0),(1,'content',13,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezimage',182,'logo',0,1,0,2,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:4:\"Logo\";}',0),(1,'content',13,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezimage',183,'logo_footer',0,0,0,3,'N;','a:1:{s:6:\"eng-GB\";s:61:\"Si non renseigné, c\'est le logo principal qui sera utilisé.\";}','a:1:{s:6:\"eng-GB\";s:18:\"Logo of the footer\";}',0),(1,'options',13,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',184,'color_primary',0,0,1,4,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:13:\"Primary color\";}',0),(1,'options',13,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',185,'color_secondary',0,0,1,5,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:15:\"Secondary color\";}',0),(1,'options',13,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',186,'email_contact',0,0,1,6,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:14:\"Contact e-mail\";}',0),(1,'options',13,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',187,'message_old_browser',0,0,1,7,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:36:\"Warning message for the old browsers\";}',0),(1,'content',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><constraints><allowed-class contentclass-identifier=\"image\"/></constraints><type value=\"2\"/><object_class value=\"\"/><selection_type value=\"0\"/><contentobject-placement/><selection_limit value=\"0\"/></related-objects>\n','ezobjectrelationlist',188,'partners',0,0,1,8,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:33:\"Footer: The logos of the partners\";}',0),(1,'content',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezrichtext',189,'footer_address',0,1,1,9,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:15:\"Footer: Address\";}',0),(1,'options',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezrichtext',190,'footer_intro_social',0,0,1,10,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:34:\"Footer: Social medias introduction\";}',0),(1,'options',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezurl',191,'facebook_link',0,0,0,11,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:21:\"Footer: Facebook link\";}',0),(1,'options',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezurl',192,'twitter_link',0,0,0,12,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:20:\"Footer: Twitter link\";}',0),(1,'options',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezurl',193,'instagram_link',0,0,0,13,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:22:\"Footer: Instagram link\";}',0),(1,'content',15,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',195,'title',0,1,1,1,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:5:\"Title\";}',0),(1,'content',15,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',196,'subtitle',0,1,1,2,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:8:\"Subtitle\";}',0),(1,'content',15,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezimage',197,'image',0,0,0,3,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:7:\"Picture\";}',0),(1,'content',15,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezrichtext',198,'intro',0,0,1,4,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:12:\"Introduction\";}',0),(1,'content',15,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><constraints/><type value=\"2\"/><object_class value=\"\"/><selection_type value=\"0\"/><contentobject-placement/><selection_limit value=\"0\"/></related-objects>\n','ezobjectrelationlist',199,'strips',0,0,1,5,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:6:\"Strips\";}',0),(1,'meta',15,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',200,'meta_title',0,0,1,6,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:10:\"Meta title\";}',0),(1,'meta',15,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',201,'meta_description',0,0,1,7,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:16:\"Meta description\";}',0),(1,'content',16,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',202,'title',0,1,1,1,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:5:\"Title\";}',0),(1,'content',16,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',203,'subtitle',0,0,1,2,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:8:\"Subtitle\";}',0),(1,'content',16,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezimage',204,'image',0,0,0,3,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:7:\"Picture\";}',0),(1,'content',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezrichtext',205,'intro',0,0,1,4,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:12:\"Introduction\";}',0),(1,'content',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezrichtext',206,'content',0,0,1,5,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:12:\"Main content\";}',0),(1,'options',16,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezdate',207,'date',0,1,1,6,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:4:\"Date\";}',0),(1,'content',16,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezimage',208,'image_embed',0,0,0,7,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:13:\"Embed picture\";}',0),(1,'meta',16,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',209,'meta_title',0,0,1,8,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:10:\"Meta title\";}',0),(1,'meta',16,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',210,'meta_description',0,0,1,9,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:16:\"Meta description\";}',0),(1,'content',17,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',211,'title',0,1,1,1,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:5:\"Title\";}',0),(1,'content',17,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',212,'subtitle',0,0,1,2,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:8:\"Subtitle\";}',0),(1,'content',17,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezimage',213,'image',0,1,0,3,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:7:\"Picture\";}',0),(1,'content',17,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezrichtext',214,'desc',0,0,1,4,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:11:\"Description\";}',0),(1,'content',17,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><constraints/><type value=\"2\"/><object_class value=\"\"/><selection_type value=\"0\"/><contentobject-placement/><selection_limit value=\"0\"/></related-objects>\n','ezobjectrelationlist',215,'strips',0,0,1,5,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:6:\"Strips\";}',0),(1,'options',17,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'ezboolean',216,'display_children',0,0,1,6,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:29:\"Display the children elements\";}',0),(1,'meta',17,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',217,'meta_title',0,0,1,7,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:10:\"Meta title\";}',0),(1,'meta',17,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',218,'meta_description',0,0,1,8,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:16:\"Meta description\";}',0),(1,'content',18,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',219,'title',0,1,1,1,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:5:\"Title\";}',0),(1,'content',18,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezrichtext',220,'desc',0,0,1,2,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:11:\"Description\";}',0),(1,'content',18,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',221,'email',0,0,1,3,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:17:\"E-mail (receiver)\";}',0),(1,'content',18,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezrichtext',222,'confirm_text',0,0,1,4,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:17:\"Confirmation text\";}',0),(1,'meta',18,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',223,'meta_title',0,0,1,5,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:10:\"Meta title\";}',0),(1,'content',18,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',224,'meta_description',0,0,1,6,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:16:\"Meta description\";}',0),(1,'content',20,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',225,'title',0,1,1,1,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:5:\"Title\";}',0),(1,'options',20,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezselection><options><option id=\"0\" name=\"Titre de grande taille\"/><option id=\"1\" name=\"Titre de taille moyenne\"/><option id=\"2\" name=\"Titre de petite taille\"/></options></ezselection>\n','ezselection',226,'title_size',0,1,1,2,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:17:\"Size of the title\";}',0),(1,'content',20,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',227,'subtitle',0,0,1,3,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:8:\"Subtitle\";}',0),(1,'content',20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezrichtext',228,'intro',0,0,1,4,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:12:\"Introduction\";}',0),(1,'content',20,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezimage',229,'img_bg',0,0,0,5,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:18:\"Background picture\";}',0),(1,'content',20,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezselection><options><option id=\"0\" name=\"White\"/><option id=\"1\" name=\"Primary color\"/><option id=\"3\" name=\"Secondary color\"/></options></ezselection>\n','ezselection',230,'bg_color',0,0,1,6,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:16:\"Background color\";}',0),(1,'options',20,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezselection><options><option id=\"0\" name=\"Boxed\"/><option id=\"1\" name=\"Fluid\"/></options></ezselection>\n','ezselection',231,'style',0,1,1,7,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:5:\"Style\";}',0),(1,'options',20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><constraints/><type value=\"2\"/><object_class value=\"\"/><selection_type value=\"0\"/><contentobject-placement/><selection_limit value=\"0\"/></related-objects>\n','ezobjectrelationlist',233,'related_object',0,0,1,8,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:16:\"Object relations\";}',0),(1,'options',20,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><constraints><allowed-class contentclass-identifier=\"content\"/></constraints><selection_type value=\"0\"/><contentobject-placement/></related-objects>\n','ezobjectrelation',234,'internal_link',0,0,1,9,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:13:\"Internal link\";}',0),(1,'options',20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezurl',235,'external_link',0,0,0,10,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:13:\"External link\";}',0),(1,'options',20,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezselection><options><option id=\"0\" name=\"Simple link\"/><option id=\"1\" name=\"Button with primary color\"/><option id=\"2\" name=\"Button with secondary color\"/><option id=\"3\" name=\"Button with black color\"/><option id=\"4\" name=\"Button with white color\"/></options></ezselection>\n','ezselection',236,'type_link',0,0,1,11,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:12:\"Type of link\";}',0),(1,'options',20,NULL,NULL,NULL,NULL,10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'eztext',237,'html',0,0,1,13,'N;','a:1:{s:6:\"eng-GB\";s:60:\"Permet de charger du contenu HTML directement dans la bande.\";}','a:1:{s:6:\"eng-GB\";s:4:\"HTML\";}',0),(1,'options',20,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezselection><options><option id=\"0\" name=\"At the bottom of the content\"/><option id=\"1\" name=\"To the right of the title\"/></options></ezselection>\n','ezselection',238,'link_position',0,0,1,12,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:13:\"Link position\";}',0),(1,'options',20,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',239,'template',0,0,1,14,'N;','a:1:{s:6:\"eng-GB\";s:51:\"Permet de charger un template de bande particulier.\";}','a:1:{s:6:\"eng-GB\";s:8:\"Template\";}',0),(1,'options',20,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezselection><options><option id=\"0\" name=\"Simple (display title / subtitle / introduction / link)\"/><option id=\"1\" name=\"Inligne (display title on one column, centered introduction, link at the right side)\"/><option id=\"2\" name=\"Block in relation (title / subtitle / introduction / objet relation(s) / link)\"/><option id=\"3\" name=\"HTML (display only the html content)\"/><option id=\"4\" name=\"Template (Display only the template given in the field)\"/><option id=\"5\" name=\"Slider - 1 item at a time\"/><option id=\"6\" name=\"Slider - 5 items at a time\"/></options></ezselection>\n','ezselection',240,'block_type',0,0,1,15,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:13:\"Type of block\";}',0),(1,'content',21,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',241,'title',0,1,1,1,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:5:\"Title\";}',0),(1,'content',21,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',242,'id_video_youtube',0,1,1,2,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:23:\"ID of the Youtube video\";}',0),(1,'options',21,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'ezboolean',243,'full_width',0,0,1,3,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:16:\"Full-width video\";}',0),(1,'content',22,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',244,'title',0,1,1,1,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:5:\"Title\";}',0),(1,'content',22,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezstring',245,'label',0,0,1,2,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:5:\"Label\";}',0),(1,'content',22,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><constraints/><selection_type value=\"0\"/><contentobject-placement/></related-objects>\n','ezobjectrelation',246,'internal_link',0,0,1,3,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:13:\"Internal link\";}',0),(1,'content',22,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ezurl',247,'external_link',0,0,0,4,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:13:\"External link\";}',0),(1,'options',22,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezselection><options><option id=\"0\" name=\"Simple link\"/><option id=\"1\" name=\"Button with primary color\"/><option id=\"2\" name=\"Button with secondary color\"/><option id=\"3\" name=\"Button with black color\"/><option id=\"4\" name=\"Button with white color\"/></options></ezselection>\n','ezselection',248,'type_link',0,0,1,5,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:14:\"Type of button\";}',0),(1,'meta',16,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'ezboolean',249,'meta_noindex_nofollow',0,0,1,10,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:22:\"Meta noindex, nofollow\";}',0),(1,'meta',15,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'ezboolean',250,'meta_noindex_nofollow',0,0,1,8,'N;','a:0:{}','a:1:{s:6:\"eng-GB\";s:22:\"Meta noindex, nofollow\";}',0);
/*!40000 ALTER TABLE `ezcontentclass_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentclass_classgroup`
--

DROP TABLE IF EXISTS `ezcontentclass_classgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentclass_classgroup` (
  `contentclass_id` int(11) NOT NULL DEFAULT '0',
  `contentclass_version` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `group_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`contentclass_id`,`contentclass_version`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentclass_classgroup`
--

LOCK TABLES `ezcontentclass_classgroup` WRITE;
/*!40000 ALTER TABLE `ezcontentclass_classgroup` DISABLE KEYS */;
INSERT INTO `ezcontentclass_classgroup` VALUES (1,0,1,'Content'),(2,0,1,'Content'),(3,0,2,'Users'),(4,0,2,'Users'),(5,0,3,'Media'),(12,0,3,'Media'),(13,0,1,'Content'),(14,1,1,'Content'),(15,0,1,'Content'),(16,0,1,'Content'),(17,0,1,'Content'),(18,0,1,'Content'),(19,1,1,'Content'),(20,0,1,'Content'),(21,0,1,'Content'),(22,0,1,'Content');
/*!40000 ALTER TABLE `ezcontentclass_classgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentclass_name`
--

DROP TABLE IF EXISTS `ezcontentclass_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentclass_name` (
  `contentclass_id` int(11) NOT NULL DEFAULT '0',
  `contentclass_version` int(11) NOT NULL DEFAULT '0',
  `language_id` bigint(20) NOT NULL DEFAULT '0',
  `language_locale` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`contentclass_id`,`contentclass_version`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentclass_name`
--

LOCK TABLES `ezcontentclass_name` WRITE;
/*!40000 ALTER TABLE `ezcontentclass_name` DISABLE KEYS */;
INSERT INTO `ezcontentclass_name` VALUES (1,0,2,'eng-GB','Folder'),(2,0,3,'eng-GB','Article'),(3,0,3,'eng-GB','User group'),(4,0,3,'eng-GB','User'),(5,0,3,'eng-GB','Image'),(12,0,3,'eng-GB','File'),(13,0,2,'eng-GB','Global configuration'),(14,1,2,'eng-GB','New Content Type'),(15,0,2,'eng-GB','Homepage'),(16,0,2,'eng-GB','Content'),(17,0,2,'eng-GB','Landing Page'),(18,0,2,'eng-GB','Contact form'),(19,1,2,'eng-GB','New Content Type'),(20,0,2,'eng-GB','Strip'),(21,0,2,'eng-GB','Youtube video'),(22,0,2,'eng-GB','Call-to-action link');
/*!40000 ALTER TABLE `ezcontentclass_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentclassgroup`
--

DROP TABLE IF EXISTS `ezcontentclassgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentclassgroup` (
  `created` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modified` int(11) NOT NULL DEFAULT '0',
  `modifier_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentclassgroup`
--

LOCK TABLES `ezcontentclassgroup` WRITE;
/*!40000 ALTER TABLE `ezcontentclassgroup` DISABLE KEYS */;
INSERT INTO `ezcontentclassgroup` VALUES (1031216928,14,1,1033922106,14,'Content'),(1031216941,14,2,1033922113,14,'Users'),(1032009743,14,3,1033922120,14,'Media');
/*!40000 ALTER TABLE `ezcontentclassgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject`
--

DROP TABLE IF EXISTS `ezcontentobject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject` (
  `contentclass_id` int(11) NOT NULL DEFAULT '0',
  `current_version` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `initial_language_id` bigint(20) NOT NULL DEFAULT '0',
  `language_mask` bigint(20) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `published` int(11) NOT NULL DEFAULT '0',
  `remote_id` varchar(100) DEFAULT NULL,
  `section_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ezcontentobject_remote_id` (`remote_id`),
  KEY `ezcontentobject_classid` (`contentclass_id`),
  KEY `ezcontentobject_currentversion` (`current_version`),
  KEY `ezcontentobject_lmask` (`language_mask`),
  KEY `ezcontentobject_owner` (`owner_id`),
  KEY `ezcontentobject_pub` (`published`),
  KEY `ezcontentobject_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject`
--

LOCK TABLES `ezcontentobject` WRITE;
/*!40000 ALTER TABLE `ezcontentobject` DISABLE KEYS */;
INSERT INTO `ezcontentobject` VALUES (1,9,1,2,3,1448889046,'eZ Platform',14,1448889046,'9459d3c29e15006e45197295722c7ade',1,2),(3,1,4,2,3,1033917596,'Users',14,1033917596,'f5c88a2209584891056f987fd965b0ba',2,1),(4,2,10,2,3,1072180405,'Anonymous User',14,1033920665,'faaeb9be3bd98ed09f606fc16d144eca',2,1),(3,1,11,2,3,1033920746,'Guest accounts',14,1033920746,'5f7f0bdb3381d6a461d8c29ff53d908f',2,1),(3,1,12,2,3,1033920775,'Administrator users',14,1033920775,'9b47a45624b023b1a76c73b74d704acf',2,1),(3,1,13,2,3,1033920794,'Editors',14,1033920794,'3c160cca19fb135f83bd02d911f04db2',2,1),(4,3,14,2,3,1301062024,'Administrator User',14,1033920830,'1bb4fe25487f05527efa8bfd394cecc7',2,1),(1,1,41,2,3,1060695457,'Media',14,1060695457,'a6e35cbcb7cd6ae4b691f3eee30cd262',3,1),(3,1,42,2,3,1072180330,'Anonymous Users',14,1072180330,'15b256dbea2ae72418ff5facc999e8f9',2,1),(1,1,45,2,3,1079684190,'Setup',14,1079684190,'241d538ce310074e602f29f49e44e938',4,1),(1,1,49,2,3,1080220197,'Images',14,1080220197,'e7ff633c6b8e0fd3531e74c6e712bead',3,1),(1,1,50,2,3,1080220220,'Files',14,1080220220,'732a5acd01b51a6fe6eab448ad4138a9',3,1),(1,1,51,2,3,1080220233,'Multimedia',14,1080220233,'09082deb98662a104f325aaa8c4933d3',3,1),(16,7,52,2,2,1521638460,'Demo page',14,1521627149,'ad6a68b6b8f40db6af14712dbfe5da33',1,1),(13,1,53,2,2,1521628862,'Global configuration',14,1521628862,'a66d525bc7cc70496391f7477701b689',1,1),(15,1,54,2,2,1521629667,'Homepage',14,1521629667,'c3a19542d7796ee5d8434946ab5d1c68',1,1),(1,1,55,4,4,1521630073,'Vidéos Youtube',14,1521630073,'1f7e05c79db14c9eebdd00413afc9b1a',3,2),(21,3,56,2,2,1521632638,'Inside Com to code',14,1521630465,'f7b28d5dd79f6adc28e0a9bfc7840dbd',3,1),(1,1,57,2,2,1521632271,'Youtube videos',14,1521632271,'63a28095936cfe8c4019e1d93cee77da',3,1),(21,4,58,2,2,1521632717,'Inside Com to code - normal width',14,1521632658,'17671e297bbf58523687928c1166f681',3,1),(1,1,59,2,2,1521632950,'Call-to-action links',14,1521632950,'1c50a1d4be17f66c8a6bc9a1780c6998',3,1),(22,1,60,2,2,1521633090,'Simple link example',14,1521633090,'5bf9cf9245bd11f89a66d6a442a86f1d',3,1),(22,3,61,2,2,1521638082,'Primary color button example',14,1521633150,'4521e64dc96c23c3a52bc1f9c037ad91',3,1),(22,1,62,2,2,1521638152,'Secondary color button example',14,1521638152,'3b4608acf3004887a8c6a1c715f49143',3,1),(22,1,63,2,2,1521638250,'Black button example',14,1521638250,'c45ad8163489983f8566c582c841d7d8',3,1),(22,1,64,2,2,1521638293,'White button example',14,1521638293,'b5130f0d16bb03050de318321716424d',3,1);
/*!40000 ALTER TABLE `ezcontentobject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject_attribute`
--

DROP TABLE IF EXISTS `ezcontentobject_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject_attribute` (
  `attribute_original_id` int(11) DEFAULT '0',
  `contentclassattribute_id` int(11) NOT NULL DEFAULT '0',
  `contentobject_id` int(11) NOT NULL DEFAULT '0',
  `data_float` double DEFAULT NULL,
  `data_int` int(11) DEFAULT NULL,
  `data_text` longtext,
  `data_type_string` varchar(50) DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_code` varchar(20) NOT NULL DEFAULT '',
  `language_id` bigint(20) NOT NULL DEFAULT '0',
  `sort_key_int` int(11) NOT NULL DEFAULT '0',
  `sort_key_string` varchar(255) NOT NULL DEFAULT '',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`version`),
  KEY `ezcontentobject_attribute_co_id_ver_lang_code` (`contentobject_id`,`version`,`language_code`),
  KEY `ezcontentobject_attribute_language_code` (`language_code`),
  KEY `ezcontentobject_classattr_id` (`contentclassattribute_id`),
  KEY `sort_key_int` (`sort_key_int`),
  KEY `sort_key_string` (`sort_key_string`)
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject_attribute`
--

LOCK TABLES `ezcontentobject_attribute` WRITE;
/*!40000 ALTER TABLE `ezcontentobject_attribute` DISABLE KEYS */;
INSERT INTO `ezcontentobject_attribute` VALUES (0,4,1,NULL,NULL,'Welcome to eZ Platform','ezstring',1,'eng-GB',3,0,'welcome to ez platform',9),(0,119,1,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>This is the clean install coming with eZ Platform. It’s a barebone setup of the platform, an excellent foundation to build upon if you want to build from scratch. For more ready to go base designs or demo sites, please check the documentation.</para></section>\n','ezrichtext',2,'eng-GB',3,0,'',9),(0,7,4,NULL,NULL,'Main group','ezstring',7,'eng-GB',3,0,'',1),(0,6,4,NULL,NULL,'Users','ezstring',8,'eng-GB',3,0,'',1),(0,8,10,0,0,'Anonymous','ezstring',19,'eng-GB',3,0,'anonymous',2),(0,9,10,0,0,'User','ezstring',20,'eng-GB',3,0,'user',2),(0,12,10,0,0,'','ezuser',21,'eng-GB',3,0,'',2),(0,6,11,0,0,'Guest accounts','ezstring',22,'eng-GB',3,0,'',1),(0,7,11,0,0,'','ezstring',23,'eng-GB',3,0,'',1),(0,6,12,0,0,'Administrator users','ezstring',24,'eng-GB',3,0,'',1),(0,7,12,0,0,'','ezstring',25,'eng-GB',3,0,'',1),(0,6,13,0,0,'Editors','ezstring',26,'eng-GB',3,0,'',1),(0,7,13,0,0,'','ezstring',27,'eng-GB',3,0,'',1),(0,8,14,0,0,'Administrator','ezstring',28,'eng-GB',3,0,'administrator',3),(0,9,14,0,0,'User','ezstring',29,'eng-GB',3,0,'user',3),(30,12,14,0,0,'','ezuser',30,'eng-GB',3,0,'',3),(0,4,41,0,0,'Media','ezstring',98,'eng-GB',3,0,'',1),(0,119,41,0,1045487555,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext',99,'eng-GB',3,0,'',1),(0,6,42,0,0,'Anonymous Users','ezstring',100,'eng-GB',3,0,'anonymous users',1),(0,7,42,0,0,'User group for the anonymous user','ezstring',101,'eng-GB',3,0,'user group for the anonymous user',1),(0,155,1,NULL,NULL,'eZ Platform','ezstring',102,'eng-GB',3,0,'ez platform',9),(0,155,41,0,0,'','ezstring',103,'eng-GB',3,0,'',1),(0,156,1,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><title ezxhtml:level=\"2\">Welcome to eZ Platform</title><para>Congratulations, you just installed eZ Platform. eZ Platform is the new generation Content Management Platform from eZ Systems and the eZ Community replacing eZ Publish, our previous software.</para><title ezxhtml:level=\"3\">Need some guidance?</title><para>Check out the following resources:</para><title ezxhtml:level=\"4\"><link xlink:href=\"ezurl://23\" xlink:show=\"none\">User Manual</link> for eZ Platform (and eZ Studio)</title><title ezxhtml:level=\"4\"><link xlink:href=\"ezurl://24\" xlink:show=\"none\">Technical doc</link> for eZ Platform</title><title ezxhtml:level=\"4\"><link xlink:href=\"ezurl://25\" xlink:show=\"none\">eZ Platform</link> developer tutorial</title><title ezxhtml:level=\"4\"><link xlink:href=\"ezurl://26\" xlink:show=\"none\">Comparison guide</link> between eZ Platform and eZ Publish Platform</title><title ezxhtml:level=\"3\">Contribute to the project?</title><para>Join the community:</para><title ezxhtml:level=\"4\"><link xlink:href=\"ezurl://27\" xlink:show=\"none\">Github repositories</link> open to the public</title><title ezxhtml:level=\"4\">Register to the community on <link xlink:href=\"ezurl://28\" xlink:show=\"none\">discuss.ezplatform.com</link>, the community site, to access forums, tutorials and blogs</title><title ezxhtml:level=\"4\">Join the conversation on <link xlink:href=\"ezurl://29\" xlink:show=\"none\">the eZ Community Slack</link></title><para>Good luck!</para></section>\n','ezrichtext',104,'eng-GB',3,0,'',9),(0,156,41,0,1045487555,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext',105,'eng-GB',3,0,'',1),(0,4,45,0,0,'Setup','ezstring',123,'eng-GB',3,0,'setup',1),(0,155,45,0,0,'','ezstring',124,'eng-GB',3,0,'',1),(0,119,45,0,1045487555,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext',125,'eng-GB',3,0,'',1),(0,156,45,0,1045487555,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext',126,'eng-GB',3,0,'',1),(0,4,49,0,0,'Images','ezstring',142,'eng-GB',3,0,'images',1),(0,155,49,0,0,'','ezstring',143,'eng-GB',3,0,'',1),(0,119,49,0,1045487555,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext',144,'eng-GB',3,0,'',1),(0,156,49,0,1045487555,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext',145,'eng-GB',3,0,'',1),(0,4,50,0,0,'Files','ezstring',147,'eng-GB',3,0,'files',1),(0,155,50,0,0,'','ezstring',148,'eng-GB',3,0,'',1),(0,119,50,0,1045487555,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext',149,'eng-GB',3,0,'',1),(0,156,50,0,1045487555,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext',150,'eng-GB',3,0,'',1),(0,4,51,0,0,'Multimedia','ezstring',152,'eng-GB',3,0,'multimedia',1),(0,155,51,0,0,'','ezstring',153,'eng-GB',3,0,'',1),(0,119,51,0,1045487555,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext',154,'eng-GB',3,0,'',1),(0,156,51,0,1045487555,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext',155,'eng-GB',3,0,'',1),(0,179,10,0,0,'','eztext',177,'eng-GB',3,0,'',2),(0,179,14,0,0,'','eztext',178,'eng-GB',3,0,'',3),(0,180,10,0,0,'','ezimage',179,'eng-GB',3,0,'',2),(0,180,14,0,0,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1301057722\"><original attribute_id=\"180\" attribute_version=\"3\" attribute_language=\"eng-GB\"/></ezimage>\n','ezimage',180,'eng-GB',3,0,'',3),(0,202,52,NULL,NULL,'Demo page','ezstring',181,'eng-GB',2,0,'demo page',2),(0,202,52,NULL,NULL,'Demo page','ezstring',181,'eng-GB',2,0,'demo page',3),(0,202,52,NULL,NULL,'Demo page','ezstring',181,'eng-GB',2,0,'demo page',4),(0,202,52,NULL,NULL,'Demo page','ezstring',181,'eng-GB',2,0,'demo page',5),(0,202,52,NULL,NULL,'Demo page','ezstring',181,'eng-GB',2,0,'demo page',6),(0,202,52,NULL,NULL,'Demo page','ezstring',181,'eng-GB',2,0,'demo page',7),(0,203,52,NULL,NULL,'Sub title demo page','ezstring',182,'eng-GB',2,0,'sub title demo page',2),(0,203,52,NULL,NULL,'Sub title demo page','ezstring',182,'eng-GB',2,0,'sub title demo page',3),(0,203,52,NULL,NULL,'Sub title demo page','ezstring',182,'eng-GB',2,0,'sub title demo page',4),(0,203,52,NULL,NULL,'Sub title demo page','ezstring',182,'eng-GB',2,0,'sub title demo page',5),(0,203,52,NULL,NULL,'Sub title demo page','ezstring',182,'eng-GB',2,0,'sub title demo page',6),(0,203,52,NULL,NULL,'Subtitle demo page','ezstring',182,'eng-GB',2,0,'subtitle demo page',7),(0,204,52,NULL,NULL,NULL,'ezimage',183,'eng-GB',2,0,'',2),(0,204,52,NULL,NULL,NULL,'ezimage',183,'eng-GB',2,0,'',3),(0,204,52,NULL,NULL,NULL,'ezimage',183,'eng-GB',2,0,'',4),(0,204,52,NULL,NULL,NULL,'ezimage',183,'eng-GB',2,0,'',5),(0,204,52,NULL,NULL,NULL,'ezimage',183,'eng-GB',2,0,'',6),(0,204,52,NULL,NULL,NULL,'ezimage',183,'eng-GB',2,0,'',7),(0,205,52,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer bibendum ornare enim sit amet fringilla. Sed sit amet ante hendrerit, tempor odio ut, lacinia dui. Nunc elementum elementum vehicula. Sed viverra libero ligula, ac ullamcorper massa ultricies vitae. Pellentesque sollicitudin mattis erat, nec porttitor erat sagittis ut. Phasellus quis risus scelerisque massa lobortis convallis ut et odio. Maecenas ultricies semper risus quis ullamcorper. Aliquam vitae porta quam. Pellentesque sed dolor non nibh commodo pharetra eget sed felis. Suspendisse vitae consequat tellus.</para></section>\n','ezrichtext',184,'eng-GB',2,0,'',2),(0,205,52,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer bibendum ornare enim sit amet fringilla. Sed sit amet ante hendrerit, tempor odio ut, lacinia dui. Nunc elementum elementum vehicula. Sed viverra libero ligula, ac ullamcorper massa ultricies vitae. Pellentesque sollicitudin mattis erat, nec porttitor erat sagittis ut. Phasellus quis risus scelerisque massa lobortis convallis ut et odio. Maecenas ultricies semper risus quis ullamcorper. Aliquam vitae porta quam. Pellentesque sed dolor non nibh commodo pharetra eget sed felis. Suspendisse vitae consequat tellus.</para></section>\n','ezrichtext',184,'eng-GB',2,0,'',3),(0,205,52,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer bibendum ornare enim sit amet fringilla. Sed sit amet ante hendrerit, tempor odio ut, lacinia dui. Nunc elementum elementum vehicula. Sed viverra libero ligula, ac ullamcorper massa ultricies vitae. Pellentesque sollicitudin mattis erat, nec porttitor erat sagittis ut. Phasellus quis risus scelerisque massa lobortis convallis ut et odio. Maecenas ultricies semper risus quis ullamcorper. Aliquam vitae porta quam. Pellentesque sed dolor non nibh commodo pharetra eget sed felis. Suspendisse vitae consequat tellus.</para></section>\n','ezrichtext',184,'eng-GB',2,0,'',4),(0,205,52,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer bibendum ornare enim sit amet fringilla. Sed sit amet ante hendrerit, tempor odio ut, lacinia dui. Nunc elementum elementum vehicula. Sed viverra libero ligula, ac ullamcorper massa ultricies vitae. Pellentesque sollicitudin mattis erat, nec porttitor erat sagittis ut. Phasellus quis risus scelerisque massa lobortis convallis ut et odio. Maecenas ultricies semper risus quis ullamcorper. Aliquam vitae porta quam. Pellentesque sed dolor non nibh commodo pharetra eget sed felis. Suspendisse vitae consequat tellus.</para></section>\n','ezrichtext',184,'eng-GB',2,0,'',5),(0,205,52,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer bibendum ornare enim sit amet fringilla. Sed sit amet ante hendrerit, tempor odio ut, lacinia dui. Nunc elementum elementum vehicula. Sed viverra libero ligula, ac ullamcorper massa ultricies vitae. Pellentesque sollicitudin mattis erat, nec porttitor erat sagittis ut. Phasellus quis risus scelerisque massa lobortis convallis ut et odio. Maecenas ultricies semper risus quis ullamcorper. Aliquam vitae porta quam. Pellentesque sed dolor non nibh commodo pharetra eget sed felis. Suspendisse vitae consequat tellus.</para></section>\n','ezrichtext',184,'eng-GB',2,0,'',6),(0,205,52,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer bibendum ornare enim sit amet fringilla. Sed sit amet ante hendrerit, tempor odio ut, lacinia dui. Nunc elementum elementum vehicula. Sed viverra libero ligula, ac ullamcorper massa ultricies vitae. Pellentesque sollicitudin mattis erat, nec porttitor erat sagittis ut. Phasellus quis risus scelerisque massa lobortis convallis ut et odio. Maecenas ultricies semper risus quis ullamcorper. Aliquam vitae porta quam. Pellentesque sed dolor non nibh commodo pharetra eget sed felis. Suspendisse vitae consequat tellus.</para></section>\n','ezrichtext',184,'eng-GB',2,0,'',7),(0,206,52,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para> </para><para>Pellentesque id nisi tortor. Sed sed posuere lorem. Mauris vel sem eu nibh tristique rhoncus sit amet ullamcorper velit. Cras leo nisl, tincidunt a tellus eu, efficitur suscipit ligula. Fusce gravida dignissim est, sit amet commodo metus vehicula at. Nulla sit amet cursus justo. Fusce hendrerit magna ut tortor maximus fringilla.</para><para>Pellentesque dolor lorem, hendrerit nec diam et, pharetra sollicitudin nulla. Vestibulum ut ligula at velit venenatis venenatis eget vel ex. Nam non consequat lectus. Vivamus vel tortor non augue fermentum consequat nec quis lorem. Nullam convallis auctor sem nec cursus. In vehicula erat nisl, quis tempor est placerat elementum. Praesent at erat fringilla, auctor augue vel, mollis justo.</para><para>Phasellus pharetra tortor tincidunt, posuere orci sit amet, consequat lacus. Suspendisse vel dignissim velit. Aenean est odio, suscipit in sem sed, suscipit egestas nibh. Donec non lobortis ligula. Curabitur faucibus porttitor suscipit. Mauris a urna sed turpis volutpat posuere ut vestibulum ex. Sed a dignissim quam. Nulla facilisi. Morbi suscipit justo eu augue bibendum, sit amet pulvinar magna sollicitudin. Quisque tristique ante sit amet quam pretium, quis volutpat lectus mattis. Nulla facilisi. Aliquam scelerisque ipsum id nibh lacinia tristique. Cras fermentum nibh pulvinar metus blandit, ac accumsan libero varius. Vivamus eu est maximus, posuere metus nec, suscipit arcu.</para><para>In velit dolor, eleifend nec rhoncus nec, blandit vel est. Praesent eleifend ipsum non odio gravida dignissim. Pellentesque tellus velit, luctus ut consequat eu, elementum id dolor. Sed luctus massa sed justo suscipit finibus. Cras mollis metus tortor, eu lobortis ex lacinia ac. Phasellus eu tempor odio. Donec justo sem, dictum vitae sem vitae, pretium aliquet erat.</para></section>\n','ezrichtext',185,'eng-GB',2,0,'',2),(0,206,52,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para> </para><para>Pellentesque id nisi tortor. Sed sed posuere lorem. Mauris vel sem eu nibh tristique rhoncus sit amet ullamcorper velit. Cras leo nisl, tincidunt a tellus eu, efficitur suscipit ligula. Fusce gravida dignissim est, sit amet commodo metus vehicula at. Nulla sit amet cursus justo. Fusce hendrerit magna ut tortor maximus fringilla.</para><para>Pellentesque dolor lorem, hendrerit nec diam et, pharetra sollicitudin nulla. Vestibulum ut ligula at velit venenatis venenatis eget vel ex. Nam non consequat lectus. Vivamus vel tortor non augue fermentum consequat nec quis lorem. Nullam convallis auctor sem nec cursus. In vehicula erat nisl, quis tempor est placerat elementum. Praesent at erat fringilla, auctor augue vel, mollis justo.</para><para>Phasellus pharetra tortor tincidunt, posuere orci sit amet, consequat lacus. Suspendisse vel dignissim velit. Aenean est odio, suscipit in sem sed, suscipit egestas nibh. Donec non lobortis ligula. Curabitur faucibus porttitor suscipit. Mauris a urna sed turpis volutpat posuere ut vestibulum ex. Sed a dignissim quam. Nulla facilisi. Morbi suscipit justo eu augue bibendum, sit amet pulvinar magna sollicitudin. Quisque tristique ante sit amet quam pretium, quis volutpat lectus mattis. Nulla facilisi. Aliquam scelerisque ipsum id nibh lacinia tristique. Cras fermentum nibh pulvinar metus blandit, ac accumsan libero varius. Vivamus eu est maximus, posuere metus nec, suscipit arcu.</para><para>In velit dolor, eleifend nec rhoncus nec, blandit vel est. Praesent eleifend ipsum non odio gravida dignissim. Pellentesque tellus velit, luctus ut consequat eu, elementum id dolor. Sed luctus massa sed justo suscipit finibus. Cras mollis metus tortor, eu lobortis ex lacinia ac. Phasellus eu tempor odio. Donec justo sem, dictum vitae sem vitae, pretium aliquet erat.</para></section>\n','ezrichtext',185,'eng-GB',2,0,'',3),(0,206,52,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Pellentesque id nisi tortor. Sed sed posuere lorem. Mauris vel sem eu nibh tristique rhoncus sit amet ullamcorper velit. Cras leo nisl, tincidunt a tellus eu, efficitur suscipit ligula. Fusce gravida dignissim est, sit amet commodo metus vehicula at. Nulla sit amet cursus justo. Fusce hendrerit magna ut tortor maximus fringilla.</para><para>Pellentesque dolor lorem, hendrerit nec diam et, pharetra sollicitudin nulla. Vestibulum ut ligula at velit venenatis venenatis eget vel ex. Nam non consequat lectus. Vivamus vel tortor non augue fermentum consequat nec quis lorem. Nullam convallis auctor sem nec cursus. In vehicula erat nisl, quis tempor est placerat elementum. Praesent at erat fringilla, auctor augue vel, mollis justo.</para><para>Phasellus pharetra tortor tincidunt, posuere orci sit amet, consequat lacus. Suspendisse vel dignissim velit. Aenean est odio, suscipit in sem sed, suscipit egestas nibh. Donec non lobortis ligula. Curabitur faucibus porttitor suscipit. Mauris a urna sed turpis volutpat posuere ut vestibulum ex. Sed a dignissim quam. Nulla facilisi. Morbi suscipit justo eu augue bibendum, sit amet pulvinar magna sollicitudin. Quisque tristique ante sit amet quam pretium, quis volutpat lectus mattis. Nulla facilisi. Aliquam scelerisque ipsum id nibh lacinia tristique. Cras fermentum nibh pulvinar metus blandit, ac accumsan libero varius. Vivamus eu est maximus, posuere metus nec, suscipit arcu.</para><para>In velit dolor, eleifend nec rhoncus nec, blandit vel est. Praesent eleifend ipsum non odio gravida dignissim. Pellentesque tellus velit, luctus ut consequat eu, elementum id dolor. Sed luctus massa sed justo suscipit finibus. Cras mollis metus tortor, eu lobortis ex lacinia ac. Phasellus eu tempor odio. Donec justo sem, dictum vitae sem vitae, pretium aliquet erat.</para></section>\n','ezrichtext',185,'eng-GB',2,0,'',4),(0,206,52,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Pellentesque id nisi tortor. Sed sed posuere lorem. Mauris vel sem eu nibh tristique rhoncus sit amet ullamcorper velit. Cras leo nisl, tincidunt a tellus eu, efficitur suscipit ligula. Fusce gravida dignissim est, sit amet commodo metus vehicula at. Nulla sit amet cursus justo. Fusce hendrerit magna ut tortor maximus fringilla.</para><para>Pellentesque dolor lorem, hendrerit nec diam et, pharetra sollicitudin nulla. Vestibulum ut ligula at velit venenatis venenatis eget vel ex. Nam non consequat lectus. Vivamus vel tortor non augue fermentum consequat nec quis lorem. Nullam convallis auctor sem nec cursus. In vehicula erat nisl, quis tempor est placerat elementum. Praesent at erat fringilla, auctor augue vel, mollis justo.</para><para>Phasellus pharetra tortor tincidunt, posuere orci sit amet, consequat lacus. Suspendisse vel dignissim velit. Aenean est odio, suscipit in sem sed, suscipit egestas nibh. Donec non lobortis ligula. Curabitur faucibus porttitor suscipit. Mauris a urna sed turpis volutpat posuere ut vestibulum ex. Sed a dignissim quam. Nulla facilisi. Morbi suscipit justo eu augue bibendum, sit amet pulvinar magna sollicitudin. Quisque tristique ante sit amet quam pretium, quis volutpat lectus mattis. Nulla facilisi. Aliquam scelerisque ipsum id nibh lacinia tristique. Cras fermentum nibh pulvinar metus blandit, ac accumsan libero varius. Vivamus eu est maximus, posuere metus nec, suscipit arcu.</para><para>In velit dolor, eleifend nec rhoncus nec, blandit vel est. Praesent eleifend ipsum non odio gravida dignissim. Pellentesque tellus velit, luctus ut consequat eu, elementum id dolor. Sed luctus massa sed justo suscipit finibus. Cras mollis metus tortor, eu lobortis ex lacinia ac. Phasellus eu tempor odio. Donec justo sem, dictum vitae sem vitae, pretium aliquet erat.</para><title ezxhtml:level=\"2\">Vidéos Youtube</title><ezembed xlink:href=\"ezcontent://56\" view=\"embed\"/></section>\n','ezrichtext',185,'eng-GB',2,0,'',5),(0,206,52,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Pellentesque id nisi tortor. Sed sed posuere lorem. Mauris vel sem eu nibh tristique rhoncus sit amet ullamcorper velit. Cras leo nisl, tincidunt a tellus eu, efficitur suscipit ligula. Fusce gravida dignissim est, sit amet commodo metus vehicula at. Nulla sit amet cursus justo. Fusce hendrerit magna ut tortor maximus fringilla.</para><para>Pellentesque dolor lorem, hendrerit nec diam et, pharetra sollicitudin nulla. Vestibulum ut ligula at velit venenatis venenatis eget vel ex. Nam non consequat lectus. Vivamus vel tortor non augue fermentum consequat nec quis lorem. Nullam convallis auctor sem nec cursus. In vehicula erat nisl, quis tempor est placerat elementum. Praesent at erat fringilla, auctor augue vel, mollis justo.</para><para>Phasellus pharetra tortor tincidunt, posuere orci sit amet, consequat lacus. Suspendisse vel dignissim velit. Aenean est odio, suscipit in sem sed, suscipit egestas nibh. Donec non lobortis ligula. Curabitur faucibus porttitor suscipit. Mauris a urna sed turpis volutpat posuere ut vestibulum ex. Sed a dignissim quam. Nulla facilisi. Morbi suscipit justo eu augue bibendum, sit amet pulvinar magna sollicitudin. Quisque tristique ante sit amet quam pretium, quis volutpat lectus mattis. Nulla facilisi. Aliquam scelerisque ipsum id nibh lacinia tristique. Cras fermentum nibh pulvinar metus blandit, ac accumsan libero varius. Vivamus eu est maximus, posuere metus nec, suscipit arcu.</para><para>In velit dolor, eleifend nec rhoncus nec, blandit vel est. Praesent eleifend ipsum non odio gravida dignissim. Pellentesque tellus velit, luctus ut consequat eu, elementum id dolor. Sed luctus massa sed justo suscipit finibus. Cras mollis metus tortor, eu lobortis ex lacinia ac. Phasellus eu tempor odio. Donec justo sem, dictum vitae sem vitae, pretium aliquet erat.</para><title ezxhtml:level=\"2\">Vidéos Youtube</title><ezembed xlink:href=\"ezcontent://56\" view=\"embed\"/><ezembed xlink:href=\"ezcontent://58\" view=\"embed\"/></section>\n','ezrichtext',185,'eng-GB',2,0,'',6),(0,206,52,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Pellentesque id nisi tortor. Sed sed posuere lorem. Mauris vel sem eu nibh tristique rhoncus sit amet ullamcorper velit. Cras leo nisl, tincidunt a tellus eu, efficitur suscipit ligula. Fusce gravida dignissim est, sit amet commodo metus vehicula at. Nulla sit amet cursus justo. Fusce hendrerit magna ut tortor maximus fringilla.</para><para>Pellentesque dolor lorem, hendrerit nec diam et, pharetra sollicitudin nulla. Vestibulum ut ligula at velit venenatis venenatis eget vel ex. Nam non consequat lectus. Vivamus vel tortor non augue fermentum consequat nec quis lorem. Nullam convallis auctor sem nec cursus. In vehicula erat nisl, quis tempor est placerat elementum. Praesent at erat fringilla, auctor augue vel, mollis justo.</para><para>Phasellus pharetra tortor tincidunt, posuere orci sit amet, consequat lacus. Suspendisse vel dignissim velit. Aenean est odio, suscipit in sem sed, suscipit egestas nibh. Donec non lobortis ligula. Curabitur faucibus porttitor suscipit. Mauris a urna sed turpis volutpat posuere ut vestibulum ex. Sed a dignissim quam. Nulla facilisi. Morbi suscipit justo eu augue bibendum, sit amet pulvinar magna sollicitudin. Quisque tristique ante sit amet quam pretium, quis volutpat lectus mattis. Nulla facilisi. Aliquam scelerisque ipsum id nibh lacinia tristique. Cras fermentum nibh pulvinar metus blandit, ac accumsan libero varius. Vivamus eu est maximus, posuere metus nec, suscipit arcu.</para><para>In velit dolor, eleifend nec rhoncus nec, blandit vel est. Praesent eleifend ipsum non odio gravida dignissim. Pellentesque tellus velit, luctus ut consequat eu, elementum id dolor. Sed luctus massa sed justo suscipit finibus. Cras mollis metus tortor, eu lobortis ex lacinia ac. Phasellus eu tempor odio. Donec justo sem, dictum vitae sem vitae, pretium aliquet erat.</para><title ezxhtml:level=\"2\">Youtube videos</title><ezembed xlink:href=\"ezcontent://56\" view=\"embed\"/><ezembed xlink:href=\"ezcontent://58\" view=\"embed\"/><title ezxhtml:level=\"2\"> Call-to-action buttons</title><ezembed xlink:href=\"ezcontent://60\" view=\"embed\"/><ezembed xlink:href=\"ezcontent://61\" view=\"embed\"/><ezembed xlink:href=\"ezcontent://62\" view=\"embed\"/><ezembed xlink:href=\"ezcontent://63\" view=\"embed\"/><ezembed xlink:href=\"ezcontent://64\" view=\"embed\"/></section>\n','ezrichtext',185,'eng-GB',2,0,'',7),(0,207,52,NULL,1521504000,NULL,'ezdate',186,'eng-GB',2,1521504000,'',2),(0,207,52,NULL,1521504000,NULL,'ezdate',186,'eng-GB',2,1521504000,'',3),(0,207,52,NULL,1521504000,NULL,'ezdate',186,'eng-GB',2,1521504000,'',4),(0,207,52,NULL,1521590400,NULL,'ezdate',186,'eng-GB',2,1521590400,'',5),(0,207,52,NULL,1521590400,NULL,'ezdate',186,'eng-GB',2,1521590400,'',6),(0,207,52,NULL,1521590400,NULL,'ezdate',186,'eng-GB',2,1521590400,'',7),(0,208,52,NULL,NULL,NULL,'ezimage',187,'eng-GB',2,0,'',2),(0,208,52,NULL,NULL,NULL,'ezimage',187,'eng-GB',2,0,'',3),(0,208,52,NULL,NULL,NULL,'ezimage',187,'eng-GB',2,0,'',4),(0,208,52,NULL,NULL,NULL,'ezimage',187,'eng-GB',2,0,'',5),(0,208,52,NULL,NULL,NULL,'ezimage',187,'eng-GB',2,0,'',6),(0,208,52,NULL,NULL,NULL,'ezimage',187,'eng-GB',2,0,'',7),(0,209,52,NULL,NULL,NULL,'ezstring',188,'eng-GB',2,0,'',2),(0,209,52,NULL,NULL,NULL,'ezstring',188,'eng-GB',2,0,'',3),(0,209,52,NULL,NULL,NULL,'ezstring',188,'eng-GB',2,0,'',4),(0,209,52,NULL,NULL,NULL,'ezstring',188,'eng-GB',2,0,'',5),(0,209,52,NULL,NULL,NULL,'ezstring',188,'eng-GB',2,0,'',6),(0,209,52,NULL,NULL,NULL,'ezstring',188,'eng-GB',2,0,'',7),(0,210,52,NULL,NULL,NULL,'ezstring',189,'eng-GB',2,0,'',2),(0,210,52,NULL,NULL,NULL,'ezstring',189,'eng-GB',2,0,'',3),(0,210,52,NULL,NULL,NULL,'ezstring',189,'eng-GB',2,0,'',4),(0,210,52,NULL,NULL,NULL,'ezstring',189,'eng-GB',2,0,'',5),(0,210,52,NULL,NULL,NULL,'ezstring',189,'eng-GB',2,0,'',6),(0,210,52,NULL,NULL,NULL,'ezstring',189,'eng-GB',2,0,'',7),(0,249,52,NULL,1,NULL,'ezboolean',190,'eng-GB',2,1,'',2),(0,249,52,NULL,0,NULL,'ezboolean',190,'eng-GB',2,0,'',3),(0,249,52,NULL,0,NULL,'ezboolean',190,'eng-GB',2,0,'',4),(0,249,52,NULL,0,NULL,'ezboolean',190,'eng-GB',2,0,'',5),(0,249,52,NULL,0,NULL,'ezboolean',190,'eng-GB',2,0,'',6),(0,249,52,NULL,0,NULL,'ezboolean',190,'eng-GB',2,0,'',7),(0,181,53,NULL,NULL,'Global configuration','ezstring',191,'eng-GB',2,0,'global configuration',1),(0,182,53,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"logo.png\"\n    suffix=\"png\" basename=\"logo\" dirpath=\"var/site/storage/images/2/9/1/0/192-1-eng-GB\" url=\"var/site/storage/images/2/9/1/0/192-1-eng-GB/logo.png\"\n    original_filename=\"logo.png\" mime_type=\"image/png\" width=\"2000\"\n    height=\"2000\" alternative_text=\"Logo Com to Code\" alias_key=\"1293033771\" timestamp=\"1521628862\">\n  <original attribute_id=\"192\" attribute_version=\"1\" attribute_language=\"eng-GB\"/>\n  <information Height=\"2000\" Width=\"2000\" IsColor=\"1\"/>\n</ezimage>','ezimage',192,'eng-GB',2,0,'',1),(0,183,53,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"logo.png\"\n    suffix=\"png\" basename=\"logo\" dirpath=\"var/site/storage/images/3/9/1/0/193-1-eng-GB\" url=\"var/site/storage/images/3/9/1/0/193-1-eng-GB/logo.png\"\n    original_filename=\"logo.png\" mime_type=\"image/png\" width=\"2000\"\n    height=\"2000\" alternative_text=\"Logo Com to Code\" alias_key=\"1293033771\" timestamp=\"1521628862\">\n  <original attribute_id=\"193\" attribute_version=\"1\" attribute_language=\"eng-GB\"/>\n  <information Height=\"2000\" Width=\"2000\" IsColor=\"1\"/>\n</ezimage>','ezimage',193,'eng-GB',2,0,'',1),(0,184,53,NULL,NULL,'#0C687E','ezstring',194,'eng-GB',2,0,'#0c687e',1),(0,185,53,NULL,NULL,'#1692B0','ezstring',195,'eng-GB',2,0,'#1692b0',1),(0,186,53,NULL,NULL,'hello@com-to-code.com','ezstring',196,'eng-GB',2,0,'hello@com-to-code.com',1),(0,187,53,NULL,NULL,NULL,'ezstring',197,'eng-GB',2,0,'',1),(0,188,53,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',198,'eng-GB',2,0,'',1),(0,189,53,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>9, rue Tronchet - 69006 LYON</para></section>\n','ezrichtext',199,'eng-GB',2,0,'',1),(0,190,53,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext',200,'eng-GB',2,0,'',1),(0,191,53,NULL,30,'Facebook Com to Code','ezurl',201,'eng-GB',2,0,'',1),(0,192,53,NULL,31,'Twitter Com to Code','ezurl',202,'eng-GB',2,0,'',1),(0,193,53,NULL,32,'Instagram Com to Code','ezurl',203,'eng-GB',2,0,'',1),(0,195,54,NULL,NULL,'Homepage','ezstring',204,'eng-GB',2,0,'homepage',1),(0,196,54,NULL,NULL,'Subtitle homepage','ezstring',205,'eng-GB',2,0,'subtitle homepage',1),(0,197,54,NULL,NULL,NULL,'ezimage',206,'eng-GB',2,0,'',1),(0,198,54,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer bibendum ornare enim sit amet fringilla. Sed sit amet ante hendrerit, tempor odio ut, lacinia dui. Nunc elementum elementum vehicula. Sed viverra libero ligula, ac ullamcorper massa ultricies vitae. Pellentesque sollicitudin mattis erat, nec porttitor erat sagittis ut. Phasellus quis risus scelerisque massa lobortis convallis ut et odio. Maecenas ultricies semper risus quis ullamcorper. Aliquam vitae porta quam. Pellentesque sed dolor non nibh commodo pharetra eget sed felis. Suspendisse vitae consequat tellus.</para></section>\n','ezrichtext',207,'eng-GB',2,0,'',1),(0,199,54,NULL,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',208,'eng-GB',2,0,'',1),(0,200,54,NULL,NULL,'Homepage','ezstring',209,'eng-GB',2,0,'homepage',1),(0,201,54,NULL,NULL,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer bibendum ornare enim sit amet fringilla. Sed sit amet ante hendrerit, tempor odio ut, lacinia dui. Nunc elementum elementum vehicula. Sed viverra libero ligula, ac ullamcorper massa ultricies vitae. Pellentesque sollicitudin mattis erat, nec porttitor erat sagittis ut. Phasellus quis risus scelerisque massa lobortis convallis ut et odio. Maecenas ultricies semper risus quis ullamcorper. Aliquam vitae porta quam. Pellentesque sed dolor non nibh commodo pharetra eget sed felis.','ezstring',210,'eng-GB',2,0,'lorem ipsum dolor sit amet, consectetur adipiscing elit. integer bibendum ornare enim sit amet fringilla. sed sit amet ante hendrerit, tempor odio ut, lacinia dui. nunc elementum elementum vehicula. sed viverra libero ligula, ac ullamcorper massa ultricie',1),(0,250,54,NULL,0,NULL,'ezboolean',211,'eng-GB',2,0,'',1),(0,4,55,NULL,NULL,'Vidéos Youtube','ezstring',212,'fre-FR',4,0,'vidéos youtube',1),(0,155,55,NULL,NULL,NULL,'ezstring',213,'fre-FR',4,0,'',1),(0,119,55,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext',214,'fre-FR',4,0,'',1),(0,156,55,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext',215,'fre-FR',4,0,'',1),(0,241,56,NULL,NULL,'Inside Com to code','ezstring',216,'eng-GB',2,0,'inside com to code',1),(0,241,56,NULL,NULL,'Inside Com to code','ezstring',216,'eng-GB',2,0,'inside com to code',2),(0,241,56,NULL,NULL,'Inside Com to code','ezstring',216,'eng-GB',2,0,'inside com to code',3),(0,242,56,NULL,NULL,'GtKpT8aUQHw&t','ezstring',217,'eng-GB',2,0,'gtkpt8auqhw&t',1),(0,242,56,NULL,NULL,'GtKpT8aUQHw','ezstring',217,'eng-GB',2,0,'gtkpt8auqhw',2),(0,242,56,NULL,NULL,'GtKpT8aUQHw','ezstring',217,'eng-GB',2,0,'gtkpt8auqhw',3),(0,243,56,NULL,1,NULL,'ezboolean',218,'eng-GB',2,1,'',1),(0,243,56,NULL,1,NULL,'ezboolean',218,'eng-GB',2,1,'',2),(0,243,56,NULL,1,NULL,'ezboolean',218,'eng-GB',2,1,'',3),(0,4,57,NULL,NULL,'Youtube videos','ezstring',219,'eng-GB',2,0,'youtube videos',1),(0,155,57,NULL,NULL,NULL,'ezstring',220,'eng-GB',2,0,'',1),(0,119,57,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext',221,'eng-GB',2,0,'',1),(0,156,57,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext',222,'eng-GB',2,0,'',1),(0,241,58,NULL,NULL,'Inside Com to code','ezstring',223,'eng-GB',2,0,'inside com to code',3),(0,241,58,NULL,NULL,'Inside Com to code - normal width','ezstring',223,'eng-GB',2,0,'inside com to code - normal width',4),(0,242,58,NULL,NULL,'GtKpT8aUQHw','ezstring',224,'eng-GB',2,0,'gtkpt8auqhw',3),(0,242,58,NULL,NULL,'GtKpT8aUQHw','ezstring',224,'eng-GB',2,0,'gtkpt8auqhw',4),(0,243,58,NULL,1,NULL,'ezboolean',225,'eng-GB',2,1,'',3),(0,243,58,NULL,0,NULL,'ezboolean',225,'eng-GB',2,0,'',4),(0,241,58,NULL,NULL,'Inside Com to code','ezstring',226,'eng-GB',2,0,'inside com to code',1),(0,242,58,NULL,NULL,'GtKpT8aUQHw&t','ezstring',227,'eng-GB',2,0,'gtkpt8auqhw&t',1),(0,243,58,NULL,1,NULL,'ezboolean',228,'eng-GB',2,1,'',1),(0,241,58,NULL,NULL,'Inside Com to code','ezstring',229,'eng-GB',2,0,'inside com to code',2),(0,242,58,NULL,NULL,'GtKpT8aUQHw','ezstring',230,'eng-GB',2,0,'gtkpt8auqhw',2),(0,243,58,NULL,1,NULL,'ezboolean',231,'eng-GB',2,1,'',2),(0,4,59,NULL,NULL,'Call-to-action links','ezstring',232,'eng-GB',2,0,'call-to-action links',1),(0,155,59,NULL,NULL,NULL,'ezstring',233,'eng-GB',2,0,'',1),(0,119,59,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext',234,'eng-GB',2,0,'',1),(0,156,59,NULL,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"5.0-variant ezpublish-1.0\"/>\n','ezrichtext',235,'eng-GB',2,0,'',1),(0,244,60,NULL,NULL,'Simple link example','ezstring',236,'eng-GB',2,0,'simple link example',1),(0,245,60,NULL,NULL,'Simple link example','ezstring',237,'eng-GB',2,0,'simple link example',1),(0,246,60,NULL,NULL,NULL,'ezobjectrelation',238,'eng-GB',2,0,'',1),(0,247,60,NULL,33,NULL,'ezurl',239,'eng-GB',2,0,'',1),(0,248,60,NULL,NULL,'0','ezselection',240,'eng-GB',2,0,'0',1),(0,244,61,NULL,NULL,'Orange button example','ezstring',241,'eng-GB',2,0,'orange button example',1),(0,244,61,NULL,NULL,'Orange button example','ezstring',241,'eng-GB',2,0,'orange button example',2),(0,244,61,NULL,NULL,'Primary color button example','ezstring',241,'eng-GB',2,0,'primary color button example',3),(0,245,61,NULL,NULL,NULL,'ezstring',242,'eng-GB',2,0,'',1),(0,245,61,NULL,NULL,NULL,'ezstring',242,'eng-GB',2,0,'',2),(0,245,61,NULL,NULL,NULL,'ezstring',242,'eng-GB',2,0,'',3),(0,246,61,NULL,NULL,NULL,'ezobjectrelation',243,'eng-GB',2,0,'',1),(0,246,61,NULL,NULL,NULL,'ezobjectrelation',243,'eng-GB',2,0,'',2),(0,246,61,NULL,NULL,NULL,'ezobjectrelation',243,'eng-GB',2,0,'',3),(0,247,61,NULL,33,NULL,'ezurl',244,'eng-GB',2,0,'',1),(0,247,61,NULL,33,NULL,'ezurl',244,'eng-GB',2,0,'',2),(0,247,61,NULL,33,NULL,'ezurl',244,'eng-GB',2,0,'',3),(0,248,61,NULL,NULL,'1','ezselection',245,'eng-GB',2,0,'1',1),(0,248,61,NULL,NULL,'1','ezselection',245,'eng-GB',2,0,'1',2),(0,248,61,NULL,NULL,'1','ezselection',245,'eng-GB',2,0,'1',3),(0,244,62,NULL,NULL,'Secondary color button example','ezstring',246,'eng-GB',2,0,'secondary color button example',1),(0,245,62,NULL,NULL,NULL,'ezstring',247,'eng-GB',2,0,'',1),(0,246,62,NULL,NULL,NULL,'ezobjectrelation',248,'eng-GB',2,0,'',1),(0,247,62,NULL,33,NULL,'ezurl',249,'eng-GB',2,0,'',1),(0,248,62,NULL,NULL,'2','ezselection',250,'eng-GB',2,0,'2',1),(0,244,63,NULL,NULL,'Black button example','ezstring',251,'eng-GB',2,0,'black button example',1),(0,245,63,NULL,NULL,NULL,'ezstring',252,'eng-GB',2,0,'',1),(0,246,63,NULL,NULL,NULL,'ezobjectrelation',253,'eng-GB',2,0,'',1),(0,247,63,NULL,33,NULL,'ezurl',254,'eng-GB',2,0,'',1),(0,248,63,NULL,NULL,'3','ezselection',255,'eng-GB',2,0,'3',1),(0,244,64,NULL,NULL,'White button example','ezstring',256,'eng-GB',2,0,'white button example',1),(0,245,64,NULL,NULL,NULL,'ezstring',257,'eng-GB',2,0,'',1),(0,246,64,NULL,NULL,NULL,'ezobjectrelation',258,'eng-GB',2,0,'',1),(0,247,64,NULL,33,NULL,'ezurl',259,'eng-GB',2,0,'',1),(0,248,64,NULL,NULL,'4','ezselection',260,'eng-GB',2,0,'4',1);
/*!40000 ALTER TABLE `ezcontentobject_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject_link`
--

DROP TABLE IF EXISTS `ezcontentobject_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject_link` (
  `contentclassattribute_id` int(11) NOT NULL DEFAULT '0',
  `from_contentobject_id` int(11) NOT NULL DEFAULT '0',
  `from_contentobject_version` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relation_type` int(11) NOT NULL DEFAULT '1',
  `to_contentobject_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezco_link_from` (`from_contentobject_id`,`from_contentobject_version`,`contentclassattribute_id`),
  KEY `ezco_link_to_co_id` (`to_contentobject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject_link`
--

LOCK TABLES `ezcontentobject_link` WRITE;
/*!40000 ALTER TABLE `ezcontentobject_link` DISABLE KEYS */;
INSERT INTO `ezcontentobject_link` VALUES (0,52,5,1,2,56),(0,52,6,2,2,56),(0,52,6,3,2,58),(0,52,7,4,2,56),(0,52,7,5,2,58),(0,52,7,6,2,60),(0,52,7,7,2,61),(0,52,7,8,2,62),(0,52,7,9,2,63),(0,52,7,10,2,64);
/*!40000 ALTER TABLE `ezcontentobject_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject_name`
--

DROP TABLE IF EXISTS `ezcontentobject_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject_name` (
  `content_translation` varchar(20) NOT NULL DEFAULT '',
  `content_version` int(11) NOT NULL DEFAULT '0',
  `contentobject_id` int(11) NOT NULL DEFAULT '0',
  `language_id` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `real_translation` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`contentobject_id`,`content_version`,`content_translation`),
  KEY `ezcontentobject_name_cov_id` (`content_version`),
  KEY `ezcontentobject_name_lang_id` (`language_id`),
  KEY `ezcontentobject_name_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject_name`
--

LOCK TABLES `ezcontentobject_name` WRITE;
/*!40000 ALTER TABLE `ezcontentobject_name` DISABLE KEYS */;
INSERT INTO `ezcontentobject_name` VALUES ('eng-GB',9,1,2,'eZ Platform','eng-GB'),('eng-GB',1,4,3,'Users','eng-GB'),('eng-GB',2,10,3,'Anonymous User','eng-GB'),('eng-GB',1,11,3,'Guest accounts','eng-GB'),('eng-GB',1,12,3,'Administrator users','eng-GB'),('eng-GB',1,13,3,'Editors','eng-GB'),('eng-GB',3,14,3,'Administrator User','eng-GB'),('eng-GB',1,41,3,'Media','eng-GB'),('eng-GB',1,42,3,'Anonymous Users','eng-GB'),('eng-GB',1,45,3,'Setup','eng-GB'),('eng-GB',1,49,3,'Images','eng-GB'),('eng-GB',1,50,3,'Files','eng-GB'),('eng-GB',1,51,3,'Multimedia','eng-GB'),('eng-GB',2,52,3,'Demo page','eng-GB'),('eng-GB',3,52,3,'Demo page','eng-GB'),('eng-GB',4,52,3,'Demo page','eng-GB'),('eng-GB',5,52,3,'Demo page','eng-GB'),('eng-GB',6,52,3,'Demo page','eng-GB'),('eng-GB',7,52,3,'Demo page','eng-GB'),('eng-GB',1,53,3,'Global configuration','eng-GB'),('eng-GB',1,54,3,'Homepage','eng-GB'),('fre-FR',1,55,5,'Vidéos Youtube','fre-FR'),('eng-GB',1,56,3,'Inside Com to code','eng-GB'),('eng-GB',2,56,3,'Inside Com to code','eng-GB'),('eng-GB',3,56,3,'Inside Com to code','eng-GB'),('eng-GB',1,57,3,'Youtube videos','eng-GB'),('eng-GB',1,58,3,'Inside Com to code','eng-GB'),('eng-GB',2,58,3,'Inside Com to code','eng-GB'),('eng-GB',3,58,3,'Inside Com to code','eng-GB'),('eng-GB',4,58,3,'Inside Com to code - normal width','eng-GB'),('eng-GB',1,59,3,'Call-to-action links','eng-GB'),('eng-GB',1,60,3,'Simple link example','eng-GB'),('eng-GB',1,61,3,'Orange button example','eng-GB'),('eng-GB',2,61,3,'Orange button example','eng-GB'),('eng-GB',3,61,3,'Primary color button example','eng-GB'),('eng-GB',1,62,3,'Secondary color button example','eng-GB'),('eng-GB',1,63,3,'Black button example','eng-GB'),('eng-GB',1,64,3,'White button example','eng-GB');
/*!40000 ALTER TABLE `ezcontentobject_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject_trash`
--

DROP TABLE IF EXISTS `ezcontentobject_trash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject_trash` (
  `contentobject_id` int(11) DEFAULT NULL,
  `contentobject_version` int(11) DEFAULT NULL,
  `depth` int(11) NOT NULL DEFAULT '0',
  `is_hidden` int(11) NOT NULL DEFAULT '0',
  `is_invisible` int(11) NOT NULL DEFAULT '0',
  `main_node_id` int(11) DEFAULT NULL,
  `modified_subnode` int(11) DEFAULT '0',
  `node_id` int(11) NOT NULL DEFAULT '0',
  `parent_node_id` int(11) NOT NULL DEFAULT '0',
  `path_identification_string` longtext,
  `path_string` varchar(255) NOT NULL DEFAULT '',
  `priority` int(11) NOT NULL DEFAULT '0',
  `remote_id` varchar(100) NOT NULL DEFAULT '',
  `sort_field` int(11) DEFAULT '1',
  `sort_order` int(11) DEFAULT '1',
  PRIMARY KEY (`node_id`),
  KEY `ezcobj_trash_co_id` (`contentobject_id`),
  KEY `ezcobj_trash_depth` (`depth`),
  KEY `ezcobj_trash_modified_subnode` (`modified_subnode`),
  KEY `ezcobj_trash_p_node_id` (`parent_node_id`),
  KEY `ezcobj_trash_path` (`path_string`),
  KEY `ezcobj_trash_path_ident` (`path_identification_string`(50))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject_trash`
--

LOCK TABLES `ezcontentobject_trash` WRITE;
/*!40000 ALTER TABLE `ezcontentobject_trash` DISABLE KEYS */;
INSERT INTO `ezcontentobject_trash` VALUES (1,9,2,0,0,56,1521629667,56,2,'node_2/location_56','/1/2/56/',0,'d03e7b304979ff28508ea7801ea71929',9,1),(55,1,2,0,0,57,1521630073,57,43,'media/videos_youtube','/1/43/57/',0,'3c9e8345a58330a6cf5b41adc9481401',9,1);
/*!40000 ALTER TABLE `ezcontentobject_trash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject_tree`
--

DROP TABLE IF EXISTS `ezcontentobject_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject_tree` (
  `contentobject_id` int(11) DEFAULT NULL,
  `contentobject_is_published` int(11) DEFAULT NULL,
  `contentobject_version` int(11) DEFAULT NULL,
  `depth` int(11) NOT NULL DEFAULT '0',
  `is_hidden` int(11) NOT NULL DEFAULT '0',
  `is_invisible` int(11) NOT NULL DEFAULT '0',
  `main_node_id` int(11) DEFAULT NULL,
  `modified_subnode` int(11) DEFAULT '0',
  `node_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_node_id` int(11) NOT NULL DEFAULT '0',
  `path_identification_string` longtext,
  `path_string` varchar(255) NOT NULL DEFAULT '',
  `priority` int(11) NOT NULL DEFAULT '0',
  `remote_id` varchar(100) NOT NULL DEFAULT '',
  `sort_field` int(11) DEFAULT '1',
  `sort_order` int(11) DEFAULT '1',
  PRIMARY KEY (`node_id`),
  KEY `ezcontentobject_tree_remote_id` (`remote_id`),
  KEY `ezcontentobject_tree_co_id` (`contentobject_id`),
  KEY `ezcontentobject_tree_depth` (`depth`),
  KEY `ezcontentobject_tree_p_node_id` (`parent_node_id`),
  KEY `ezcontentobject_tree_path` (`path_string`),
  KEY `ezcontentobject_tree_path_ident` (`path_identification_string`(50)),
  KEY `modified_subnode` (`modified_subnode`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject_tree`
--

LOCK TABLES `ezcontentobject_tree` WRITE;
/*!40000 ALTER TABLE `ezcontentobject_tree` DISABLE KEYS */;
INSERT INTO `ezcontentobject_tree` VALUES (0,1,1,0,0,0,1,1521632521,1,1,'','/1/',0,'629709ba256fe317c3ddcee35453a96a',1,1),(54,1,1,1,0,0,2,1521629704,2,1,'node_2','/1/2/',0,'f3e90596361e31d496d4026eb624c983',8,1),(4,1,1,1,0,0,5,1301062024,5,1,'users','/1/5/',0,'3f6d92f8044aed134f32153517850f5a',1,1),(11,1,1,2,0,0,12,1081860719,12,5,'users/guest_accounts','/1/5/12/',0,'602dcf84765e56b7f999eaafd3821dd3',1,1),(12,1,1,2,0,0,13,1301062024,13,5,'users/administrator_users','/1/5/13/',0,'769380b7aa94541679167eab817ca893',1,1),(13,1,1,2,0,0,14,1081860719,14,5,'users/editors','/1/5/14/',0,'f7dda2854fc68f7c8455d9cb14bd04a9',1,1),(14,1,3,3,0,0,15,1301062024,15,13,'users/administrator_users/administrator_user','/1/5/13/15/',0,'e5161a99f733200b9ed4e80f9c16187b',1,1),(41,1,1,1,0,0,43,1521632521,43,1,'media','/1/43/',0,'75c715a51699d2d309a924eca6a95145',9,1),(42,1,1,2,0,0,44,1081860719,44,5,'users/anonymous_users','/1/5/44/',0,'4fdf0072da953bb276c0c7e0141c5c9b',9,1),(10,1,2,3,0,0,45,1081860719,45,44,'users/anonymous_users/anonymous_user','/1/5/44/45/',0,'2cf8343bee7b482bab82b269d8fecd76',9,1),(45,1,1,1,0,0,48,1448833726,48,1,'setup2','/1/48/',0,'182ce1b5af0c09fa378557c462ba2617',9,1),(49,1,1,2,0,0,51,1081860720,51,43,'media/images','/1/43/51/',0,'1b26c0454b09bb49dfb1b9190ffd67cb',9,1),(50,1,1,2,0,0,52,1081860720,52,43,'media/files','/1/43/52/',0,'0b113a208f7890f9ad3c24444ff5988c',9,1),(51,1,1,2,0,0,53,1081860720,53,43,'media/multimedia','/1/43/53/',0,'4f18b82c75f10aad476cae5adf98c11f',9,1),(52,1,7,2,0,0,54,1521627149,54,2,'node_2/demo_page','/1/2/54/',0,'2ffbcfea2c42ab512b0d3539630619ac',9,1),(53,1,1,2,0,0,55,1521628862,55,2,'node_2/global_configuration','/1/2/55/',0,'4d1f19db5e04d25db212711d94ea9eee',9,1),(56,1,3,3,0,0,58,1521630465,58,59,'media/youtube_videos/inside_com_to_code','/1/43/59/58/',0,'23fb5eb16812844fcd1d6e977529e293',9,1),(57,1,1,2,0,0,59,1521632271,59,43,'media/youtube_videos','/1/43/59/',0,'46a7da2c2fafe76cfb05d74e19eecc32',9,1),(58,1,4,3,0,0,60,1521632658,60,59,'media/youtube_videos/inside_com_to_code_normal_width','/1/43/59/60/',0,'bbbdd3978b8504c7d171a5430dd6da46',9,1),(59,1,1,2,0,0,61,1521632950,61,43,'media/call_to_action_links','/1/43/61/',0,'6d19ab1e3ed923784a1f5c44f46cf630',9,1),(60,1,1,3,0,0,62,1521633090,62,61,'media/call_to_action_links/simple_link_example','/1/43/61/62/',0,'a0a247ca8b7ea154576f2e0758b4593c',9,1),(61,1,3,3,0,0,63,1521633150,63,61,'media/call_to_action_links/primary_color_button_example','/1/43/61/63/',0,'c52a15055ae7e688fe3f00b06d07ddb3',9,1),(62,1,1,3,0,0,64,1521638152,64,61,'media/call_to_action_links/secondary_color_button_example','/1/43/61/64/',0,'7723054b2334aee049154dab18290ad2',9,1),(63,1,1,3,0,0,65,1521638250,65,61,'media/call_to_action_links/black_button_example','/1/43/61/65/',0,'addcd18c60c6a7c7622764027435b6e4',9,1),(64,1,1,3,0,0,66,1521638293,66,61,'media/call_to_action_links/white_button_example','/1/43/61/66/',0,'1226ab890a820487a3dbe01000382ed0',9,1);
/*!40000 ALTER TABLE `ezcontentobject_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject_version`
--

DROP TABLE IF EXISTS `ezcontentobject_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject_version` (
  `contentobject_id` int(11) DEFAULT NULL,
  `created` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `initial_language_id` bigint(20) NOT NULL DEFAULT '0',
  `language_mask` bigint(20) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  `workflow_event_pos` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezcobj_version_creator_id` (`creator_id`),
  KEY `ezcobj_version_status` (`status`),
  KEY `idx_object_version_objver` (`contentobject_id`,`version`),
  KEY `ezcontobj_version_obj_status` (`contentobject_id`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=533 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject_version`
--

LOCK TABLES `ezcontentobject_version` WRITE;
/*!40000 ALTER TABLE `ezcontentobject_version` DISABLE KEYS */;
INSERT INTO `ezcontentobject_version` VALUES (4,0,14,4,2,3,0,1,0,1,1),(11,1033920737,14,439,2,3,1033920746,1,0,1,0),(12,1033920760,14,440,2,3,1033920775,1,0,1,0),(13,1033920786,14,441,2,3,1033920794,1,0,1,0),(41,1060695450,14,472,2,3,1060695457,1,0,1,0),(42,1072180278,14,473,2,3,1072180330,1,0,1,0),(10,1072180337,14,474,2,3,1072180405,1,0,2,0),(45,1079684084,14,477,2,3,1079684190,1,0,1,0),(49,1080220181,14,488,2,3,1080220197,1,0,1,0),(50,1080220211,14,489,2,3,1080220220,1,0,1,0),(51,1080220225,14,490,2,3,1080220233,1,0,1,0),(14,1301061783,14,499,2,3,1301062024,1,0,3,0),(1,1448889045,14,506,2,3,1448889046,1,0,9,0),(52,1521628232,14,508,2,2,1521628366,3,0,2,0),(52,1521628344,14,509,2,2,1521631218,3,0,3,0),(53,1521628627,14,510,2,2,1521628862,1,0,1,0),(54,1521629667,14,511,2,2,1521629667,1,0,1,0),(55,1521630073,14,512,4,4,1521630073,1,0,1,0),(56,1521630465,14,513,2,2,1521632437,3,0,1,0),(52,1521630582,14,514,2,2,1521631449,3,0,4,0),(52,1521631344,14,515,2,2,1521632801,3,0,5,0),(57,1521632271,14,516,2,2,1521632271,1,0,1,0),(56,1521632358,14,517,2,2,1521632638,3,0,2,0),(56,1521632561,14,518,2,2,1521632638,1,0,3,0),(58,1521632658,14,519,2,2,1521632717,3,0,3,0),(58,1521632658,14,520,2,2,1521632658,3,0,1,0),(58,1521632658,14,521,2,2,1521632658,3,0,2,0),(58,1521632694,14,522,2,2,1521632717,1,0,4,0),(52,1521632764,14,523,2,2,1521638460,3,0,6,0),(59,1521632950,14,524,2,2,1521632950,1,0,1,0),(60,1521633090,14,525,2,2,1521633090,1,0,1,0),(61,1521633150,14,526,2,2,1521638082,3,0,1,0),(61,1521638061,14,527,2,2,1521638061,0,0,2,0),(61,1521638062,14,528,2,2,1521638082,1,0,3,0),(62,1521638152,14,529,2,2,1521638153,1,0,1,0),(63,1521638250,14,530,2,2,1521638250,1,0,1,0),(64,1521638293,14,531,2,2,1521638293,1,0,1,0),(52,1521638312,14,532,2,2,1521638460,1,0,7,0);
/*!40000 ALTER TABLE `ezcontentobject_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcurrencydata`
--

DROP TABLE IF EXISTS `ezcurrencydata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcurrencydata` (
  `auto_rate_value` decimal(10,5) NOT NULL DEFAULT '0.00000',
  `code` varchar(4) NOT NULL DEFAULT '',
  `custom_rate_value` decimal(10,5) NOT NULL DEFAULT '0.00000',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(255) NOT NULL DEFAULT '',
  `rate_factor` decimal(10,5) NOT NULL DEFAULT '1.00000',
  `status` int(11) NOT NULL DEFAULT '1',
  `symbol` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ezcurrencydata_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcurrencydata`
--

LOCK TABLES `ezcurrencydata` WRITE;
/*!40000 ALTER TABLE `ezcurrencydata` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcurrencydata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezdfsfile`
--

DROP TABLE IF EXISTS `ezdfsfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezdfsfile` (
  `name` text NOT NULL,
  `name_trunk` text NOT NULL,
  `name_hash` varchar(34) NOT NULL DEFAULT '',
  `datatype` varchar(255) NOT NULL DEFAULT 'application/octet-stream',
  `scope` varchar(25) NOT NULL DEFAULT '',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0',
  `mtime` int(11) NOT NULL DEFAULT '0',
  `expired` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name_hash`),
  KEY `ezdfsfile_name` (`name`(250)),
  KEY `ezdfsfile_name_trunk` (`name_trunk`(250)),
  KEY `ezdfsfile_mtime` (`mtime`),
  KEY `ezdfsfile_expired_name` (`expired`,`name`(250))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezdfsfile`
--

LOCK TABLES `ezdfsfile` WRITE;
/*!40000 ALTER TABLE `ezdfsfile` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezdfsfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezdiscountrule`
--

DROP TABLE IF EXISTS `ezdiscountrule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezdiscountrule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezdiscountrule`
--

LOCK TABLES `ezdiscountrule` WRITE;
/*!40000 ALTER TABLE `ezdiscountrule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezdiscountrule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezdiscountsubrule`
--

DROP TABLE IF EXISTS `ezdiscountsubrule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezdiscountsubrule` (
  `discount_percent` float DEFAULT NULL,
  `discountrule_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `limitation` char(1) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezdiscountsubrule`
--

LOCK TABLES `ezdiscountsubrule` WRITE;
/*!40000 ALTER TABLE `ezdiscountsubrule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezdiscountsubrule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezdiscountsubrule_value`
--

DROP TABLE IF EXISTS `ezdiscountsubrule_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezdiscountsubrule_value` (
  `discountsubrule_id` int(11) NOT NULL DEFAULT '0',
  `issection` int(11) NOT NULL DEFAULT '0',
  `value` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`discountsubrule_id`,`value`,`issection`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezdiscountsubrule_value`
--

LOCK TABLES `ezdiscountsubrule_value` WRITE;
/*!40000 ALTER TABLE `ezdiscountsubrule_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezdiscountsubrule_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezenumobjectvalue`
--

DROP TABLE IF EXISTS `ezenumobjectvalue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezenumobjectvalue` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT '0',
  `contentobject_attribute_version` int(11) NOT NULL DEFAULT '0',
  `enumelement` varchar(255) NOT NULL DEFAULT '',
  `enumid` int(11) NOT NULL DEFAULT '0',
  `enumvalue` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`contentobject_attribute_id`,`contentobject_attribute_version`,`enumid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezenumobjectvalue`
--

LOCK TABLES `ezenumobjectvalue` WRITE;
/*!40000 ALTER TABLE `ezenumobjectvalue` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezenumobjectvalue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezenumvalue`
--

DROP TABLE IF EXISTS `ezenumvalue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezenumvalue` (
  `contentclass_attribute_id` int(11) NOT NULL DEFAULT '0',
  `contentclass_attribute_version` int(11) NOT NULL DEFAULT '0',
  `enumelement` varchar(255) NOT NULL DEFAULT '',
  `enumvalue` varchar(255) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `placement` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`contentclass_attribute_id`,`contentclass_attribute_version`),
  KEY `ezenumvalue_co_cl_attr_id_co_class_att_ver` (`contentclass_attribute_id`,`contentclass_attribute_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezenumvalue`
--

LOCK TABLES `ezenumvalue` WRITE;
/*!40000 ALTER TABLE `ezenumvalue` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezenumvalue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezforgot_password`
--

DROP TABLE IF EXISTS `ezforgot_password`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezforgot_password` (
  `hash_key` varchar(32) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezforgot_password_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezforgot_password`
--

LOCK TABLES `ezforgot_password` WRITE;
/*!40000 ALTER TABLE `ezforgot_password` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezforgot_password` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezgeneral_digest_user_settings`
--

DROP TABLE IF EXISTS `ezgeneral_digest_user_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezgeneral_digest_user_settings` (
  `day` varchar(255) NOT NULL DEFAULT '',
  `digest_type` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `receive_digest` int(11) NOT NULL DEFAULT '0',
  `time` varchar(255) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ezgeneral_digest_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezgeneral_digest_user_settings`
--

LOCK TABLES `ezgeneral_digest_user_settings` WRITE;
/*!40000 ALTER TABLE `ezgeneral_digest_user_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezgeneral_digest_user_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezgmaplocation`
--

DROP TABLE IF EXISTS `ezgmaplocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezgmaplocation` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT '0',
  `contentobject_version` int(11) NOT NULL DEFAULT '0',
  `latitude` double NOT NULL DEFAULT '0',
  `longitude` double NOT NULL DEFAULT '0',
  `address` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`contentobject_attribute_id`,`contentobject_version`),
  KEY `latitude_longitude_key` (`latitude`,`longitude`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezgmaplocation`
--

LOCK TABLES `ezgmaplocation` WRITE;
/*!40000 ALTER TABLE `ezgmaplocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezgmaplocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezimagefile`
--

DROP TABLE IF EXISTS `ezimagefile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezimagefile` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT '0',
  `filepath` longtext NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `ezimagefile_coid` (`contentobject_attribute_id`),
  KEY `ezimagefile_file` (`filepath`(200))
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezimagefile`
--

LOCK TABLES `ezimagefile` WRITE;
/*!40000 ALTER TABLE `ezimagefile` DISABLE KEYS */;
INSERT INTO `ezimagefile` VALUES (192,'var/site/storage/images/2/9/1/0/192-1-eng-GB/logo.png',1),(193,'var/site/storage/images/3/9/1/0/193-1-eng-GB/logo.png',2),(192,'var/site/storage/images/2/9/1/0/192-1-eng-GB/logo.png',3),(193,'var/site/storage/images/3/9/1/0/193-1-eng-GB/logo.png',4);
/*!40000 ALTER TABLE `ezimagefile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezinfocollection`
--

DROP TABLE IF EXISTS `ezinfocollection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezinfocollection` (
  `contentobject_id` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modified` int(11) DEFAULT '0',
  `user_identifier` varchar(34) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezinfocollection_co_id_created` (`contentobject_id`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezinfocollection`
--

LOCK TABLES `ezinfocollection` WRITE;
/*!40000 ALTER TABLE `ezinfocollection` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezinfocollection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezinfocollection_attribute`
--

DROP TABLE IF EXISTS `ezinfocollection_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezinfocollection_attribute` (
  `contentclass_attribute_id` int(11) NOT NULL DEFAULT '0',
  `contentobject_attribute_id` int(11) DEFAULT NULL,
  `contentobject_id` int(11) DEFAULT NULL,
  `data_float` float DEFAULT NULL,
  `data_int` int(11) DEFAULT NULL,
  `data_text` longtext,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `informationcollection_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezinfocollection_attr_cca_id` (`contentclass_attribute_id`),
  KEY `ezinfocollection_attr_co_id` (`contentobject_id`),
  KEY `ezinfocollection_attr_coa_id` (`contentobject_attribute_id`),
  KEY `ezinfocollection_attr_ic_id` (`informationcollection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezinfocollection_attribute`
--

LOCK TABLES `ezinfocollection_attribute` WRITE;
/*!40000 ALTER TABLE `ezinfocollection_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezinfocollection_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezisbn_group`
--

DROP TABLE IF EXISTS `ezisbn_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezisbn_group` (
  `description` varchar(255) NOT NULL DEFAULT '',
  `group_number` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezisbn_group`
--

LOCK TABLES `ezisbn_group` WRITE;
/*!40000 ALTER TABLE `ezisbn_group` DISABLE KEYS */;
INSERT INTO `ezisbn_group` VALUES ('English language',0,1),('English language',1,2),('French language',2,3),('German language',3,4),('Japan',4,5),('Russian Federation and former USSR',5,6),('Iran',600,7),('Kazakhstan',601,8),('Indonesia',602,9),('Saudi Arabia',603,10),('Vietnam',604,11),('Turkey',605,12),('Romania',606,13),('Mexico',607,14),('Macedonia',608,15),('Lithuania',609,16),('Thailand',611,17),('Peru',612,18),('Mauritius',613,19),('Lebanon',614,20),('Hungary',615,21),('Thailand',616,22),('Ukraine',617,23),('China, People\'s Republic',7,24),('Czech Republic and Slovakia',80,25),('India',81,26),('Norway',82,27),('Poland',83,28),('Spain',84,29),('Brazil',85,30),('Serbia and Montenegro',86,31),('Denmark',87,32),('Italy',88,33),('Korea, Republic',89,34),('Netherlands',90,35),('Sweden',91,36),('International NGO Publishers and EC Organizations',92,37),('India',93,38),('Netherlands',94,39),('Argentina',950,40),('Finland',951,41),('Finland',952,42),('Croatia',953,43),('Bulgaria',954,44),('Sri Lanka',955,45),('Chile',956,46),('Taiwan',957,47),('Colombia',958,48),('Cuba',959,49),('Greece',960,50),('Slovenia',961,51),('Hong Kong, China',962,52),('Hungary',963,53),('Iran',964,54),('Israel',965,55),('Ukraine',966,56),('Malaysia',967,57),('Mexico',968,58),('Pakistan',969,59),('Mexico',970,60),('Philippines',971,61),('Portugal',972,62),('Romania',973,63),('Thailand',974,64),('Turkey',975,65),('Caribbean Community',976,66),('Egypt',977,67),('Nigeria',978,68),('Indonesia',979,69),('Venezuela',980,70),('Singapore',981,71),('South Pacific',982,72),('Malaysia',983,73),('Bangladesh',984,74),('Belarus',985,75),('Taiwan',986,76),('Argentina',987,77),('Hong Kong, China',988,78),('Portugal',989,79),('Qatar',9927,80),('Albania',9928,81),('Guatemala',9929,82),('Costa Rica',9930,83),('Algeria',9931,84),('Lao People\'s Democratic Republic',9932,85),('Syria',9933,86),('Latvia',9934,87),('Iceland',9935,88),('Afghanistan',9936,89),('Nepal',9937,90),('Tunisia',9938,91),('Armenia',9939,92),('Montenegro',9940,93),('Georgia',9941,94),('Ecuador',9942,95),('Uzbekistan',9943,96),('Turkey',9944,97),('Dominican Republic',9945,98),('Korea, P.D.R.',9946,99),('Algeria',9947,100),('United Arab Emirates',9948,101),('Estonia',9949,102),('Palestine',9950,103),('Kosova',9951,104),('Azerbaijan',9952,105),('Lebanon',9953,106),('Morocco',9954,107),('Lithuania',9955,108),('Cameroon',9956,109),('Jordan',9957,110),('Bosnia and Herzegovina',9958,111),('Libya',9959,112),('Saudi Arabia',9960,113),('Algeria',9961,114),('Panama',9962,115),('Cyprus',9963,116),('Ghana',9964,117),('Kazakhstan',9965,118),('Kenya',9966,119),('Kyrgyz Republic',9967,120),('Costa Rica',9968,121),('Uganda',9970,122),('Singapore',9971,123),('Peru',9972,124),('Tunisia',9973,125),('Uruguay',9974,126),('Moldova',9975,127),('Tanzania',9976,128),('Costa Rica',9977,129),('Ecuador',9978,130),('Iceland',9979,131),('Papua New Guinea',9980,132),('Morocco',9981,133),('Zambia',9982,134),('Gambia',9983,135),('Latvia',9984,136),('Estonia',9985,137),('Lithuania',9986,138),('Tanzania',9987,139),('Ghana',9988,140),('Macedonia',9989,141),('Bahrain',99901,142),('Gabon',99902,143),('Mauritius',99903,144),('Netherlands Antilles and Aruba',99904,145),('Bolivia',99905,146),('Kuwait',99906,147),('Malawi',99908,148),('Malta',99909,149),('Sierra Leone',99910,150),('Lesotho',99911,151),('Botswana',99912,152),('Andorra',99913,153),('Suriname',99914,154),('Maldives',99915,155),('Namibia',99916,156),('Brunei Darussalam',99917,157),('Faroe Islands',99918,158),('Benin',99919,159),('Andorra',99920,160),('Qatar',99921,161),('Guatemala',99922,162),('El Salvador',99923,163),('Nicaragua',99924,164),('Paraguay',99925,165),('Honduras',99926,166),('Albania',99927,167),('Georgia',99928,168),('Mongolia',99929,169),('Armenia',99930,170),('Seychelles',99931,171),('Malta',99932,172),('Nepal',99933,173),('Dominican Republic',99934,174),('Haiti',99935,175),('Bhutan',99936,176),('Macau',99937,177),('Srpska, Republic of',99938,178),('Guatemala',99939,179),('Georgia',99940,180),('Armenia',99941,181),('Sudan',99942,182),('Albania',99943,183),('Ethiopia',99944,184),('Namibia',99945,185),('Nepal',99946,186),('Tajikistan',99947,187),('Eritrea',99948,188),('Mauritius',99949,189),('Cambodia',99950,190),('Congo',99951,191),('Mali',99952,192),('Paraguay',99953,193),('Bolivia',99954,194),('Srpska, Republic of',99955,195),('Albania',99956,196),('Malta',99957,197),('Bahrain',99958,198),('Luxembourg',99959,199),('Malawi',99960,200),('El Salvador',99961,201),('Mongolia',99962,202),('Cambodia',99963,203),('Nicaragua',99964,204),('Macau',99965,205),('Kuwait',99966,206),('Paraguay',99967,207),('Botswana',99968,208),('France',10,209);
/*!40000 ALTER TABLE `ezisbn_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezisbn_group_range`
--

DROP TABLE IF EXISTS `ezisbn_group_range`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezisbn_group_range` (
  `from_number` int(11) NOT NULL DEFAULT '0',
  `group_from` varchar(32) NOT NULL DEFAULT '',
  `group_length` int(11) NOT NULL DEFAULT '0',
  `group_to` varchar(32) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to_number` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezisbn_group_range`
--

LOCK TABLES `ezisbn_group_range` WRITE;
/*!40000 ALTER TABLE `ezisbn_group_range` DISABLE KEYS */;
INSERT INTO `ezisbn_group_range` VALUES (0,'0',1,'5',1,59999),(60000,'600',3,'649',2,64999),(70000,'7',1,'7',3,79999),(80000,'80',2,'94',4,94999),(95000,'950',3,'989',5,98999),(99000,'9900',4,'9989',6,99899),(99900,'99900',5,'99999',7,99999),(10000,'10',2,'10',8,10999);
/*!40000 ALTER TABLE `ezisbn_group_range` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezisbn_registrant_range`
--

DROP TABLE IF EXISTS `ezisbn_registrant_range`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezisbn_registrant_range` (
  `from_number` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isbn_group_id` int(11) NOT NULL DEFAULT '0',
  `registrant_from` varchar(32) NOT NULL DEFAULT '',
  `registrant_length` int(11) NOT NULL DEFAULT '0',
  `registrant_to` varchar(32) NOT NULL DEFAULT '',
  `to_number` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=927 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezisbn_registrant_range`
--

LOCK TABLES `ezisbn_registrant_range` WRITE;
/*!40000 ALTER TABLE `ezisbn_registrant_range` DISABLE KEYS */;
INSERT INTO `ezisbn_registrant_range` VALUES (0,1,1,'00',2,'19',19999),(20000,2,1,'200',3,'699',69999),(70000,3,1,'7000',4,'8499',84999),(85000,4,1,'85000',5,'89999',89999),(90000,5,1,'900000',6,'949999',94999),(95000,6,1,'9500000',7,'9999999',99999),(0,7,2,'00',2,'09',9999),(10000,8,2,'100',3,'399',39999),(40000,9,2,'4000',4,'5499',54999),(55000,10,2,'55000',5,'86979',86979),(86980,11,2,'869800',6,'998999',99899),(99900,12,2,'9990000',7,'9999999',99999),(0,13,3,'00',2,'19',19999),(20000,14,3,'200',3,'349',34999),(35000,15,3,'35000',5,'39999',39999),(40000,16,3,'400',3,'699',69999),(70000,17,3,'7000',4,'8399',83999),(84000,18,3,'84000',5,'89999',89999),(90000,19,3,'900000',6,'949999',94999),(95000,20,3,'9500000',7,'9999999',99999),(0,21,4,'00',2,'02',2999),(3000,22,4,'030',3,'033',3399),(3400,23,4,'0340',4,'0369',3699),(3700,24,4,'03700',5,'03999',3999),(4000,25,4,'04',2,'19',19999),(20000,26,4,'200',3,'699',69999),(70000,27,4,'7000',4,'8499',84999),(85000,28,4,'85000',5,'89999',89999),(90000,29,4,'900000',6,'949999',94999),(95000,30,4,'9500000',7,'9539999',95399),(95400,31,4,'95400',5,'96999',96999),(97000,32,4,'9700000',7,'9899999',98999),(99000,33,4,'99000',5,'99499',99499),(99500,34,4,'99500',5,'99999',99999),(0,35,5,'00',2,'19',19999),(20000,36,5,'200',3,'699',69999),(70000,37,5,'7000',4,'8499',84999),(85000,38,5,'85000',5,'89999',89999),(90000,39,5,'900000',6,'949999',94999),(95000,40,5,'9500000',7,'9999999',99999),(0,41,6,'00',2,'19',19999),(20000,42,6,'200',3,'420',42099),(42100,43,6,'4210',4,'4299',42999),(43000,44,6,'430',3,'430',43099),(43100,45,6,'4310',4,'4399',43999),(44000,46,6,'440',3,'440',44099),(44100,47,6,'4410',4,'4499',44999),(45000,48,6,'450',3,'699',69999),(70000,49,6,'7000',4,'8499',84999),(85000,50,6,'85000',5,'89999',89999),(90000,51,6,'900000',6,'909999',90999),(91000,52,6,'91000',5,'91999',91999),(92000,53,6,'9200',4,'9299',92999),(93000,54,6,'93000',5,'94999',94999),(95000,55,6,'9500000',7,'9500999',95009),(95010,56,6,'9501',4,'9799',97999),(98000,57,6,'98000',5,'98999',98999),(99000,58,6,'9900000',7,'9909999',99099),(99100,59,6,'9910',4,'9999',99999),(0,60,7,'00',2,'09',9999),(10000,61,7,'100',3,'499',49999),(50000,62,7,'5000',4,'8999',89999),(90000,63,7,'90000',5,'99999',99999),(0,64,8,'00',2,'19',19999),(20000,65,8,'200',3,'699',69999),(70000,66,8,'7000',4,'7999',79999),(80000,67,8,'80000',5,'84999',84999),(85000,68,8,'85',2,'99',99999),(0,69,9,'00',2,'19',19999),(20000,70,9,'200',3,'799',79999),(80000,71,9,'8000',4,'9499',94999),(95000,72,9,'95000',5,'99999',99999),(0,73,10,'00',2,'04',4999),(5000,74,10,'05',2,'49',49999),(50000,75,10,'500',3,'799',79999),(80000,76,10,'8000',4,'8999',89999),(90000,77,10,'90000',5,'99999',99999),(0,78,11,'0',1,'4',49999),(50000,79,11,'50',2,'89',89999),(90000,80,11,'900',3,'979',97999),(98000,81,11,'9800',4,'9999',99999),(1000,82,12,'01',2,'09',9999),(10000,83,12,'100',3,'399',39999),(40000,84,12,'4000',4,'5999',59999),(60000,85,12,'60000',5,'89999',89999),(90000,86,12,'90',2,'99',99999),(0,87,13,'0',1,'0',9999),(10000,88,13,'10',2,'49',49999),(50000,89,13,'500',3,'799',79999),(80000,90,13,'8000',4,'9199',91999),(92000,91,13,'92000',5,'99999',99999),(0,92,14,'00',2,'39',39999),(40000,93,14,'400',3,'749',74999),(75000,94,14,'7500',4,'9499',94999),(95000,95,14,'95000',5,'99999',99999),(0,96,15,'0',1,'0',9999),(10000,97,15,'10',2,'19',19999),(20000,98,15,'200',3,'449',44999),(45000,99,15,'4500',4,'6499',64999),(65000,100,15,'65000',5,'69999',69999),(70000,101,15,'7',1,'9',99999),(0,102,16,'00',2,'39',39999),(40000,103,16,'400',3,'799',79999),(80000,104,16,'8000',4,'9499',94999),(95000,105,16,'95000',5,'99999',99999),(0,106,18,'00',2,'29',29999),(30000,107,18,'300',3,'399',39999),(40000,108,18,'4000',4,'4499',44999),(45000,109,18,'45000',5,'49999',49999),(50000,110,18,'50',2,'99',99999),(0,111,19,'0',1,'9',99999),(0,112,20,'00',2,'39',39999),(40000,113,20,'400',3,'799',79999),(80000,114,20,'8000',4,'9499',94999),(95000,115,20,'95000',5,'99999',99999),(0,116,21,'00',2,'09',9999),(10000,117,21,'100',3,'499',49999),(50000,118,21,'5000',4,'7999',79999),(80000,119,21,'80000',5,'89999',89999),(0,120,22,'00',2,'19',19999),(20000,121,22,'200',3,'699',69999),(70000,122,22,'7000',4,'8999',89999),(90000,123,22,'90000',5,'99999',99999),(0,124,23,'00',2,'49',49999),(50000,125,23,'500',3,'699',69999),(70000,126,23,'7000',4,'8999',89999),(90000,127,23,'90000',5,'99999',99999),(0,128,24,'00',2,'09',9999),(10000,129,24,'100',3,'499',49999),(50000,130,24,'5000',4,'7999',79999),(80000,131,24,'80000',5,'89999',89999),(90000,132,24,'900000',6,'999999',99999),(0,133,25,'00',2,'19',19999),(20000,134,25,'200',3,'699',69999),(70000,135,25,'7000',4,'8499',84999),(85000,136,25,'85000',5,'89999',89999),(90000,137,25,'900000',6,'999999',99999),(0,138,26,'00',2,'19',19999),(20000,139,26,'200',3,'699',69999),(70000,140,26,'7000',4,'8499',84999),(85000,141,26,'85000',5,'89999',89999),(90000,142,26,'900000',6,'999999',99999),(0,143,27,'00',2,'19',19999),(20000,144,27,'200',3,'699',69999),(70000,145,27,'7000',4,'8999',89999),(90000,146,27,'90000',5,'98999',98999),(99000,147,27,'990000',6,'999999',99999),(0,148,28,'00',2,'19',19999),(20000,149,28,'200',3,'599',59999),(60000,150,28,'60000',5,'69999',69999),(70000,151,28,'7000',4,'8499',84999),(85000,152,28,'85000',5,'89999',89999),(90000,153,28,'900000',6,'999999',99999),(0,154,29,'00',2,'14',14999),(15000,155,29,'15000',5,'19999',19999),(20000,156,29,'200',3,'699',69999),(70000,157,29,'7000',4,'8499',84999),(85000,158,29,'85000',5,'89999',89999),(90000,159,29,'9000',4,'9199',91999),(92000,160,29,'920000',6,'923999',92399),(92400,161,29,'92400',5,'92999',92999),(93000,162,29,'930000',6,'949999',94999),(95000,163,29,'95000',5,'96999',96999),(97000,164,29,'9700',4,'9999',99999),(0,165,30,'00',2,'19',19999),(20000,166,30,'200',3,'599',59999),(60000,167,30,'60000',5,'69999',69999),(70000,168,30,'7000',4,'8499',84999),(85000,169,30,'85000',5,'89999',89999),(90000,170,30,'900000',6,'979999',97999),(98000,171,30,'98000',5,'99999',99999),(0,172,31,'00',2,'29',29999),(30000,173,31,'300',3,'599',59999),(60000,174,31,'6000',4,'7999',79999),(80000,175,31,'80000',5,'89999',89999),(90000,176,31,'900000',6,'999999',99999),(0,177,32,'00',2,'29',29999),(40000,178,32,'400',3,'649',64999),(70000,179,32,'7000',4,'7999',79999),(85000,180,32,'85000',5,'94999',94999),(97000,181,32,'970000',6,'999999',99999),(0,182,33,'00',2,'19',19999),(20000,183,33,'200',3,'599',59999),(60000,184,33,'6000',4,'8499',84999),(85000,185,33,'85000',5,'89999',89999),(90000,186,33,'900000',6,'949999',94999),(95000,187,33,'95000',5,'99999',99999),(0,188,34,'00',2,'24',24999),(25000,189,34,'250',3,'549',54999),(55000,190,34,'5500',4,'8499',84999),(85000,191,34,'85000',5,'94999',94999),(95000,192,34,'950000',6,'969999',96999),(97000,193,34,'97000',5,'98999',98999),(99000,194,34,'990',3,'999',99999),(0,195,35,'00',2,'19',19999),(20000,196,35,'200',3,'499',49999),(50000,197,35,'5000',4,'6999',69999),(70000,198,35,'70000',5,'79999',79999),(80000,199,35,'800000',6,'849999',84999),(85000,200,35,'8500',4,'8999',89999),(90000,201,35,'90',2,'90',90999),(91000,202,35,'910000',6,'939999',93999),(94000,203,35,'94',2,'94',94999),(95000,204,35,'950000',6,'999999',99999),(0,205,36,'0',1,'1',19999),(20000,206,36,'20',2,'49',49999),(50000,207,36,'500',3,'649',64999),(70000,208,36,'7000',4,'7999',79999),(85000,209,36,'85000',5,'94999',94999),(97000,210,36,'970000',6,'999999',99999),(0,211,37,'0',1,'5',59999),(60000,212,37,'60',2,'79',79999),(80000,213,37,'800',3,'899',89999),(90000,214,37,'9000',4,'9499',94999),(95000,215,37,'95000',5,'98999',98999),(99000,216,37,'990000',6,'999999',99999),(0,217,38,'00',2,'09',9999),(10000,218,38,'100',3,'499',49999),(50000,219,38,'5000',4,'7999',79999),(80000,220,38,'80000',5,'94999',94999),(95000,221,38,'950000',6,'999999',99999),(0,222,39,'000',3,'599',59999),(60000,223,39,'6000',4,'8999',89999),(90000,224,39,'90000',5,'99999',99999),(0,225,40,'00',2,'49',49999),(50000,226,40,'500',3,'899',89999),(90000,227,40,'9000',4,'9899',98999),(99000,228,40,'99000',5,'99999',99999),(0,229,41,'0',1,'1',19999),(20000,230,41,'20',2,'54',54999),(55000,231,41,'550',3,'889',88999),(89000,232,41,'8900',4,'9499',94999),(95000,233,41,'95000',5,'99999',99999),(0,234,42,'00',2,'19',19999),(20000,235,42,'200',3,'499',49999),(50000,236,42,'5000',4,'5999',59999),(60000,237,42,'60',2,'65',65999),(66000,238,42,'6600',4,'6699',66999),(67000,239,42,'67000',5,'69999',69999),(70000,240,42,'7000',4,'7999',79999),(80000,241,42,'80',2,'94',94999),(95000,242,42,'9500',4,'9899',98999),(99000,243,42,'99000',5,'99999',99999),(0,244,43,'0',1,'0',9999),(10000,245,43,'10',2,'14',14999),(15000,246,43,'150',3,'549',54999),(55000,247,43,'55000',5,'59999',59999),(60000,248,43,'6000',4,'9499',94999),(95000,249,43,'95000',5,'99999',99999),(0,250,44,'00',2,'28',28999),(29000,251,44,'2900',4,'2999',29999),(30000,252,44,'300',3,'799',79999),(80000,253,44,'8000',4,'8999',89999),(90000,254,44,'90000',5,'92999',92999),(93000,255,44,'9300',4,'9999',99999),(0,256,45,'0000',4,'1999',19999),(20000,257,45,'20',2,'49',49999),(50000,258,45,'50000',5,'54999',54999),(55000,259,45,'550',3,'799',79999),(80000,260,45,'8000',4,'9499',94999),(95000,261,45,'95000',5,'99999',99999),(0,262,46,'00',2,'19',19999),(20000,263,46,'200',3,'699',69999),(70000,264,46,'7000',4,'9999',99999),(0,265,47,'00',2,'02',2999),(3000,266,47,'0300',4,'0499',4999),(5000,267,47,'05',2,'19',19999),(20000,268,47,'2000',4,'2099',20999),(21000,269,47,'21',2,'27',27999),(28000,270,47,'28000',5,'30999',30999),(31000,271,47,'31',2,'43',43999),(44000,272,47,'440',3,'819',81999),(82000,273,47,'8200',4,'9699',96999),(97000,274,47,'97000',5,'99999',99999),(0,275,48,'00',2,'56',56999),(57000,276,48,'57000',5,'59999',59999),(60000,277,48,'600',3,'799',79999),(80000,278,48,'8000',4,'9499',94999),(95000,279,48,'95000',5,'99999',99999),(0,280,49,'00',2,'19',19999),(20000,281,49,'200',3,'699',69999),(70000,282,49,'7000',4,'8499',84999),(85000,283,49,'85000',5,'99999',99999),(0,284,50,'00',2,'19',19999),(20000,285,50,'200',3,'659',65999),(66000,286,50,'6600',4,'6899',68999),(69000,287,50,'690',3,'699',69999),(70000,288,50,'7000',4,'8499',84999),(85000,289,50,'85000',5,'92999',92999),(93000,290,50,'93',2,'93',93999),(94000,291,50,'9400',4,'9799',97999),(98000,292,50,'98000',5,'99999',99999),(0,293,51,'00',2,'19',19999),(20000,294,51,'200',3,'599',59999),(60000,295,51,'6000',4,'8999',89999),(90000,296,51,'90000',5,'94999',94999),(0,297,52,'00',2,'19',19999),(20000,298,52,'200',3,'699',69999),(70000,299,52,'7000',4,'8499',84999),(85000,300,52,'85000',5,'86999',86999),(87000,301,52,'8700',4,'8999',89999),(90000,302,52,'900',3,'999',99999),(0,303,53,'00',2,'19',19999),(20000,304,53,'200',3,'699',69999),(70000,305,53,'7000',4,'8499',84999),(85000,306,53,'85000',5,'89999',89999),(90000,307,53,'9000',4,'9999',99999),(0,308,54,'00',2,'14',14999),(15000,309,54,'150',3,'249',24999),(25000,310,54,'2500',4,'2999',29999),(30000,311,54,'300',3,'549',54999),(55000,312,54,'5500',4,'8999',89999),(90000,313,54,'90000',5,'96999',96999),(97000,314,54,'970',3,'989',98999),(99000,315,54,'9900',4,'9999',99999),(0,316,55,'00',2,'19',19999),(20000,317,55,'200',3,'599',59999),(70000,318,55,'7000',4,'7999',79999),(90000,319,55,'90000',5,'99999',99999),(0,320,56,'00',2,'14',14999),(15000,321,56,'1500',4,'1699',16999),(17000,322,56,'170',3,'199',19999),(20000,323,56,'2000',4,'2999',29999),(30000,324,56,'300',3,'699',69999),(70000,325,56,'7000',4,'8999',89999),(90000,326,56,'90000',5,'99999',99999),(0,327,57,'00',2,'00',999),(1000,328,57,'0100',4,'0999',9999),(10000,329,57,'10000',5,'19999',19999),(30000,330,57,'300',3,'499',49999),(50000,331,57,'5000',4,'5999',59999),(60000,332,57,'60',2,'89',89999),(90000,333,57,'900',3,'989',98999),(99000,334,57,'9900',4,'9989',99899),(99900,335,57,'99900',5,'99999',99999),(1000,336,58,'01',2,'39',39999),(40000,337,58,'400',3,'499',49999),(50000,338,58,'5000',4,'7999',79999),(80000,339,58,'800',3,'899',89999),(90000,340,58,'9000',4,'9999',99999),(0,341,59,'0',1,'1',19999),(20000,342,59,'20',2,'39',39999),(40000,343,59,'400',3,'799',79999),(80000,344,59,'8000',4,'9999',99999),(1000,345,60,'01',2,'59',59999),(60000,346,60,'600',3,'899',89999),(90000,347,60,'9000',4,'9099',90999),(91000,348,60,'91000',5,'96999',96999),(97000,349,60,'9700',4,'9999',99999),(0,350,61,'000',3,'015',1599),(1600,351,61,'0160',4,'0199',1999),(2000,352,61,'02',2,'02',2999),(3000,353,61,'0300',4,'0599',5999),(6000,354,61,'06',2,'09',9999),(10000,355,61,'10',2,'49',49999),(50000,356,61,'500',3,'849',84999),(85000,357,61,'8500',4,'9099',90999),(91000,358,61,'91000',5,'98999',98999),(99000,359,61,'9900',4,'9999',99999),(0,360,62,'0',1,'1',19999),(20000,361,62,'20',2,'54',54999),(55000,362,62,'550',3,'799',79999),(80000,363,62,'8000',4,'9499',94999),(95000,364,62,'95000',5,'99999',99999),(0,365,63,'0',1,'0',9999),(10000,366,63,'100',3,'169',16999),(17000,367,63,'1700',4,'1999',19999),(20000,368,63,'20',2,'54',54999),(55000,369,63,'550',3,'759',75999),(76000,370,63,'7600',4,'8499',84999),(85000,371,63,'85000',5,'88999',88999),(89000,372,63,'8900',4,'9499',94999),(95000,373,63,'95000',5,'99999',99999),(0,374,64,'00',2,'19',19999),(20000,375,64,'200',3,'699',69999),(70000,376,64,'7000',4,'8499',84999),(85000,377,64,'85000',5,'89999',89999),(90000,378,64,'90000',5,'94999',94999),(95000,379,64,'9500',4,'9999',99999),(0,380,65,'00000',5,'01999',1999),(2000,381,65,'02',2,'24',24999),(25000,382,65,'250',3,'599',59999),(60000,383,65,'6000',4,'9199',91999),(92000,384,65,'92000',5,'98999',98999),(99000,385,65,'990',3,'999',99999),(0,386,66,'0',1,'3',39999),(40000,387,66,'40',2,'59',59999),(60000,388,66,'600',3,'799',79999),(80000,389,66,'8000',4,'9499',94999),(95000,390,66,'95000',5,'99999',99999),(0,391,67,'00',2,'19',19999),(20000,392,67,'200',3,'499',49999),(50000,393,67,'5000',4,'6999',69999),(70000,394,67,'700',3,'999',99999),(0,395,68,'000',3,'199',19999),(20000,396,68,'2000',4,'2999',29999),(30000,397,68,'30000',5,'79999',79999),(80000,398,68,'8000',4,'8999',89999),(90000,399,68,'900',3,'999',99999),(0,400,69,'000',3,'099',9999),(10000,401,69,'1000',4,'1499',14999),(15000,402,69,'15000',5,'19999',19999),(20000,403,69,'20',2,'29',29999),(30000,404,69,'3000',4,'3999',39999),(40000,405,69,'400',3,'799',79999),(80000,406,69,'8000',4,'9499',94999),(95000,407,69,'95000',5,'99999',99999),(0,408,70,'00',2,'19',19999),(20000,409,70,'200',3,'599',59999),(60000,410,70,'6000',4,'9999',99999),(0,411,71,'00',2,'11',11999),(12000,412,71,'1200',4,'1999',19999),(20000,413,71,'200',3,'289',28999),(29000,414,71,'2900',4,'9999',99999),(0,415,72,'00',2,'09',9999),(10000,416,72,'100',3,'699',69999),(70000,417,72,'70',2,'89',89999),(90000,418,72,'9000',4,'9799',97999),(98000,419,72,'98000',5,'99999',99999),(0,420,73,'00',2,'01',1999),(2000,421,73,'020',3,'199',19999),(20000,422,73,'2000',4,'3999',39999),(40000,423,73,'40000',5,'44999',44999),(45000,424,73,'45',2,'49',49999),(50000,425,73,'50',2,'79',79999),(80000,426,73,'800',3,'899',89999),(90000,427,73,'9000',4,'9899',98999),(99000,428,73,'99000',5,'99999',99999),(0,429,74,'00',2,'39',39999),(40000,430,74,'400',3,'799',79999),(80000,431,74,'8000',4,'8999',89999),(90000,432,74,'90000',5,'99999',99999),(0,433,75,'00',2,'39',39999),(40000,434,75,'400',3,'599',59999),(60000,435,75,'6000',4,'8999',89999),(90000,436,75,'90000',5,'99999',99999),(0,437,76,'00',2,'11',11999),(12000,438,76,'120',3,'559',55999),(56000,439,76,'5600',4,'7999',79999),(80000,440,76,'80000',5,'99999',99999),(0,441,77,'00',2,'09',9999),(10000,442,77,'1000',4,'1999',19999),(20000,443,77,'20000',5,'29999',29999),(30000,444,77,'30',2,'49',49999),(50000,445,77,'500',3,'899',89999),(90000,446,77,'9000',4,'9499',94999),(95000,447,77,'95000',5,'99999',99999),(0,448,78,'00',2,'14',14999),(15000,449,78,'15000',5,'16999',16999),(17000,450,78,'17000',5,'19999',19999),(20000,451,78,'200',3,'799',79999),(80000,452,78,'8000',4,'9699',96999),(97000,453,78,'97000',5,'99999',99999),(0,454,79,'0',1,'1',19999),(20000,455,79,'20',2,'54',54999),(55000,456,79,'550',3,'799',79999),(80000,457,79,'8000',4,'9499',94999),(95000,458,79,'95000',5,'99999',99999),(0,459,80,'00',2,'09',9999),(10000,460,80,'100',3,'399',39999),(40000,461,80,'4000',4,'4999',49999),(0,462,81,'00',2,'09',9999),(10000,463,81,'100',3,'399',39999),(40000,464,81,'4000',4,'4999',49999),(0,465,82,'0',1,'3',39999),(40000,466,82,'40',2,'54',54999),(55000,467,82,'550',3,'799',79999),(80000,468,82,'8000',4,'9999',99999),(0,469,83,'00',2,'49',49999),(50000,470,83,'500',3,'939',93999),(94000,471,83,'9400',4,'9999',99999),(0,472,84,'00',2,'29',29999),(30000,473,84,'300',3,'899',89999),(90000,474,84,'9000',4,'9999',99999),(0,475,85,'00',2,'39',39999),(40000,476,85,'400',3,'849',84999),(85000,477,85,'8500',4,'9999',99999),(0,478,86,'0',1,'0',9999),(10000,479,86,'10',2,'39',39999),(40000,480,86,'400',3,'899',89999),(90000,481,86,'9000',4,'9999',99999),(0,482,87,'0',1,'0',9999),(10000,483,87,'10',2,'49',49999),(50000,484,87,'500',3,'799',79999),(80000,485,87,'8000',4,'9999',99999),(0,486,88,'0',1,'0',9999),(10000,487,88,'10',2,'39',39999),(40000,488,88,'400',3,'899',89999),(90000,489,88,'9000',4,'9999',99999),(0,490,89,'0',1,'1',19999),(20000,491,89,'20',2,'39',39999),(40000,492,89,'400',3,'799',79999),(80000,493,89,'8000',4,'9999',99999),(0,494,90,'0',1,'2',29999),(30000,495,90,'30',2,'49',49999),(50000,496,90,'500',3,'799',79999),(80000,497,90,'8000',4,'9999',99999),(0,498,91,'00',2,'79',79999),(80000,499,91,'800',3,'949',94999),(95000,500,91,'9500',4,'9999',99999),(0,501,92,'0',1,'4',49999),(50000,502,92,'50',2,'79',79999),(80000,503,92,'800',3,'899',89999),(90000,504,92,'9000',4,'9999',99999),(0,505,93,'0',1,'1',19999),(20000,506,93,'20',2,'49',49999),(50000,507,93,'500',3,'899',89999),(90000,508,93,'9000',4,'9999',99999),(0,509,94,'0',1,'0',9999),(10000,510,94,'10',2,'39',39999),(40000,511,94,'400',3,'899',89999),(90000,512,94,'9000',4,'9999',99999),(0,513,95,'00',2,'89',89999),(90000,514,95,'900',3,'984',98499),(98500,515,95,'9850',4,'9999',99999),(0,516,96,'00',2,'29',29999),(30000,517,96,'300',3,'399',39999),(40000,518,96,'4000',4,'9999',99999),(0,519,97,'0000',4,'0999',9999),(10000,520,97,'100',3,'499',49999),(50000,521,97,'5000',4,'5999',59999),(60000,522,97,'60',2,'69',69999),(70000,523,97,'700',3,'799',79999),(80000,524,97,'80',2,'89',89999),(90000,525,97,'900',3,'999',99999),(0,526,98,'00',2,'00',999),(1000,527,98,'010',3,'079',7999),(8000,528,98,'08',2,'39',39999),(40000,529,98,'400',3,'569',56999),(57000,530,98,'57',2,'57',57999),(58000,531,98,'580',3,'849',84999),(85000,532,98,'8500',4,'9999',99999),(0,533,99,'0',1,'1',19999),(20000,534,99,'20',2,'39',39999),(40000,535,99,'400',3,'899',89999),(90000,536,99,'9000',4,'9999',99999),(0,537,100,'0',1,'1',19999),(20000,538,100,'20',2,'79',79999),(80000,539,100,'800',3,'999',99999),(0,540,101,'00',2,'39',39999),(40000,541,101,'400',3,'849',84999),(85000,542,101,'8500',4,'9999',99999),(0,543,102,'0',1,'0',9999),(10000,544,102,'10',2,'39',39999),(40000,545,102,'400',3,'899',89999),(90000,546,102,'9000',4,'9999',99999),(0,547,103,'00',2,'29',29999),(30000,548,103,'300',3,'849',84999),(85000,549,103,'8500',4,'9999',99999),(0,550,104,'00',2,'39',39999),(40000,551,104,'400',3,'849',84999),(85000,552,104,'8500',4,'9999',99999),(0,553,105,'0',1,'1',19999),(20000,554,105,'20',2,'39',39999),(40000,555,105,'400',3,'799',79999),(80000,556,105,'8000',4,'9999',99999),(0,557,106,'0',1,'0',9999),(10000,558,106,'10',2,'39',39999),(40000,559,106,'400',3,'599',59999),(60000,560,106,'60',2,'89',89999),(90000,561,106,'9000',4,'9999',99999),(0,562,107,'0',1,'1',19999),(20000,563,107,'20',2,'39',39999),(40000,564,107,'400',3,'799',79999),(80000,565,107,'8000',4,'9999',99999),(0,566,108,'00',2,'39',39999),(40000,567,108,'400',3,'929',92999),(93000,568,108,'9300',4,'9999',99999),(0,569,109,'0',1,'0',9999),(10000,570,109,'10',2,'39',39999),(40000,571,109,'400',3,'899',89999),(90000,572,109,'9000',4,'9999',99999),(0,573,110,'00',2,'39',39999),(40000,574,110,'400',3,'699',69999),(70000,575,110,'70',2,'84',84999),(85000,576,110,'8500',4,'8799',87999),(88000,577,110,'88',2,'99',99999),(0,578,111,'0',1,'0',9999),(10000,579,111,'10',2,'18',18999),(19000,580,111,'1900',4,'1999',19999),(20000,581,111,'20',2,'49',49999),(50000,582,111,'500',3,'899',89999),(90000,583,111,'9000',4,'9999',99999),(0,584,112,'0',1,'1',19999),(20000,585,112,'20',2,'79',79999),(80000,586,112,'800',3,'949',94999),(95000,587,112,'9500',4,'9999',99999),(0,588,113,'00',2,'59',59999),(60000,589,113,'600',3,'899',89999),(90000,590,113,'9000',4,'9999',99999),(0,591,114,'0',1,'2',29999),(30000,592,114,'30',2,'69',69999),(70000,593,114,'700',3,'949',94999),(95000,594,114,'9500',4,'9999',99999),(0,595,115,'00',2,'54',54999),(55000,596,115,'5500',4,'5599',55999),(56000,597,115,'56',2,'59',59999),(60000,598,115,'600',3,'849',84999),(85000,599,115,'8500',4,'9999',99999),(0,600,116,'0',1,'2',29999),(30000,601,116,'30',2,'54',54999),(55000,602,116,'550',3,'734',73499),(73500,603,116,'7350',4,'7499',74999),(75000,604,116,'7500',4,'9999',99999),(0,605,117,'0',1,'6',69999),(70000,606,117,'70',2,'94',94999),(95000,607,117,'950',3,'999',99999),(0,608,118,'00',2,'39',39999),(40000,609,118,'400',3,'899',89999),(90000,610,118,'9000',4,'9999',99999),(0,611,119,'000',3,'149',14999),(15000,612,119,'1500',4,'1999',19999),(20000,613,119,'20',2,'69',69999),(70000,614,119,'7000',4,'7499',74999),(75000,615,119,'750',3,'959',95999),(96000,616,119,'9600',4,'9999',99999),(0,617,120,'00',2,'39',39999),(40000,618,120,'400',3,'899',89999),(90000,619,120,'9000',4,'9999',99999),(0,620,121,'00',2,'49',49999),(50000,621,121,'500',3,'939',93999),(94000,622,121,'9400',4,'9999',99999),(0,623,122,'00',2,'39',39999),(40000,624,122,'400',3,'899',89999),(90000,625,122,'9000',4,'9999',99999),(0,626,123,'0',1,'5',59999),(60000,627,123,'60',2,'89',89999),(90000,628,123,'900',3,'989',98999),(99000,629,123,'9900',4,'9999',99999),(0,630,124,'00',2,'09',9999),(10000,631,124,'1',1,'1',19999),(20000,632,124,'200',3,'249',24999),(25000,633,124,'2500',4,'2999',29999),(30000,634,124,'30',2,'59',59999),(60000,635,124,'600',3,'899',89999),(90000,636,124,'9000',4,'9999',99999),(0,637,125,'00',2,'05',5999),(6000,638,125,'060',3,'089',8999),(9000,639,125,'0900',4,'0999',9999),(10000,640,125,'10',2,'69',69999),(70000,641,125,'700',3,'969',96999),(97000,642,125,'9700',4,'9999',99999),(0,643,126,'0',1,'2',29999),(30000,644,126,'30',2,'54',54999),(55000,645,126,'550',3,'749',74999),(75000,646,126,'7500',4,'9499',94999),(95000,647,126,'95',2,'99',99999),(0,648,127,'0',1,'0',9999),(10000,649,127,'100',3,'399',39999),(40000,650,127,'4000',4,'4499',44999),(45000,651,127,'45',2,'89',89999),(90000,652,127,'900',3,'949',94999),(95000,653,127,'9500',4,'9999',99999),(0,654,128,'0',1,'5',59999),(60000,655,128,'60',2,'89',89999),(90000,656,128,'900',3,'989',98999),(99000,657,128,'9900',4,'9999',99999),(0,658,129,'00',2,'89',89999),(90000,659,129,'900',3,'989',98999),(99000,660,129,'9900',4,'9999',99999),(0,661,130,'00',2,'29',29999),(30000,662,130,'300',3,'399',39999),(40000,663,130,'40',2,'94',94999),(95000,664,130,'950',3,'989',98999),(99000,665,130,'9900',4,'9999',99999),(0,666,131,'0',1,'4',49999),(50000,667,131,'50',2,'64',64999),(65000,668,131,'650',3,'659',65999),(66000,669,131,'66',2,'75',75999),(76000,670,131,'760',3,'899',89999),(90000,671,131,'9000',4,'9999',99999),(0,672,132,'0',1,'3',39999),(40000,673,132,'40',2,'89',89999),(90000,674,132,'900',3,'989',98999),(99000,675,132,'9900',4,'9999',99999),(0,676,133,'00',2,'09',9999),(10000,677,133,'100',3,'159',15999),(16000,678,133,'1600',4,'1999',19999),(20000,679,133,'20',2,'79',79999),(80000,680,133,'800',3,'949',94999),(95000,681,133,'9500',4,'9999',99999),(0,682,134,'00',2,'79',79999),(80000,683,134,'800',3,'989',98999),(99000,684,134,'9900',4,'9999',99999),(80000,685,135,'80',2,'94',94999),(95000,686,135,'950',3,'989',98999),(99000,687,135,'9900',4,'9999',99999),(0,688,136,'00',2,'49',49999),(50000,689,136,'500',3,'899',89999),(90000,690,136,'9000',4,'9999',99999),(0,691,137,'0',1,'4',49999),(50000,692,137,'50',2,'79',79999),(80000,693,137,'800',3,'899',89999),(90000,694,137,'9000',4,'9999',99999),(0,695,138,'00',2,'39',39999),(40000,696,138,'400',3,'899',89999),(90000,697,138,'9000',4,'9399',93999),(94000,698,138,'940',3,'969',96999),(97000,699,138,'97',2,'99',99999),(0,700,139,'00',2,'39',39999),(40000,701,139,'400',3,'879',87999),(88000,702,139,'8800',4,'9999',99999),(0,703,140,'0',1,'2',29999),(30000,704,140,'30',2,'54',54999),(55000,705,140,'550',3,'749',74999),(75000,706,140,'7500',4,'9999',99999),(0,707,141,'0',1,'0',9999),(10000,708,141,'100',3,'199',19999),(20000,709,141,'2000',4,'2999',29999),(30000,710,141,'30',2,'59',59999),(60000,711,141,'600',3,'949',94999),(95000,712,141,'9500',4,'9999',99999),(0,713,142,'00',2,'49',49999),(50000,714,142,'500',3,'799',79999),(80000,715,142,'80',2,'99',99999),(0,716,144,'0',1,'1',19999),(20000,717,144,'20',2,'89',89999),(90000,718,144,'900',3,'999',99999),(0,719,145,'0',1,'5',59999),(60000,720,145,'60',2,'89',89999),(90000,721,145,'900',3,'999',99999),(0,722,146,'0',1,'3',39999),(40000,723,146,'40',2,'79',79999),(80000,724,146,'800',3,'999',99999),(0,725,147,'0',1,'2',29999),(30000,726,147,'30',2,'59',59999),(60000,727,147,'600',3,'699',69999),(70000,728,147,'70',2,'89',89999),(90000,729,147,'90',2,'94',94999),(95000,730,147,'950',3,'999',99999),(0,731,148,'0',1,'0',9999),(10000,732,148,'10',2,'89',89999),(90000,733,148,'900',3,'999',99999),(0,734,149,'0',1,'3',39999),(40000,735,149,'40',2,'94',94999),(95000,736,149,'950',3,'999',99999),(0,737,150,'0',1,'2',29999),(30000,738,150,'30',2,'89',89999),(90000,739,150,'900',3,'999',99999),(0,740,151,'00',2,'59',59999),(60000,741,151,'600',3,'999',99999),(0,742,152,'0',1,'3',39999),(40000,743,152,'400',3,'599',59999),(60000,744,152,'60',2,'89',89999),(90000,745,152,'900',3,'999',99999),(0,746,153,'0',1,'2',29999),(30000,747,153,'30',2,'35',35999),(60000,748,153,'600',3,'604',60499),(0,749,154,'0',1,'4',49999),(50000,750,154,'50',2,'89',89999),(90000,751,154,'900',3,'999',99999),(0,752,155,'0',1,'4',49999),(50000,753,155,'50',2,'79',79999),(80000,754,155,'800',3,'999',99999),(0,755,156,'0',1,'2',29999),(30000,756,156,'30',2,'69',69999),(70000,757,156,'700',3,'999',99999),(0,758,157,'0',1,'2',29999),(30000,759,157,'30',2,'89',89999),(90000,760,157,'900',3,'999',99999),(0,761,158,'0',1,'3',39999),(40000,762,158,'40',2,'79',79999),(80000,763,158,'800',3,'999',99999),(0,764,159,'0',1,'2',29999),(30000,765,159,'300',3,'399',39999),(40000,766,159,'40',2,'69',69999),(90000,767,159,'900',3,'999',99999),(0,768,160,'0',1,'4',49999),(50000,769,160,'50',2,'89',89999),(90000,770,160,'900',3,'999',99999),(0,771,161,'0',1,'1',19999),(20000,772,161,'20',2,'69',69999),(70000,773,161,'700',3,'799',79999),(80000,774,161,'8',1,'8',89999),(90000,775,161,'90',2,'99',99999),(0,776,162,'0',1,'3',39999),(40000,777,162,'40',2,'69',69999),(70000,778,162,'700',3,'999',99999),(0,779,163,'0',1,'1',19999),(20000,780,163,'20',2,'79',79999),(80000,781,163,'800',3,'999',99999),(0,782,164,'0',1,'1',19999),(20000,783,164,'20',2,'79',79999),(80000,784,164,'800',3,'999',99999),(0,785,165,'0',1,'3',39999),(40000,786,165,'40',2,'79',79999),(80000,787,165,'800',3,'999',99999),(0,788,166,'0',1,'0',9999),(10000,789,166,'10',2,'59',59999),(60000,790,166,'600',3,'999',99999),(0,791,167,'0',1,'2',29999),(30000,792,167,'30',2,'59',59999),(60000,793,167,'600',3,'999',99999),(0,794,168,'0',1,'0',9999),(10000,795,168,'10',2,'79',79999),(80000,796,168,'800',3,'999',99999),(0,797,169,'0',1,'4',49999),(50000,798,169,'50',2,'79',79999),(80000,799,169,'800',3,'999',99999),(0,800,170,'0',1,'4',49999),(50000,801,170,'50',2,'79',79999),(80000,802,170,'800',3,'999',99999),(0,803,171,'0',1,'4',49999),(50000,804,171,'50',2,'79',79999),(80000,805,171,'800',3,'999',99999),(0,806,172,'0',1,'0',9999),(10000,807,172,'10',2,'59',59999),(60000,808,172,'600',3,'699',69999),(70000,809,172,'7',1,'7',79999),(80000,810,172,'80',2,'99',99999),(0,811,173,'0',1,'2',29999),(30000,812,173,'30',2,'59',59999),(60000,813,173,'600',3,'999',99999),(0,814,174,'0',1,'1',19999),(20000,815,174,'20',2,'79',79999),(80000,816,174,'800',3,'999',99999),(0,817,175,'0',1,'2',29999),(30000,818,175,'30',2,'59',59999),(60000,819,175,'600',3,'699',69999),(70000,820,175,'7',1,'8',89999),(90000,821,175,'90',2,'99',99999),(0,822,176,'0',1,'0',9999),(10000,823,176,'10',2,'59',59999),(60000,824,176,'600',3,'999',99999),(0,825,177,'0',1,'1',19999),(20000,826,177,'20',2,'59',59999),(60000,827,177,'600',3,'999',99999),(0,828,178,'0',1,'1',19999),(20000,829,178,'20',2,'59',59999),(60000,830,178,'600',3,'899',89999),(90000,831,178,'90',2,'99',99999),(0,832,179,'0',1,'5',59999),(60000,833,179,'60',2,'89',89999),(90000,834,179,'900',3,'999',99999),(0,835,180,'0',1,'0',9999),(10000,836,180,'10',2,'69',69999),(70000,837,180,'700',3,'999',99999),(0,838,181,'0',1,'2',29999),(30000,839,181,'30',2,'79',79999),(80000,840,181,'800',3,'999',99999),(0,841,182,'0',1,'4',49999),(50000,842,182,'50',2,'79',79999),(80000,843,182,'800',3,'999',99999),(0,844,183,'0',1,'2',29999),(30000,845,183,'30',2,'59',59999),(60000,846,183,'600',3,'999',99999),(0,847,184,'0',1,'4',49999),(50000,848,184,'50',2,'79',79999),(80000,849,184,'800',3,'999',99999),(0,850,185,'0',1,'5',59999),(60000,851,185,'60',2,'89',89999),(90000,852,185,'900',3,'999',99999),(0,853,186,'0',1,'2',29999),(30000,854,186,'30',2,'59',59999),(60000,855,186,'600',3,'999',99999),(0,856,187,'0',1,'2',29999),(30000,857,187,'30',2,'69',69999),(70000,858,187,'700',3,'999',99999),(0,859,188,'0',1,'4',49999),(50000,860,188,'50',2,'79',79999),(80000,861,188,'800',3,'999',99999),(0,862,189,'0',1,'1',19999),(20000,863,189,'20',2,'89',89999),(90000,864,189,'900',3,'999',99999),(0,865,190,'0',1,'4',49999),(50000,866,190,'50',2,'79',79999),(80000,867,190,'800',3,'999',99999),(0,868,192,'0',1,'4',49999),(50000,869,192,'50',2,'79',79999),(80000,870,192,'800',3,'999',99999),(0,871,193,'0',1,'2',29999),(30000,872,193,'30',2,'79',79999),(80000,873,193,'800',3,'939',93999),(94000,874,193,'94',2,'99',99999),(0,875,194,'0',1,'2',29999),(30000,876,194,'30',2,'69',69999),(70000,877,194,'700',3,'999',99999),(0,878,195,'0',1,'1',19999),(20000,879,195,'20',2,'59',59999),(60000,880,195,'600',3,'799',79999),(80000,881,195,'80',2,'89',89999),(90000,882,195,'90',2,'99',99999),(0,883,196,'00',2,'59',59999),(60000,884,196,'600',3,'859',85999),(86000,885,196,'86',2,'99',99999),(0,886,197,'0',1,'1',19999),(20000,887,197,'20',2,'79',79999),(80000,888,197,'800',3,'999',99999),(0,889,198,'0',1,'4',49999),(50000,890,198,'50',2,'94',94999),(95000,891,198,'950',3,'999',99999),(0,892,199,'0',1,'2',29999),(30000,893,199,'30',2,'59',59999),(60000,894,199,'600',3,'999',99999),(0,895,200,'0',1,'0',9999),(10000,896,200,'10',2,'94',94999),(95000,897,200,'950',3,'999',99999),(0,898,201,'0',1,'3',39999),(40000,899,201,'40',2,'89',89999),(90000,900,201,'900',3,'999',99999),(0,901,202,'0',1,'4',49999),(50000,902,202,'50',2,'79',79999),(80000,903,202,'800',3,'999',99999),(0,904,203,'00',2,'49',49999),(50000,905,203,'500',3,'999',99999),(0,906,204,'0',1,'1',19999),(20000,907,204,'20',2,'79',79999),(80000,908,204,'800',3,'999',99999),(0,909,205,'0',1,'3',39999),(40000,910,205,'40',2,'79',79999),(80000,911,205,'800',3,'999',99999),(0,912,206,'0',1,'2',29999),(30000,913,206,'30',2,'69',69999),(70000,914,206,'700',3,'799',79999),(0,915,207,'0',1,'1',19999),(20000,916,207,'20',2,'59',59999),(60000,917,207,'600',3,'899',89999),(0,918,208,'0',1,'3',39999),(40000,919,208,'400',3,'599',59999),(60000,920,208,'60',2,'89',89999),(90000,921,208,'900',3,'999',99999),(0,922,209,'00',2,'19',19999),(20000,923,209,'200',3,'699',69999),(70000,924,209,'7000',4,'8999',89999),(90000,925,209,'90000',5,'97599',97599),(97600,926,209,'976000',6,'999999',99999);
/*!40000 ALTER TABLE `ezisbn_registrant_range` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezkeyword`
--

DROP TABLE IF EXISTS `ezkeyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezkeyword` (
  `class_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezkeyword_keyword` (`keyword`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezkeyword`
--

LOCK TABLES `ezkeyword` WRITE;
/*!40000 ALTER TABLE `ezkeyword` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezkeyword` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezkeyword_attribute_link`
--

DROP TABLE IF EXISTS `ezkeyword_attribute_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezkeyword_attribute_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword_id` int(11) NOT NULL DEFAULT '0',
  `objectattribute_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezkeyword_attr_link_kid_oaid` (`keyword_id`,`objectattribute_id`),
  KEY `ezkeyword_attr_link_oaid` (`objectattribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezkeyword_attribute_link`
--

LOCK TABLES `ezkeyword_attribute_link` WRITE;
/*!40000 ALTER TABLE `ezkeyword_attribute_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezkeyword_attribute_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezm_block`
--

DROP TABLE IF EXISTS `ezm_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezm_block` (
  `id` char(32) NOT NULL,
  `zone_id` char(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `node_id` int(10) unsigned NOT NULL,
  `overflow_id` char(32) DEFAULT NULL,
  `last_update` int(10) unsigned DEFAULT '0',
  `block_type` varchar(255) DEFAULT NULL,
  `fetch_params` longtext,
  `rotation_type` int(10) unsigned DEFAULT NULL,
  `rotation_interval` int(10) unsigned DEFAULT NULL,
  `is_removed` int(2) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezm_block__is_removed` (`is_removed`),
  KEY `ezm_block__node_id` (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezm_block`
--

LOCK TABLES `ezm_block` WRITE;
/*!40000 ALTER TABLE `ezm_block` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezm_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezm_pool`
--

DROP TABLE IF EXISTS `ezm_pool`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezm_pool` (
  `block_id` char(32) NOT NULL,
  `object_id` int(10) unsigned NOT NULL,
  `node_id` int(10) unsigned NOT NULL,
  `priority` int(10) unsigned DEFAULT '0',
  `ts_publication` int(11) DEFAULT '0',
  `ts_visible` int(10) unsigned DEFAULT '0',
  `ts_hidden` int(10) unsigned DEFAULT '0',
  `rotation_until` int(10) unsigned DEFAULT '0',
  `moved_to` char(32) DEFAULT NULL,
  PRIMARY KEY (`block_id`,`object_id`),
  KEY `ezm_pool__block_id__ts_publication__priority` (`block_id`,`ts_publication`,`priority`),
  KEY `ezm_pool__block_id__ts_visible` (`block_id`,`ts_visible`),
  KEY `ezm_pool__block_id__ts_hidden` (`block_id`,`ts_hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezm_pool`
--

LOCK TABLES `ezm_pool` WRITE;
/*!40000 ALTER TABLE `ezm_pool` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezm_pool` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezmedia`
--

DROP TABLE IF EXISTS `ezmedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezmedia` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT '0',
  `controls` varchar(50) DEFAULT NULL,
  `filename` varchar(255) NOT NULL DEFAULT '',
  `has_controller` int(11) DEFAULT '0',
  `height` int(11) DEFAULT NULL,
  `is_autoplay` int(11) DEFAULT '0',
  `is_loop` int(11) DEFAULT '0',
  `mime_type` varchar(50) NOT NULL DEFAULT '',
  `original_filename` varchar(255) NOT NULL DEFAULT '',
  `pluginspage` varchar(255) DEFAULT NULL,
  `quality` varchar(50) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `width` int(11) DEFAULT NULL,
  PRIMARY KEY (`contentobject_attribute_id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezmedia`
--

LOCK TABLES `ezmedia` WRITE;
/*!40000 ALTER TABLE `ezmedia` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezmedia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezmessage`
--

DROP TABLE IF EXISTS `ezmessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezmessage` (
  `body` longtext,
  `destination_address` varchar(50) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_sent` int(11) NOT NULL DEFAULT '0',
  `send_method` varchar(50) NOT NULL DEFAULT '',
  `send_time` varchar(50) NOT NULL DEFAULT '',
  `send_weekday` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezmessage`
--

LOCK TABLES `ezmessage` WRITE;
/*!40000 ALTER TABLE `ezmessage` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezmessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezmodule_run`
--

DROP TABLE IF EXISTS `ezmodule_run`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezmodule_run` (
  `function_name` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_data` longtext,
  `module_name` varchar(255) DEFAULT NULL,
  `workflow_process_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ezmodule_run_workflow_process_id_s` (`workflow_process_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezmodule_run`
--

LOCK TABLES `ezmodule_run` WRITE;
/*!40000 ALTER TABLE `ezmodule_run` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezmodule_run` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezmultipricedata`
--

DROP TABLE IF EXISTS `ezmultipricedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezmultipricedata` (
  `contentobject_attr_id` int(11) NOT NULL DEFAULT '0',
  `contentobject_attr_version` int(11) NOT NULL DEFAULT '0',
  `currency_code` varchar(4) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL DEFAULT '0',
  `value` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `ezmultipricedata_coa_id` (`contentobject_attr_id`),
  KEY `ezmultipricedata_coa_version` (`contentobject_attr_version`),
  KEY `ezmultipricedata_currency_code` (`currency_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezmultipricedata`
--

LOCK TABLES `ezmultipricedata` WRITE;
/*!40000 ALTER TABLE `ezmultipricedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezmultipricedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eznode_assignment`
--

DROP TABLE IF EXISTS `eznode_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eznode_assignment` (
  `contentobject_id` int(11) DEFAULT NULL,
  `contentobject_version` int(11) DEFAULT NULL,
  `from_node_id` int(11) DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_main` int(11) NOT NULL DEFAULT '0',
  `op_code` int(11) NOT NULL DEFAULT '0',
  `parent_node` int(11) DEFAULT NULL,
  `parent_remote_id` varchar(100) NOT NULL DEFAULT '',
  `remote_id` varchar(100) NOT NULL DEFAULT '0',
  `sort_field` int(11) DEFAULT '1',
  `sort_order` int(11) DEFAULT '1',
  `priority` int(11) NOT NULL DEFAULT '0',
  `is_hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `eznode_assignment_co_version` (`contentobject_version`),
  KEY `eznode_assignment_coid_cov` (`contentobject_id`,`contentobject_version`),
  KEY `eznode_assignment_is_main` (`is_main`),
  KEY `eznode_assignment_parent_node` (`parent_node`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eznode_assignment`
--

LOCK TABLES `eznode_assignment` WRITE;
/*!40000 ALTER TABLE `eznode_assignment` DISABLE KEYS */;
INSERT INTO `eznode_assignment` VALUES (8,2,0,4,1,2,5,'','0',1,1,0,0),(42,1,0,5,1,2,5,'','0',9,1,0,0),(10,2,-1,6,1,2,44,'','0',9,1,0,0),(4,1,0,7,1,2,1,'','0',1,1,0,0),(12,1,0,8,1,2,5,'','0',1,1,0,0),(13,1,0,9,1,2,5,'','0',1,1,0,0),(41,1,0,11,1,2,1,'','0',1,1,0,0),(11,1,0,12,1,2,5,'','0',1,1,0,0),(45,1,-1,16,1,2,1,'','0',9,1,0,0),(49,1,0,27,1,2,43,'','0',9,1,0,0),(50,1,0,28,1,2,43,'','0',9,1,0,0),(51,1,0,29,1,2,43,'','0',9,1,0,0),(56,1,0,34,1,2,1,'','0',2,0,0,0),(14,3,-1,38,1,2,13,'','0',1,1,0,0),(54,2,-1,39,1,2,58,'','0',1,1,0,0),(53,1,0,41,1,2,2,'4d1f19db5e04d25db212711d94ea9eee','0',9,1,0,0),(54,1,0,42,1,2,2,'d03e7b304979ff28508ea7801ea71929','0',9,1,0,0),(55,1,0,43,1,2,43,'3c9e8345a58330a6cf5b41adc9481401','0',9,1,0,0),(56,1,0,44,1,5,59,'23fb5eb16812844fcd1d6e977529e293','0',9,1,0,0),(57,1,0,45,1,2,43,'46a7da2c2fafe76cfb05d74e19eecc32','0',9,1,0,0),(58,3,0,46,1,2,59,'bbbdd3978b8504c7d171a5430dd6da46','0',9,1,0,0),(59,1,0,47,1,2,43,'6d19ab1e3ed923784a1f5c44f46cf630','0',9,1,0,0),(60,1,0,48,1,2,61,'a0a247ca8b7ea154576f2e0758b4593c','0',9,1,0,0),(61,1,0,49,1,2,61,'c52a15055ae7e688fe3f00b06d07ddb3','0',9,1,0,0),(62,1,0,50,1,2,61,'7723054b2334aee049154dab18290ad2','0',9,1,0,0),(63,1,0,51,1,2,61,'addcd18c60c6a7c7622764027435b6e4','0',9,1,0,0),(64,1,0,52,1,2,61,'1226ab890a820487a3dbe01000382ed0','0',9,1,0,0);
/*!40000 ALTER TABLE `eznode_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eznotificationcollection`
--

DROP TABLE IF EXISTS `eznotificationcollection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eznotificationcollection` (
  `data_subject` longtext NOT NULL,
  `data_text` longtext NOT NULL,
  `event_id` int(11) NOT NULL DEFAULT '0',
  `handler` varchar(255) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transport` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eznotificationcollection`
--

LOCK TABLES `eznotificationcollection` WRITE;
/*!40000 ALTER TABLE `eznotificationcollection` DISABLE KEYS */;
/*!40000 ALTER TABLE `eznotificationcollection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eznotificationcollection_item`
--

DROP TABLE IF EXISTS `eznotificationcollection_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eznotificationcollection_item` (
  `address` varchar(255) NOT NULL DEFAULT '',
  `collection_id` int(11) NOT NULL DEFAULT '0',
  `event_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `send_date` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eznotificationcollection_item`
--

LOCK TABLES `eznotificationcollection_item` WRITE;
/*!40000 ALTER TABLE `eznotificationcollection_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `eznotificationcollection_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eznotificationevent`
--

DROP TABLE IF EXISTS `eznotificationevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eznotificationevent` (
  `data_int1` int(11) NOT NULL DEFAULT '0',
  `data_int2` int(11) NOT NULL DEFAULT '0',
  `data_int3` int(11) NOT NULL DEFAULT '0',
  `data_int4` int(11) NOT NULL DEFAULT '0',
  `data_text1` longtext NOT NULL,
  `data_text2` longtext NOT NULL,
  `data_text3` longtext NOT NULL,
  `data_text4` longtext NOT NULL,
  `event_type_string` varchar(255) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eznotificationevent`
--

LOCK TABLES `eznotificationevent` WRITE;
/*!40000 ALTER TABLE `eznotificationevent` DISABLE KEYS */;
/*!40000 ALTER TABLE `eznotificationevent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezoperation_memento`
--

DROP TABLE IF EXISTS `ezoperation_memento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezoperation_memento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `main` int(11) NOT NULL DEFAULT '0',
  `main_key` varchar(32) NOT NULL DEFAULT '',
  `memento_data` longtext NOT NULL,
  `memento_key` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`memento_key`),
  KEY `ezoperation_memento_memento_key_main` (`memento_key`,`main`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezoperation_memento`
--

LOCK TABLES `ezoperation_memento` WRITE;
/*!40000 ALTER TABLE `ezoperation_memento` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezoperation_memento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezorder`
--

DROP TABLE IF EXISTS `ezorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezorder` (
  `account_identifier` varchar(100) NOT NULL DEFAULT 'default',
  `created` int(11) NOT NULL DEFAULT '0',
  `data_text_1` longtext,
  `data_text_2` longtext,
  `email` varchar(150) DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ignore_vat` int(11) NOT NULL DEFAULT '0',
  `is_archived` int(11) NOT NULL DEFAULT '0',
  `is_temporary` int(11) NOT NULL DEFAULT '1',
  `order_nr` int(11) NOT NULL DEFAULT '0',
  `productcollection_id` int(11) NOT NULL DEFAULT '0',
  `status_id` int(11) DEFAULT '0',
  `status_modified` int(11) DEFAULT '0',
  `status_modifier_id` int(11) DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezorder_is_archived` (`is_archived`),
  KEY `ezorder_is_tmp` (`is_temporary`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezorder`
--

LOCK TABLES `ezorder` WRITE;
/*!40000 ALTER TABLE `ezorder` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezorder_item`
--

DROP TABLE IF EXISTS `ezorder_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezorder_item` (
  `description` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_vat_inc` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `price` float DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `vat_value` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezorder_item_order_id` (`order_id`),
  KEY `ezorder_item_type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezorder_item`
--

LOCK TABLES `ezorder_item` WRITE;
/*!40000 ALTER TABLE `ezorder_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezorder_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezorder_nr_incr`
--

DROP TABLE IF EXISTS `ezorder_nr_incr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezorder_nr_incr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezorder_nr_incr`
--

LOCK TABLES `ezorder_nr_incr` WRITE;
/*!40000 ALTER TABLE `ezorder_nr_incr` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezorder_nr_incr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezorder_status`
--

DROP TABLE IF EXISTS `ezorder_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezorder_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL DEFAULT '',
  `status_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezorder_status_active` (`is_active`),
  KEY `ezorder_status_name` (`name`),
  KEY `ezorder_status_sid` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezorder_status`
--

LOCK TABLES `ezorder_status` WRITE;
/*!40000 ALTER TABLE `ezorder_status` DISABLE KEYS */;
INSERT INTO `ezorder_status` VALUES (1,1,'Pending',1),(2,1,'Processing',2),(3,1,'Delivered',3);
/*!40000 ALTER TABLE `ezorder_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezorder_status_history`
--

DROP TABLE IF EXISTS `ezorder_status_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezorder_status_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modified` int(11) NOT NULL DEFAULT '0',
  `modifier_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `status_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezorder_status_history_mod` (`modified`),
  KEY `ezorder_status_history_oid` (`order_id`),
  KEY `ezorder_status_history_sid` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezorder_status_history`
--

LOCK TABLES `ezorder_status_history` WRITE;
/*!40000 ALTER TABLE `ezorder_status_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezorder_status_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpackage`
--

DROP TABLE IF EXISTS `ezpackage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpackage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `install_date` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `version` varchar(30) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpackage`
--

LOCK TABLES `ezpackage` WRITE;
/*!40000 ALTER TABLE `ezpackage` DISABLE KEYS */;
INSERT INTO `ezpackage` VALUES (1,1301057838,'plain_site_data','1.0-1');
/*!40000 ALTER TABLE `ezpackage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpaymentobject`
--

DROP TABLE IF EXISTS `ezpaymentobject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpaymentobject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `payment_string` varchar(255) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `workflowprocess_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpaymentobject`
--

LOCK TABLES `ezpaymentobject` WRITE;
/*!40000 ALTER TABLE `ezpaymentobject` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezpaymentobject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpdf_export`
--

DROP TABLE IF EXISTS `ezpdf_export`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpdf_export` (
  `created` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `export_classes` varchar(255) DEFAULT NULL,
  `export_structure` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intro_text` longtext,
  `modified` int(11) DEFAULT NULL,
  `modifier_id` int(11) DEFAULT NULL,
  `pdf_filename` varchar(255) DEFAULT NULL,
  `show_frontpage` int(11) DEFAULT NULL,
  `site_access` varchar(255) DEFAULT NULL,
  `source_node_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `sub_text` longtext,
  `title` varchar(255) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpdf_export`
--

LOCK TABLES `ezpdf_export` WRITE;
/*!40000 ALTER TABLE `ezpdf_export` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezpdf_export` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpending_actions`
--

DROP TABLE IF EXISTS `ezpending_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpending_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(64) NOT NULL DEFAULT '',
  `created` int(11) DEFAULT NULL,
  `param` longtext,
  PRIMARY KEY (`id`),
  KEY `ezpending_actions_action` (`action`),
  KEY `ezpending_actions_created` (`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpending_actions`
--

LOCK TABLES `ezpending_actions` WRITE;
/*!40000 ALTER TABLE `ezpending_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezpending_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpolicy`
--

DROP TABLE IF EXISTS `ezpolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpolicy` (
  `function_name` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) DEFAULT NULL,
  `original_id` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezpolicy_original_id` (`original_id`),
  KEY `ezpolicy_role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=346 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpolicy`
--

LOCK TABLES `ezpolicy` WRITE;
/*!40000 ALTER TABLE `ezpolicy` DISABLE KEYS */;
INSERT INTO `ezpolicy` VALUES ('*',317,'content',0,3),('login',319,'user',0,3),('read',333,'content',0,4),('*',340,'url',0,3),('*',343,'*',0,2),('read',344,'content',0,1),('login',345,'user',0,1);
/*!40000 ALTER TABLE `ezpolicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpolicy_limitation`
--

DROP TABLE IF EXISTS `ezpolicy_limitation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpolicy_limitation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) NOT NULL DEFAULT '',
  `policy_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `policy_id` (`policy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=262 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpolicy_limitation`
--

LOCK TABLES `ezpolicy_limitation` WRITE;
/*!40000 ALTER TABLE `ezpolicy_limitation` DISABLE KEYS */;
INSERT INTO `ezpolicy_limitation` VALUES (252,'Section',329),(254,'Class',333),(255,'Owner',333),(260,'SiteAccess',345),(261,'Section',344);
/*!40000 ALTER TABLE `ezpolicy_limitation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpolicy_limitation_value`
--

DROP TABLE IF EXISTS `ezpolicy_limitation_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpolicy_limitation_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `limitation_id` int(11) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezpolicy_limitation_value_val` (`value`),
  KEY `ezpolicy_limit_value_limit_id` (`limitation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=493 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpolicy_limitation_value`
--

LOCK TABLES `ezpolicy_limitation_value` WRITE;
/*!40000 ALTER TABLE `ezpolicy_limitation_value` DISABLE KEYS */;
INSERT INTO `ezpolicy_limitation_value` VALUES (478,252,'1'),(480,254,'4'),(481,255,'1'),(488,260,'4082745666'),(489,260,'3430272718'),(490,260,'1766001124'),(491,261,'3'),(492,261,'1');
/*!40000 ALTER TABLE `ezpolicy_limitation_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpreferences`
--

DROP TABLE IF EXISTS `ezpreferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpreferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `value` longtext,
  PRIMARY KEY (`id`),
  KEY `ezpreferences_name` (`name`),
  KEY `ezpreferences_user_id_idx` (`user_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpreferences`
--

LOCK TABLES `ezpreferences` WRITE;
/*!40000 ALTER TABLE `ezpreferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezpreferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezprest_authcode`
--

DROP TABLE IF EXISTS `ezprest_authcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezprest_authcode` (
  `client_id` varchar(200) NOT NULL DEFAULT '',
  `expirytime` bigint(20) NOT NULL DEFAULT '0',
  `id` varchar(200) NOT NULL DEFAULT '',
  `scope` varchar(200) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `authcode_client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezprest_authcode`
--

LOCK TABLES `ezprest_authcode` WRITE;
/*!40000 ALTER TABLE `ezprest_authcode` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezprest_authcode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezprest_authorized_clients`
--

DROP TABLE IF EXISTS `ezprest_authorized_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezprest_authorized_clients` (
  `created` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_client_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_user` (`rest_client_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezprest_authorized_clients`
--

LOCK TABLES `ezprest_authorized_clients` WRITE;
/*!40000 ALTER TABLE `ezprest_authorized_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezprest_authorized_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezprest_clients`
--

DROP TABLE IF EXISTS `ezprest_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezprest_clients` (
  `client_id` varchar(200) DEFAULT NULL,
  `client_secret` varchar(200) DEFAULT NULL,
  `created` int(11) NOT NULL DEFAULT '0',
  `description` longtext,
  `endpoint_uri` varchar(200) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `updated` int(11) NOT NULL DEFAULT '0',
  `version` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `client_id_unique` (`client_id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezprest_clients`
--

LOCK TABLES `ezprest_clients` WRITE;
/*!40000 ALTER TABLE `ezprest_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezprest_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezprest_token`
--

DROP TABLE IF EXISTS `ezprest_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezprest_token` (
  `client_id` varchar(200) NOT NULL DEFAULT '',
  `expirytime` bigint(20) NOT NULL DEFAULT '0',
  `id` varchar(200) NOT NULL DEFAULT '',
  `refresh_token` varchar(200) NOT NULL DEFAULT '',
  `scope` varchar(200) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `token_client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezprest_token`
--

LOCK TABLES `ezprest_token` WRITE;
/*!40000 ALTER TABLE `ezprest_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezprest_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezproductcategory`
--

DROP TABLE IF EXISTS `ezproductcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezproductcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezproductcategory`
--

LOCK TABLES `ezproductcategory` WRITE;
/*!40000 ALTER TABLE `ezproductcategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezproductcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezproductcollection`
--

DROP TABLE IF EXISTS `ezproductcollection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezproductcollection` (
  `created` int(11) DEFAULT NULL,
  `currency_code` varchar(4) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezproductcollection`
--

LOCK TABLES `ezproductcollection` WRITE;
/*!40000 ALTER TABLE `ezproductcollection` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezproductcollection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezproductcollection_item`
--

DROP TABLE IF EXISTS `ezproductcollection_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezproductcollection_item` (
  `contentobject_id` int(11) NOT NULL DEFAULT '0',
  `discount` float DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_vat_inc` int(11) DEFAULT NULL,
  `item_count` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `price` float DEFAULT '0',
  `productcollection_id` int(11) NOT NULL DEFAULT '0',
  `vat_value` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezproductcollection_item_contentobject_id` (`contentobject_id`),
  KEY `ezproductcollection_item_productcollection_id` (`productcollection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezproductcollection_item`
--

LOCK TABLES `ezproductcollection_item` WRITE;
/*!40000 ALTER TABLE `ezproductcollection_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezproductcollection_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezproductcollection_item_opt`
--

DROP TABLE IF EXISTS `ezproductcollection_item_opt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezproductcollection_item_opt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `object_attribute_id` int(11) DEFAULT NULL,
  `option_item_id` int(11) NOT NULL DEFAULT '0',
  `price` float NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ezproductcollection_item_opt_item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezproductcollection_item_opt`
--

LOCK TABLES `ezproductcollection_item_opt` WRITE;
/*!40000 ALTER TABLE `ezproductcollection_item_opt` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezproductcollection_item_opt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpublishingqueueprocesses`
--

DROP TABLE IF EXISTS `ezpublishingqueueprocesses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpublishingqueueprocesses` (
  `created` int(11) DEFAULT NULL,
  `ezcontentobject_version_id` int(11) NOT NULL DEFAULT '0',
  `finished` int(11) DEFAULT NULL,
  `pid` int(8) DEFAULT NULL,
  `started` int(11) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`ezcontentobject_version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpublishingqueueprocesses`
--

LOCK TABLES `ezpublishingqueueprocesses` WRITE;
/*!40000 ALTER TABLE `ezpublishingqueueprocesses` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezpublishingqueueprocesses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezrole`
--

DROP TABLE IF EXISTS `ezrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezrole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_new` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` char(1) DEFAULT NULL,
  `version` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezrole`
--

LOCK TABLES `ezrole` WRITE;
/*!40000 ALTER TABLE `ezrole` DISABLE KEYS */;
INSERT INTO `ezrole` VALUES (1,0,'Anonymous','0',0),(2,0,'Administrator','0',0),(3,0,'Editor','',0),(4,0,'Member','',0);
/*!40000 ALTER TABLE `ezrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezrss_export`
--

DROP TABLE IF EXISTS `ezrss_export`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezrss_export` (
  `access_url` varchar(255) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `description` longtext,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) DEFAULT NULL,
  `main_node_only` int(11) NOT NULL DEFAULT '1',
  `modified` int(11) DEFAULT NULL,
  `modifier_id` int(11) DEFAULT NULL,
  `node_id` int(11) DEFAULT NULL,
  `number_of_objects` int(11) NOT NULL DEFAULT '0',
  `rss_version` varchar(255) DEFAULT NULL,
  `site_access` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezrss_export`
--

LOCK TABLES `ezrss_export` WRITE;
/*!40000 ALTER TABLE `ezrss_export` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezrss_export` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezrss_export_item`
--

DROP TABLE IF EXISTS `ezrss_export_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezrss_export_item` (
  `category` varchar(255) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `enclosure` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rssexport_id` int(11) DEFAULT NULL,
  `source_node_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `subnodes` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`status`),
  KEY `ezrss_export_rsseid` (`rssexport_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezrss_export_item`
--

LOCK TABLES `ezrss_export_item` WRITE;
/*!40000 ALTER TABLE `ezrss_export_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezrss_export_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezrss_import`
--

DROP TABLE IF EXISTS `ezrss_import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezrss_import` (
  `active` int(11) DEFAULT NULL,
  `class_description` varchar(255) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `class_title` varchar(255) DEFAULT NULL,
  `class_url` varchar(255) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `destination_node_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `import_description` longtext NOT NULL,
  `modified` int(11) DEFAULT NULL,
  `modifier_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `object_owner_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `url` longtext,
  PRIMARY KEY (`id`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezrss_import`
--

LOCK TABLES `ezrss_import` WRITE;
/*!40000 ALTER TABLE `ezrss_import` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezrss_import` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezscheduled_script`
--

DROP TABLE IF EXISTS `ezscheduled_script`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezscheduled_script` (
  `command` varchar(255) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last_report_timestamp` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `process_id` int(11) NOT NULL DEFAULT '0',
  `progress` int(3) DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezscheduled_script_timestamp` (`last_report_timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezscheduled_script`
--

LOCK TABLES `ezscheduled_script` WRITE;
/*!40000 ALTER TABLE `ezscheduled_script` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezscheduled_script` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsearch_object_word_link`
--

DROP TABLE IF EXISTS `ezsearch_object_word_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsearch_object_word_link` (
  `contentclass_attribute_id` int(11) NOT NULL DEFAULT '0',
  `contentclass_id` int(11) NOT NULL DEFAULT '0',
  `contentobject_id` int(11) NOT NULL DEFAULT '0',
  `frequency` float NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) NOT NULL DEFAULT '',
  `integer_value` int(11) NOT NULL DEFAULT '0',
  `next_word_id` int(11) NOT NULL DEFAULT '0',
  `placement` int(11) NOT NULL DEFAULT '0',
  `prev_word_id` int(11) NOT NULL DEFAULT '0',
  `published` int(11) NOT NULL DEFAULT '0',
  `section_id` int(11) NOT NULL DEFAULT '0',
  `word_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezsearch_object_word_link_frequency` (`frequency`),
  KEY `ezsearch_object_word_link_identifier` (`identifier`),
  KEY `ezsearch_object_word_link_integer_value` (`integer_value`),
  KEY `ezsearch_object_word_link_object` (`contentobject_id`),
  KEY `ezsearch_object_word_link_word` (`word_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7601 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsearch_object_word_link`
--

LOCK TABLES `ezsearch_object_word_link` WRITE;
/*!40000 ALTER TABLE `ezsearch_object_word_link` DISABLE KEYS */;
INSERT INTO `ezsearch_object_word_link` VALUES (7,3,4,0,4855,'description',0,1061,0,0,1033917596,2,1060),(7,3,4,0,4856,'description',0,1062,1,1060,1033917596,2,1061),(6,3,4,0,4857,'name',0,0,2,1061,1033917596,2,1062),(8,4,10,0,4858,'first_name',0,1029,0,0,1033920665,2,1063),(9,4,10,0,4859,'last_name',0,0,1,1063,1033920665,2,1029),(6,3,11,0,4860,'name',0,1065,0,0,1033920746,2,1064),(6,3,11,0,4861,'name',0,0,1,1064,1033920746,2,1065),(6,3,12,0,4862,'name',0,1062,0,0,1033920775,2,1066),(6,3,12,0,4863,'name',0,0,1,1066,1033920775,2,1062),(6,3,13,0,4864,'name',0,0,0,0,1033920794,2,1067),(8,4,14,0,4865,'first_name',0,1029,0,0,1033920830,2,1066),(9,4,14,0,4866,'last_name',0,0,1,1066,1033920830,2,1029),(4,1,41,0,4867,'name',0,0,0,0,1060695457,3,1068),(6,3,42,0,4868,'name',0,1062,0,0,1072180330,2,1063),(6,3,42,0,4869,'name',0,1029,1,1063,1072180330,2,1062),(7,3,42,0,4870,'description',0,1061,2,1062,1072180330,2,1029),(7,3,42,0,4871,'description',0,996,3,1029,1072180330,2,1061),(7,3,42,0,4872,'description',0,975,4,1061,1072180330,2,996),(7,3,42,0,4873,'description',0,1063,5,996,1072180330,2,975),(7,3,42,0,4874,'description',0,1029,6,975,1072180330,2,1063),(7,3,42,0,4875,'description',0,0,7,1063,1072180330,2,1029),(4,1,45,0,4876,'name',0,0,0,0,1079684190,4,984),(4,1,49,0,4877,'name',0,0,0,0,1080220197,3,1069),(4,1,50,0,4878,'name',0,0,0,0,1080220220,3,1070),(4,1,51,0,4879,'name',0,0,0,0,1080220233,3,1071),(181,13,53,0,5933,'title',0,1463,0,0,1521628862,1,1462),(181,13,53,0,5934,'title',0,1464,1,1462,1521628862,1,1463),(184,13,53,0,5935,'color_primary',0,1465,2,1463,1521628862,1,1464),(185,13,53,0,5936,'color_secondary',0,1466,3,1464,1521628862,1,1465),(186,13,53,0,5937,'email_contact',0,1050,4,1465,1521628862,1,1466),(186,13,53,0,5938,'email_contact',0,970,5,1466,1521628862,1,1050),(186,13,53,0,5939,'email_contact',0,1467,6,1050,1521628862,1,970),(186,13,53,0,5940,'email_contact',0,1050,7,970,1521628862,1,1467),(186,13,53,0,5941,'email_contact',0,1468,8,1467,1521628862,1,1050),(189,13,53,0,5942,'footer_address',0,1469,9,1050,1521628862,1,1468),(189,13,53,0,5943,'footer_address',0,1470,10,1468,1521628862,1,1469),(189,13,53,0,5944,'footer_address',0,1471,11,1469,1521628862,1,1470),(189,13,53,0,5945,'footer_address',0,1472,12,1470,1521628862,1,1471),(189,13,53,0,5946,'footer_address',0,0,13,1471,1521628862,1,1472),(195,15,54,0,5947,'title',0,1474,0,0,1521629667,1,1473),(196,15,54,0,5948,'subtitle',0,1473,1,1473,1521629667,1,1474),(196,15,54,0,5949,'subtitle',0,1335,2,1474,1521629667,1,1473),(198,15,54,0,5950,'intro',0,1336,3,1473,1521629667,1,1335),(198,15,54,0,5951,'intro',0,1337,4,1335,1521629667,1,1336),(198,15,54,0,5952,'intro',0,1338,5,1336,1521629667,1,1337),(198,15,54,0,5953,'intro',0,1339,6,1337,1521629667,1,1338),(198,15,54,0,5954,'intro',0,1340,7,1338,1521629667,1,1339),(198,15,54,0,5955,'intro',0,1341,8,1339,1521629667,1,1340),(198,15,54,0,5956,'intro',0,1342,9,1340,1521629667,1,1341),(198,15,54,0,5957,'intro',0,1343,10,1341,1521629667,1,1342),(198,15,54,0,5958,'intro',0,1344,11,1342,1521629667,1,1343),(198,15,54,0,5959,'intro',0,1345,12,1343,1521629667,1,1344),(198,15,54,0,5960,'intro',0,1346,13,1344,1521629667,1,1345),(198,15,54,0,5961,'intro',0,1338,14,1345,1521629667,1,1346),(198,15,54,0,5962,'intro',0,1339,15,1346,1521629667,1,1338),(198,15,54,0,5963,'intro',0,1347,16,1338,1521629667,1,1339),(198,15,54,0,5964,'intro',0,1348,17,1339,1521629667,1,1347),(198,15,54,0,5965,'intro',0,1338,18,1347,1521629667,1,1348),(198,15,54,0,5966,'intro',0,1339,19,1348,1521629667,1,1338),(198,15,54,0,5967,'intro',0,1349,20,1338,1521629667,1,1339),(198,15,54,0,5968,'intro',0,1350,21,1339,1521629667,1,1349),(198,15,54,0,5969,'intro',0,1351,22,1349,1521629667,1,1350),(198,15,54,0,5970,'intro',0,1352,23,1350,1521629667,1,1351),(198,15,54,0,5971,'intro',0,1353,24,1351,1521629667,1,1352),(198,15,54,0,5972,'intro',0,1354,25,1352,1521629667,1,1353),(198,15,54,0,5973,'intro',0,1355,26,1353,1521629667,1,1354),(198,15,54,0,5974,'intro',0,1356,27,1354,1521629667,1,1355),(198,15,54,0,5975,'intro',0,1357,28,1355,1521629667,1,1356),(198,15,54,0,5976,'intro',0,1357,29,1356,1521629667,1,1357),(198,15,54,0,5977,'intro',0,1358,30,1357,1521629667,1,1357),(198,15,54,0,5978,'intro',0,1348,31,1357,1521629667,1,1358),(198,15,54,0,5979,'intro',0,1359,32,1358,1521629667,1,1348),(198,15,54,0,5980,'intro',0,1360,33,1348,1521629667,1,1359),(198,15,54,0,5981,'intro',0,1361,34,1359,1521629667,1,1360),(198,15,54,0,5982,'intro',0,1362,35,1360,1521629667,1,1361),(198,15,54,0,5983,'intro',0,1363,36,1361,1521629667,1,1362),(198,15,54,0,5984,'intro',0,1364,37,1362,1521629667,1,1363),(198,15,54,0,5985,'intro',0,1365,38,1363,1521629667,1,1364),(198,15,54,0,5986,'intro',0,1366,39,1364,1521629667,1,1365),(198,15,54,0,5987,'intro',0,1367,40,1365,1521629667,1,1366),(198,15,54,0,5988,'intro',0,1368,41,1366,1521629667,1,1367),(198,15,54,0,5989,'intro',0,1369,42,1367,1521629667,1,1368),(198,15,54,0,5990,'intro',0,1370,43,1368,1521629667,1,1369),(198,15,54,0,5991,'intro',0,1371,44,1369,1521629667,1,1370),(198,15,54,0,5992,'intro',0,1372,45,1370,1521629667,1,1371),(198,15,54,0,5993,'intro',0,1370,46,1371,1521629667,1,1372),(198,15,54,0,5994,'intro',0,1373,47,1372,1521629667,1,1370),(198,15,54,0,5995,'intro',0,1353,48,1370,1521629667,1,1373),(198,15,54,0,5996,'intro',0,1374,49,1373,1521629667,1,1353),(198,15,54,0,5997,'intro',0,1375,50,1353,1521629667,1,1374),(198,15,54,0,5998,'intro',0,1376,51,1374,1521629667,1,1375),(198,15,54,0,5999,'intro',0,1377,52,1375,1521629667,1,1376),(198,15,54,0,6000,'intro',0,1364,53,1376,1521629667,1,1377),(198,15,54,0,6001,'intro',0,1378,54,1377,1521629667,1,1364),(198,15,54,0,6002,'intro',0,1379,55,1364,1521629667,1,1378),(198,15,54,0,6003,'intro',0,1353,56,1378,1521629667,1,1379),(198,15,54,0,6004,'intro',0,1380,57,1379,1521629667,1,1353),(198,15,54,0,6005,'intro',0,1352,58,1353,1521629667,1,1380),(198,15,54,0,6006,'intro',0,1381,59,1380,1521629667,1,1352),(198,15,54,0,6007,'intro',0,1365,60,1352,1521629667,1,1381),(198,15,54,0,6008,'intro',0,1382,61,1381,1521629667,1,1365),(198,15,54,0,6009,'intro',0,1376,62,1365,1521629667,1,1382),(198,15,54,0,6010,'intro',0,1375,63,1382,1521629667,1,1376),(198,15,54,0,6011,'intro',0,1363,64,1376,1521629667,1,1375),(198,15,54,0,6012,'intro',0,1383,65,1375,1521629667,1,1363),(198,15,54,0,6013,'intro',0,1366,66,1363,1521629667,1,1383),(198,15,54,0,6014,'intro',0,1384,67,1383,1521629667,1,1366),(198,15,54,0,6015,'intro',0,1385,68,1366,1521629667,1,1384),(198,15,54,0,6016,'intro',0,1367,69,1384,1521629667,1,1385),(198,15,54,0,6017,'intro',0,1348,70,1385,1521629667,1,1367),(198,15,54,0,6018,'intro',0,1337,71,1367,1521629667,1,1348),(198,15,54,0,6019,'intro',0,1386,72,1348,1521629667,1,1337),(198,15,54,0,6020,'intro',0,1387,73,1337,1521629667,1,1386),(198,15,54,0,6021,'intro',0,1388,74,1386,1521629667,1,1387),(198,15,54,0,6022,'intro',0,1389,75,1387,1521629667,1,1388),(198,15,54,0,6023,'intro',0,1390,76,1388,1521629667,1,1389),(198,15,54,0,6024,'intro',0,1348,77,1389,1521629667,1,1390),(198,15,54,0,6025,'intro',0,1391,78,1390,1521629667,1,1348),(198,15,54,0,6026,'intro',0,1392,79,1348,1521629667,1,1391),(198,15,54,0,6027,'intro',0,1366,80,1391,1521629667,1,1392),(198,15,54,0,6028,'intro',0,1393,81,1392,1521629667,1,1366),(198,15,54,0,6029,'intro',0,1394,82,1366,1521629667,1,1393),(198,15,54,0,6030,'intro',0,1473,83,1393,1521629667,1,1394),(200,15,54,0,6031,'meta_title',0,1335,84,1394,1521629667,1,1473),(201,15,54,0,6032,'meta_description',0,1336,85,1473,1521629667,1,1335),(201,15,54,0,6033,'meta_description',0,1337,86,1335,1521629667,1,1336),(201,15,54,0,6034,'meta_description',0,1338,87,1336,1521629667,1,1337),(201,15,54,0,6035,'meta_description',0,1339,88,1337,1521629667,1,1338),(201,15,54,0,6036,'meta_description',0,1340,89,1338,1521629667,1,1339),(201,15,54,0,6037,'meta_description',0,1341,90,1339,1521629667,1,1340),(201,15,54,0,6038,'meta_description',0,1342,91,1340,1521629667,1,1341),(201,15,54,0,6039,'meta_description',0,1343,92,1341,1521629667,1,1342),(201,15,54,0,6040,'meta_description',0,1344,93,1342,1521629667,1,1343),(201,15,54,0,6041,'meta_description',0,1345,94,1343,1521629667,1,1344),(201,15,54,0,6042,'meta_description',0,1346,95,1344,1521629667,1,1345),(201,15,54,0,6043,'meta_description',0,1338,96,1345,1521629667,1,1346),(201,15,54,0,6044,'meta_description',0,1339,97,1346,1521629667,1,1338),(201,15,54,0,6045,'meta_description',0,1347,98,1338,1521629667,1,1339),(201,15,54,0,6046,'meta_description',0,1348,99,1339,1521629667,1,1347),(201,15,54,0,6047,'meta_description',0,1338,100,1347,1521629667,1,1348),(201,15,54,0,6048,'meta_description',0,1339,101,1348,1521629667,1,1338),(201,15,54,0,6049,'meta_description',0,1349,102,1338,1521629667,1,1339),(201,15,54,0,6050,'meta_description',0,1350,103,1339,1521629667,1,1349),(201,15,54,0,6051,'meta_description',0,1351,104,1349,1521629667,1,1350),(201,15,54,0,6052,'meta_description',0,1352,105,1350,1521629667,1,1351),(201,15,54,0,6053,'meta_description',0,1353,106,1351,1521629667,1,1352),(201,15,54,0,6054,'meta_description',0,1354,107,1352,1521629667,1,1353),(201,15,54,0,6055,'meta_description',0,1355,108,1353,1521629667,1,1354),(201,15,54,0,6056,'meta_description',0,1356,109,1354,1521629667,1,1355),(201,15,54,0,6057,'meta_description',0,1357,110,1355,1521629667,1,1356),(201,15,54,0,6058,'meta_description',0,1357,111,1356,1521629667,1,1357),(201,15,54,0,6059,'meta_description',0,1358,112,1357,1521629667,1,1357),(201,15,54,0,6060,'meta_description',0,1348,113,1357,1521629667,1,1358),(201,15,54,0,6061,'meta_description',0,1359,114,1358,1521629667,1,1348),(201,15,54,0,6062,'meta_description',0,1360,115,1348,1521629667,1,1359),(201,15,54,0,6063,'meta_description',0,1361,116,1359,1521629667,1,1360),(201,15,54,0,6064,'meta_description',0,1362,117,1360,1521629667,1,1361),(201,15,54,0,6065,'meta_description',0,1363,118,1361,1521629667,1,1362),(201,15,54,0,6066,'meta_description',0,1364,119,1362,1521629667,1,1363),(201,15,54,0,6067,'meta_description',0,1365,120,1363,1521629667,1,1364),(201,15,54,0,6068,'meta_description',0,1366,121,1364,1521629667,1,1365),(201,15,54,0,6069,'meta_description',0,1367,122,1365,1521629667,1,1366),(201,15,54,0,6070,'meta_description',0,1368,123,1366,1521629667,1,1367),(201,15,54,0,6071,'meta_description',0,1369,124,1367,1521629667,1,1368),(201,15,54,0,6072,'meta_description',0,1370,125,1368,1521629667,1,1369),(201,15,54,0,6073,'meta_description',0,1371,126,1369,1521629667,1,1370),(201,15,54,0,6074,'meta_description',0,1372,127,1370,1521629667,1,1371),(201,15,54,0,6075,'meta_description',0,1370,128,1371,1521629667,1,1372),(201,15,54,0,6076,'meta_description',0,1373,129,1372,1521629667,1,1370),(201,15,54,0,6077,'meta_description',0,1353,130,1370,1521629667,1,1373),(201,15,54,0,6078,'meta_description',0,1374,131,1373,1521629667,1,1353),(201,15,54,0,6079,'meta_description',0,1375,132,1353,1521629667,1,1374),(201,15,54,0,6080,'meta_description',0,1376,133,1374,1521629667,1,1375),(201,15,54,0,6081,'meta_description',0,1377,134,1375,1521629667,1,1376),(201,15,54,0,6082,'meta_description',0,1364,135,1376,1521629667,1,1377),(201,15,54,0,6083,'meta_description',0,1378,136,1377,1521629667,1,1364),(201,15,54,0,6084,'meta_description',0,1379,137,1364,1521629667,1,1378),(201,15,54,0,6085,'meta_description',0,1353,138,1378,1521629667,1,1379),(201,15,54,0,6086,'meta_description',0,1380,139,1379,1521629667,1,1353),(201,15,54,0,6087,'meta_description',0,1352,140,1353,1521629667,1,1380),(201,15,54,0,6088,'meta_description',0,1381,141,1380,1521629667,1,1352),(201,15,54,0,6089,'meta_description',0,1365,142,1352,1521629667,1,1381),(201,15,54,0,6090,'meta_description',0,1382,143,1381,1521629667,1,1365),(201,15,54,0,6091,'meta_description',0,1376,144,1365,1521629667,1,1382),(201,15,54,0,6092,'meta_description',0,1375,145,1382,1521629667,1,1376),(201,15,54,0,6093,'meta_description',0,1363,146,1376,1521629667,1,1375),(201,15,54,0,6094,'meta_description',0,1383,147,1375,1521629667,1,1363),(201,15,54,0,6095,'meta_description',0,1366,148,1363,1521629667,1,1383),(201,15,54,0,6096,'meta_description',0,1384,149,1383,1521629667,1,1366),(201,15,54,0,6097,'meta_description',0,1385,150,1366,1521629667,1,1384),(201,15,54,0,6098,'meta_description',0,1367,151,1384,1521629667,1,1385),(201,15,54,0,6099,'meta_description',0,1348,152,1385,1521629667,1,1367),(201,15,54,0,6100,'meta_description',0,1337,153,1367,1521629667,1,1348),(201,15,54,0,6101,'meta_description',0,1386,154,1348,1521629667,1,1337),(201,15,54,0,6102,'meta_description',0,1387,155,1337,1521629667,1,1386),(201,15,54,0,6103,'meta_description',0,1388,156,1386,1521629667,1,1387),(201,15,54,0,6104,'meta_description',0,1389,157,1387,1521629667,1,1388),(201,15,54,0,6105,'meta_description',0,1390,158,1388,1521629667,1,1389),(201,15,54,0,6106,'meta_description',0,1348,159,1389,1521629667,1,1390),(201,15,54,0,6107,'meta_description',0,1391,160,1390,1521629667,1,1348),(201,15,54,0,6108,'meta_description',0,0,161,1348,1521629667,1,1391),(4,1,57,0,6821,'name',0,1475,0,0,1521632271,3,1476),(4,1,57,0,6822,'name',0,0,1,1476,1521632271,3,1475),(241,21,56,0,6828,'title',0,1050,0,0,1521630465,3,1626),(241,21,56,0,6829,'title',0,970,1,1626,1521630465,3,1050),(241,21,56,0,6830,'title',0,1467,2,1050,1521630465,3,970),(241,21,56,0,6831,'title',0,1627,3,970,1521630465,3,1467),(242,21,56,0,6832,'id_video_youtube',0,0,4,1467,1521630465,3,1627),(241,21,58,0,6838,'title',0,1050,0,0,1521632658,3,1626),(241,21,58,0,6839,'title',0,970,1,1626,1521632658,3,1050),(241,21,58,0,6840,'title',0,1467,2,1050,1521632658,3,970),(241,21,58,0,6841,'title',0,1628,3,970,1521632658,3,1467),(241,21,58,0,6842,'title',0,1629,4,1467,1521632658,3,1628),(241,21,58,0,6843,'title',0,1627,5,1628,1521632658,3,1629),(242,21,58,0,6844,'id_video_youtube',0,0,6,1629,1521632658,3,1627),(4,1,59,0,7198,'name',0,970,0,0,1521632950,3,1702),(4,1,59,0,7199,'name',0,1703,1,1702,1521632950,3,970),(4,1,59,0,7200,'name',0,1704,2,970,1521632950,3,1703),(4,1,59,0,7201,'name',0,0,3,1703,1521632950,3,1704),(244,22,60,0,7202,'title',0,1706,0,0,1521633090,3,1705),(244,22,60,0,7203,'title',0,1707,1,1705,1521633090,3,1706),(244,22,60,0,7204,'title',0,1705,2,1706,1521633090,3,1707),(245,22,60,0,7205,'label',0,1706,3,1707,1521633090,3,1705),(245,22,60,0,7206,'label',0,1707,4,1705,1521633090,3,1706),(245,22,60,0,7207,'label',0,1708,5,1706,1521633090,3,1707),(248,22,60,0,7208,'type_link',0,1705,6,1707,1521633090,3,1708),(248,22,60,0,7209,'type_link',0,0,7,1708,1521633090,3,1705),(244,22,61,0,7215,'title',0,1713,0,0,1521633150,3,1712),(244,22,61,0,7216,'title',0,1714,1,1712,1521633150,3,1713),(244,22,61,0,7217,'title',0,1707,2,1713,1521633150,3,1714),(244,22,61,0,7218,'title',0,1714,3,1714,1521633150,3,1707),(248,22,61,0,7219,'type_link',0,1715,4,1707,1521633150,3,1714),(248,22,61,0,7220,'type_link',0,1712,5,1714,1521633150,3,1715),(248,22,61,0,7221,'type_link',0,1713,6,1715,1521633150,3,1712),(248,22,61,0,7222,'type_link',0,0,7,1712,1521633150,3,1713),(244,22,62,0,7223,'title',0,1713,0,0,1521638152,3,1716),(244,22,62,0,7224,'title',0,1714,1,1716,1521638152,3,1713),(244,22,62,0,7225,'title',0,1707,2,1713,1521638152,3,1714),(244,22,62,0,7226,'title',0,1714,3,1714,1521638152,3,1707),(248,22,62,0,7227,'type_link',0,1715,4,1707,1521638152,3,1714),(248,22,62,0,7228,'type_link',0,1716,5,1714,1521638152,3,1715),(248,22,62,0,7229,'type_link',0,1713,6,1715,1521638152,3,1716),(248,22,62,0,7230,'type_link',0,0,7,1716,1521638152,3,1713),(244,22,63,0,7231,'title',0,1714,0,0,1521638250,3,1717),(244,22,63,0,7232,'title',0,1707,1,1717,1521638250,3,1714),(244,22,63,0,7233,'title',0,1714,2,1714,1521638250,3,1707),(248,22,63,0,7234,'type_link',0,1715,3,1707,1521638250,3,1714),(248,22,63,0,7235,'type_link',0,1717,4,1714,1521638250,3,1715),(248,22,63,0,7236,'type_link',0,1713,5,1715,1521638250,3,1717),(248,22,63,0,7237,'type_link',0,0,6,1717,1521638250,3,1713),(244,22,64,0,7238,'title',0,1714,0,0,1521638293,3,1718),(244,22,64,0,7239,'title',0,1707,1,1718,1521638293,3,1714),(244,22,64,0,7240,'title',0,1714,2,1714,1521638293,3,1707),(248,22,64,0,7241,'type_link',0,1715,3,1707,1521638293,3,1714),(248,22,64,0,7242,'type_link',0,1718,4,1714,1521638293,3,1715),(248,22,64,0,7243,'type_link',0,1713,5,1715,1521638293,3,1718),(248,22,64,0,7244,'type_link',0,0,6,1718,1521638293,3,1713),(202,16,52,0,7245,'title',0,1720,0,0,1521627149,1,1719),(202,16,52,0,7246,'title',0,1474,1,1719,1521627149,1,1720),(203,16,52,0,7247,'subtitle',0,1719,2,1720,1521627149,1,1474),(203,16,52,0,7248,'subtitle',0,1720,3,1474,1521627149,1,1719),(203,16,52,0,7249,'subtitle',0,1335,4,1719,1521627149,1,1720),(205,16,52,0,7250,'intro',0,1336,5,1720,1521627149,1,1335),(205,16,52,0,7251,'intro',0,1337,6,1335,1521627149,1,1336),(205,16,52,0,7252,'intro',0,1338,7,1336,1521627149,1,1337),(205,16,52,0,7253,'intro',0,1339,8,1337,1521627149,1,1338),(205,16,52,0,7254,'intro',0,1340,9,1338,1521627149,1,1339),(205,16,52,0,7255,'intro',0,1341,10,1339,1521627149,1,1340),(205,16,52,0,7256,'intro',0,1342,11,1340,1521627149,1,1341),(205,16,52,0,7257,'intro',0,1343,12,1341,1521627149,1,1342),(205,16,52,0,7258,'intro',0,1344,13,1342,1521627149,1,1343),(205,16,52,0,7259,'intro',0,1345,14,1343,1521627149,1,1344),(205,16,52,0,7260,'intro',0,1346,15,1344,1521627149,1,1345),(205,16,52,0,7261,'intro',0,1338,16,1345,1521627149,1,1346),(205,16,52,0,7262,'intro',0,1339,17,1346,1521627149,1,1338),(205,16,52,0,7263,'intro',0,1347,18,1338,1521627149,1,1339),(205,16,52,0,7264,'intro',0,1348,19,1339,1521627149,1,1347),(205,16,52,0,7265,'intro',0,1338,20,1347,1521627149,1,1348),(205,16,52,0,7266,'intro',0,1339,21,1348,1521627149,1,1338),(205,16,52,0,7267,'intro',0,1349,22,1338,1521627149,1,1339),(205,16,52,0,7268,'intro',0,1350,23,1339,1521627149,1,1349),(205,16,52,0,7269,'intro',0,1351,24,1349,1521627149,1,1350),(205,16,52,0,7270,'intro',0,1352,25,1350,1521627149,1,1351),(205,16,52,0,7271,'intro',0,1353,26,1351,1521627149,1,1352),(205,16,52,0,7272,'intro',0,1354,27,1352,1521627149,1,1353),(205,16,52,0,7273,'intro',0,1355,28,1353,1521627149,1,1354),(205,16,52,0,7274,'intro',0,1356,29,1354,1521627149,1,1355),(205,16,52,0,7275,'intro',0,1357,30,1355,1521627149,1,1356),(205,16,52,0,7276,'intro',0,1357,31,1356,1521627149,1,1357),(205,16,52,0,7277,'intro',0,1358,32,1357,1521627149,1,1357),(205,16,52,0,7278,'intro',0,1348,33,1357,1521627149,1,1358),(205,16,52,0,7279,'intro',0,1359,34,1358,1521627149,1,1348),(205,16,52,0,7280,'intro',0,1360,35,1348,1521627149,1,1359),(205,16,52,0,7281,'intro',0,1361,36,1359,1521627149,1,1360),(205,16,52,0,7282,'intro',0,1362,37,1360,1521627149,1,1361),(205,16,52,0,7283,'intro',0,1363,38,1361,1521627149,1,1362),(205,16,52,0,7284,'intro',0,1364,39,1362,1521627149,1,1363),(205,16,52,0,7285,'intro',0,1365,40,1363,1521627149,1,1364),(205,16,52,0,7286,'intro',0,1366,41,1364,1521627149,1,1365),(205,16,52,0,7287,'intro',0,1367,42,1365,1521627149,1,1366),(205,16,52,0,7288,'intro',0,1368,43,1366,1521627149,1,1367),(205,16,52,0,7289,'intro',0,1369,44,1367,1521627149,1,1368),(205,16,52,0,7290,'intro',0,1370,45,1368,1521627149,1,1369),(205,16,52,0,7291,'intro',0,1371,46,1369,1521627149,1,1370),(205,16,52,0,7292,'intro',0,1372,47,1370,1521627149,1,1371),(205,16,52,0,7293,'intro',0,1370,48,1371,1521627149,1,1372),(205,16,52,0,7294,'intro',0,1373,49,1372,1521627149,1,1370),(205,16,52,0,7295,'intro',0,1353,50,1370,1521627149,1,1373),(205,16,52,0,7296,'intro',0,1374,51,1373,1521627149,1,1353),(205,16,52,0,7297,'intro',0,1375,52,1353,1521627149,1,1374),(205,16,52,0,7298,'intro',0,1376,53,1374,1521627149,1,1375),(205,16,52,0,7299,'intro',0,1377,54,1375,1521627149,1,1376),(205,16,52,0,7300,'intro',0,1364,55,1376,1521627149,1,1377),(205,16,52,0,7301,'intro',0,1378,56,1377,1521627149,1,1364),(205,16,52,0,7302,'intro',0,1379,57,1364,1521627149,1,1378),(205,16,52,0,7303,'intro',0,1353,58,1378,1521627149,1,1379),(205,16,52,0,7304,'intro',0,1380,59,1379,1521627149,1,1353),(205,16,52,0,7305,'intro',0,1352,60,1353,1521627149,1,1380),(205,16,52,0,7306,'intro',0,1381,61,1380,1521627149,1,1352),(205,16,52,0,7307,'intro',0,1365,62,1352,1521627149,1,1381),(205,16,52,0,7308,'intro',0,1382,63,1381,1521627149,1,1365),(205,16,52,0,7309,'intro',0,1376,64,1365,1521627149,1,1382),(205,16,52,0,7310,'intro',0,1375,65,1382,1521627149,1,1376),(205,16,52,0,7311,'intro',0,1363,66,1376,1521627149,1,1375),(205,16,52,0,7312,'intro',0,1383,67,1375,1521627149,1,1363),(205,16,52,0,7313,'intro',0,1366,68,1363,1521627149,1,1383),(205,16,52,0,7314,'intro',0,1384,69,1383,1521627149,1,1366),(205,16,52,0,7315,'intro',0,1385,70,1366,1521627149,1,1384),(205,16,52,0,7316,'intro',0,1367,71,1384,1521627149,1,1385),(205,16,52,0,7317,'intro',0,1348,72,1385,1521627149,1,1367),(205,16,52,0,7318,'intro',0,1337,73,1367,1521627149,1,1348),(205,16,52,0,7319,'intro',0,1386,74,1348,1521627149,1,1337),(205,16,52,0,7320,'intro',0,1387,75,1337,1521627149,1,1386),(205,16,52,0,7321,'intro',0,1388,76,1386,1521627149,1,1387),(205,16,52,0,7322,'intro',0,1389,77,1387,1521627149,1,1388),(205,16,52,0,7323,'intro',0,1390,78,1388,1521627149,1,1389),(205,16,52,0,7324,'intro',0,1348,79,1389,1521627149,1,1390),(205,16,52,0,7325,'intro',0,1391,80,1390,1521627149,1,1348),(205,16,52,0,7326,'intro',0,1392,81,1348,1521627149,1,1391),(205,16,52,0,7327,'intro',0,1366,82,1391,1521627149,1,1392),(205,16,52,0,7328,'intro',0,1393,83,1392,1521627149,1,1366),(205,16,52,0,7329,'intro',0,1394,84,1366,1521627149,1,1393),(205,16,52,0,7330,'intro',0,1367,85,1393,1521627149,1,1394),(206,16,52,0,7331,'content',0,1721,86,1394,1521627149,1,1367),(206,16,52,0,7332,'content',0,1722,87,1367,1521627149,1,1721),(206,16,52,0,7333,'content',0,1723,88,1721,1521627149,1,1722),(206,16,52,0,7334,'content',0,1348,89,1722,1521627149,1,1723),(206,16,52,0,7335,'content',0,1348,90,1723,1521627149,1,1348),(206,16,52,0,7336,'content',0,1724,91,1348,1521627149,1,1348),(206,16,52,0,7337,'content',0,1335,92,1348,1521627149,1,1724),(206,16,52,0,7338,'content',0,1725,93,1724,1521627149,1,1335),(206,16,52,0,7339,'content',0,1726,94,1335,1521627149,1,1725),(206,16,52,0,7340,'content',0,1727,95,1725,1521627149,1,1726),(206,16,52,0,7341,'content',0,1728,96,1726,1521627149,1,1727),(206,16,52,0,7342,'content',0,1387,97,1727,1521627149,1,1728),(206,16,52,0,7343,'content',0,1729,98,1728,1521627149,1,1387),(206,16,52,0,7344,'content',0,1730,99,1387,1521627149,1,1729),(206,16,52,0,7345,'content',0,1338,100,1729,1521627149,1,1730),(206,16,52,0,7346,'content',0,1339,101,1730,1521627149,1,1338),(206,16,52,0,7347,'content',0,1363,102,1338,1521627149,1,1339),(206,16,52,0,7348,'content',0,1731,103,1339,1521627149,1,1363),(206,16,52,0,7349,'content',0,1732,104,1363,1521627149,1,1731),(206,16,52,0,7350,'content',0,1733,105,1731,1521627149,1,1732),(206,16,52,0,7351,'content',0,1734,106,1732,1521627149,1,1733),(206,16,52,0,7352,'content',0,1735,107,1733,1521627149,1,1734),(206,16,52,0,7353,'content',0,1736,108,1734,1521627149,1,1735),(206,16,52,0,7354,'content',0,1394,109,1735,1521627149,1,1736),(206,16,52,0,7355,'content',0,1728,110,1736,1521627149,1,1394),(206,16,52,0,7356,'content',0,1737,111,1394,1521627149,1,1728),(206,16,52,0,7357,'content',0,1738,112,1728,1521627149,1,1737),(206,16,52,0,7358,'content',0,1361,113,1737,1521627149,1,1738),(206,16,52,0,7359,'content',0,1739,114,1738,1521627149,1,1361),(206,16,52,0,7360,'content',0,1740,115,1361,1521627149,1,1739),(206,16,52,0,7361,'content',0,1741,116,1739,1521627149,1,1740),(206,16,52,0,7362,'content',0,1742,117,1740,1521627149,1,1741),(206,16,52,0,7363,'content',0,1338,118,1741,1521627149,1,1742),(206,16,52,0,7364,'content',0,1339,119,1742,1521627149,1,1338),(206,16,52,0,7365,'content',0,1388,120,1338,1521627149,1,1339),(206,16,52,0,7366,'content',0,1743,121,1339,1521627149,1,1388),(206,16,52,0,7367,'content',0,1358,122,1388,1521627149,1,1743),(206,16,52,0,7368,'content',0,1744,123,1743,1521627149,1,1358),(206,16,52,0,7369,'content',0,1745,124,1358,1521627149,1,1744),(206,16,52,0,7370,'content',0,1338,125,1744,1521627149,1,1745),(206,16,52,0,7371,'content',0,1339,126,1745,1521627149,1,1338),(206,16,52,0,7372,'content',0,1746,127,1338,1521627149,1,1339),(206,16,52,0,7373,'content',0,1747,128,1339,1521627149,1,1746),(206,16,52,0,7374,'content',0,1739,129,1746,1521627149,1,1747),(206,16,52,0,7375,'content',0,1350,130,1747,1521627149,1,1739),(206,16,52,0,7376,'content',0,1748,131,1739,1521627149,1,1350),(206,16,52,0,7377,'content',0,1353,132,1350,1521627149,1,1748),(206,16,52,0,7378,'content',0,1723,133,1748,1521627149,1,1353),(206,16,52,0,7379,'content',0,1749,134,1353,1521627149,1,1723),(206,16,52,0,7380,'content',0,1347,135,1723,1521627149,1,1749),(206,16,52,0,7381,'content',0,1367,136,1749,1521627149,1,1347),(206,16,52,0,7382,'content',0,1337,137,1347,1521627149,1,1367),(206,16,52,0,7383,'content',0,1335,138,1367,1521627149,1,1337),(206,16,52,0,7384,'content',0,1350,139,1337,1521627149,1,1335),(206,16,52,0,7385,'content',0,1371,140,1335,1521627149,1,1350),(206,16,52,0,7386,'content',0,1750,141,1350,1521627149,1,1371),(206,16,52,0,7387,'content',0,1380,142,1371,1521627149,1,1750),(206,16,52,0,7388,'content',0,1389,143,1750,1521627149,1,1380),(206,16,52,0,7389,'content',0,1368,144,1380,1521627149,1,1389),(206,16,52,0,7390,'content',0,1745,145,1389,1521627149,1,1368),(206,16,52,0,7391,'content',0,1751,146,1368,1521627149,1,1745),(206,16,52,0,7392,'content',0,1353,147,1745,1521627149,1,1751),(206,16,52,0,7393,'content',0,1361,148,1751,1521627149,1,1353),(206,16,52,0,7394,'content',0,1744,149,1353,1521627149,1,1361),(206,16,52,0,7395,'content',0,1731,150,1361,1521627149,1,1744),(206,16,52,0,7396,'content',0,1752,151,1744,1521627149,1,1731),(206,16,52,0,7397,'content',0,1752,152,1731,1521627149,1,1752),(206,16,52,0,7398,'content',0,1390,153,1752,1521627149,1,1752),(206,16,52,0,7399,'content',0,1726,154,1752,1521627149,1,1390),(206,16,52,0,7400,'content',0,1753,155,1390,1521627149,1,1726),(206,16,52,0,7401,'content',0,1754,156,1726,1521627149,1,1753),(206,16,52,0,7402,'content',0,1386,157,1753,1521627149,1,1754),(206,16,52,0,7403,'content',0,1393,158,1754,1521627149,1,1386),(206,16,52,0,7404,'content',0,1755,159,1386,1521627149,1,1393),(206,16,52,0,7405,'content',0,1756,160,1393,1521627149,1,1755),(206,16,52,0,7406,'content',0,1726,161,1755,1521627149,1,1756),(206,16,52,0,7407,'content',0,1723,162,1756,1521627149,1,1726),(206,16,52,0,7408,'content',0,1386,163,1726,1521627149,1,1723),(206,16,52,0,7409,'content',0,1757,164,1723,1521627149,1,1386),(206,16,52,0,7410,'content',0,1758,165,1386,1521627149,1,1757),(206,16,52,0,7411,'content',0,1393,166,1757,1521627149,1,1758),(206,16,52,0,7412,'content',0,1371,167,1758,1521627149,1,1393),(206,16,52,0,7413,'content',0,1375,168,1393,1521627149,1,1371),(206,16,52,0,7414,'content',0,1335,169,1371,1521627149,1,1375),(206,16,52,0,7415,'content',0,1759,170,1375,1521627149,1,1335),(206,16,52,0,7416,'content',0,1379,171,1335,1521627149,1,1759),(206,16,52,0,7417,'content',0,1760,172,1759,1521627149,1,1379),(206,16,52,0,7418,'content',0,1727,173,1379,1521627149,1,1760),(206,16,52,0,7419,'content',0,1371,174,1760,1521627149,1,1727),(206,16,52,0,7420,'content',0,1746,175,1727,1521627149,1,1371),(206,16,52,0,7421,'content',0,1761,176,1371,1521627149,1,1746),(206,16,52,0,7422,'content',0,1358,177,1746,1521627149,1,1761),(206,16,52,0,7423,'content',0,1370,178,1761,1521627149,1,1358),(206,16,52,0,7424,'content',0,1734,179,1358,1521627149,1,1370),(206,16,52,0,7425,'content',0,1375,180,1370,1521627149,1,1734),(206,16,52,0,7426,'content',0,1351,181,1734,1521627149,1,1375),(206,16,52,0,7427,'content',0,1742,182,1375,1521627149,1,1351),(206,16,52,0,7428,'content',0,1762,183,1351,1521627149,1,1742),(206,16,52,0,7429,'content',0,1357,184,1742,1521627149,1,1762),(206,16,52,0,7430,'content',0,1763,185,1762,1521627149,1,1357),(206,16,52,0,7431,'content',0,1744,186,1357,1521627149,1,1763),(206,16,52,0,7432,'content',0,1370,187,1763,1521627149,1,1744),(206,16,52,0,7433,'content',0,1347,188,1744,1521627149,1,1370),(206,16,52,0,7434,'content',0,1760,189,1370,1521627149,1,1347),(206,16,52,0,7435,'content',0,1757,190,1347,1521627149,1,1760),(206,16,52,0,7436,'content',0,1726,191,1760,1521627149,1,1757),(206,16,52,0,7437,'content',0,1764,192,1757,1521627149,1,1726),(206,16,52,0,7438,'content',0,1747,193,1726,1521627149,1,1764),(206,16,52,0,7439,'content',0,1374,194,1764,1521627149,1,1747),(206,16,52,0,7440,'content',0,1389,195,1747,1521627149,1,1374),(206,16,52,0,7441,'content',0,1723,196,1374,1521627149,1,1389),(206,16,52,0,7442,'content',0,1735,197,1389,1521627149,1,1723),(206,16,52,0,7443,'content',0,1724,198,1723,1521627149,1,1735),(206,16,52,0,7444,'content',0,1765,199,1735,1521627149,1,1724),(206,16,52,0,7445,'content',0,1338,200,1724,1521627149,1,1765),(206,16,52,0,7446,'content',0,1339,201,1765,1521627149,1,1338),(206,16,52,0,7447,'content',0,1393,202,1338,1521627149,1,1339),(206,16,52,0,7448,'content',0,1766,203,1339,1521627149,1,1393),(206,16,52,0,7449,'content',0,1392,204,1393,1521627149,1,1766),(206,16,52,0,7450,'content',0,1726,205,1766,1521627149,1,1392),(206,16,52,0,7451,'content',0,1741,206,1392,1521627149,1,1726),(206,16,52,0,7452,'content',0,1731,207,1726,1521627149,1,1741),(206,16,52,0,7453,'content',0,1767,208,1741,1521627149,1,1731),(206,16,52,0,7454,'content',0,1742,209,1731,1521627149,1,1767),(206,16,52,0,7455,'content',0,1352,210,1767,1521627149,1,1742),(206,16,52,0,7456,'content',0,1738,211,1742,1521627149,1,1352),(206,16,52,0,7457,'content',0,1761,212,1352,1521627149,1,1738),(206,16,52,0,7458,'content',0,1727,213,1738,1521627149,1,1761),(206,16,52,0,7459,'content',0,1348,214,1761,1521627149,1,1727),(206,16,52,0,7460,'content',0,1738,215,1727,1521627149,1,1348),(206,16,52,0,7461,'content',0,1768,216,1348,1521627149,1,1738),(206,16,52,0,7462,'content',0,1387,217,1738,1521627149,1,1768),(206,16,52,0,7463,'content',0,1769,218,1768,1521627149,1,1387),(206,16,52,0,7464,'content',0,1386,219,1387,1521627149,1,1769),(206,16,52,0,7465,'content',0,1378,220,1769,1521627149,1,1386),(206,16,52,0,7466,'content',0,1361,221,1386,1521627149,1,1378),(206,16,52,0,7467,'content',0,1770,222,1378,1521627149,1,1361),(206,16,52,0,7468,'content',0,1771,223,1361,1521627149,1,1770),(206,16,52,0,7469,'content',0,1372,224,1770,1521627149,1,1771),(206,16,52,0,7470,'content',0,1738,225,1771,1521627149,1,1372),(206,16,52,0,7471,'content',0,1725,226,1372,1521627149,1,1738),(206,16,52,0,7472,'content',0,1736,227,1738,1521627149,1,1725),(206,16,52,0,7473,'content',0,1772,228,1725,1521627149,1,1736),(206,16,52,0,7474,'content',0,1348,229,1736,1521627149,1,1772),(206,16,52,0,7475,'content',0,1773,230,1772,1521627149,1,1348),(206,16,52,0,7476,'content',0,1774,231,1348,1521627149,1,1773),(206,16,52,0,7477,'content',0,1724,232,1773,1521627149,1,1774),(206,16,52,0,7478,'content',0,1353,233,1774,1521627149,1,1724),(206,16,52,0,7479,'content',0,1751,234,1724,1521627149,1,1353),(206,16,52,0,7480,'content',0,1753,235,1353,1521627149,1,1751),(206,16,52,0,7481,'content',0,1348,236,1751,1521627149,1,1753),(206,16,52,0,7482,'content',0,1736,237,1753,1521627149,1,1348),(206,16,52,0,7483,'content',0,1741,238,1348,1521627149,1,1736),(206,16,52,0,7484,'content',0,1385,239,1736,1521627149,1,1741),(206,16,52,0,7485,'content',0,1745,240,1741,1521627149,1,1385),(206,16,52,0,7486,'content',0,1775,241,1385,1521627149,1,1745),(206,16,52,0,7487,'content',0,1776,242,1745,1521627149,1,1775),(206,16,52,0,7488,'content',0,1738,243,1775,1521627149,1,1776),(206,16,52,0,7489,'content',0,1747,244,1776,1521627149,1,1738),(206,16,52,0,7490,'content',0,1728,245,1738,1521627149,1,1747),(206,16,52,0,7491,'content',0,1757,246,1747,1521627149,1,1728),(206,16,52,0,7492,'content',0,1344,247,1728,1521627149,1,1757),(206,16,52,0,7493,'content',0,1338,248,1757,1521627149,1,1344),(206,16,52,0,7494,'content',0,1339,249,1344,1521627149,1,1338),(206,16,52,0,7495,'content',0,1777,250,1338,1521627149,1,1339),(206,16,52,0,7496,'content',0,1748,251,1339,1521627149,1,1777),(206,16,52,0,7497,'content',0,1368,252,1777,1521627149,1,1748),(206,16,52,0,7498,'content',0,1778,253,1748,1521627149,1,1368),(206,16,52,0,7499,'content',0,1729,254,1368,1521627149,1,1778),(206,16,52,0,7500,'content',0,1349,255,1778,1521627149,1,1729),(206,16,52,0,7501,'content',0,1338,256,1729,1521627149,1,1349),(206,16,52,0,7502,'content',0,1339,257,1349,1521627149,1,1338),(206,16,52,0,7503,'content',0,1385,258,1338,1521627149,1,1339),(206,16,52,0,7504,'content',0,1779,259,1339,1521627149,1,1385),(206,16,52,0,7505,'content',0,1375,260,1385,1521627149,1,1779),(206,16,52,0,7506,'content',0,1774,261,1779,1521627149,1,1375),(206,16,52,0,7507,'content',0,1755,262,1375,1521627149,1,1774),(206,16,52,0,7508,'content',0,1369,263,1774,1521627149,1,1755),(206,16,52,0,7509,'content',0,1745,264,1755,1521627149,1,1369),(206,16,52,0,7510,'content',0,1775,265,1369,1521627149,1,1745),(206,16,52,0,7511,'content',0,1383,266,1745,1521627149,1,1775),(206,16,52,0,7512,'content',0,1377,267,1775,1521627149,1,1383),(206,16,52,0,7513,'content',0,1336,268,1383,1521627149,1,1377),(206,16,52,0,7514,'content',0,1721,269,1377,1521627149,1,1336),(206,16,52,0,7515,'content',0,1387,270,1336,1521627149,1,1721),(206,16,52,0,7516,'content',0,1354,271,1721,1521627149,1,1387),(206,16,52,0,7517,'content',0,1729,272,1387,1521627149,1,1354),(206,16,52,0,7518,'content',0,1732,273,1354,1521627149,1,1729),(206,16,52,0,7519,'content',0,1758,274,1729,1521627149,1,1732),(206,16,52,0,7520,'content',0,1387,275,1732,1521627149,1,1758),(206,16,52,0,7521,'content',0,1777,276,1758,1521627149,1,1387),(206,16,52,0,7522,'content',0,1743,277,1387,1521627149,1,1777),(206,16,52,0,7523,'content',0,1780,278,1777,1521627149,1,1743),(206,16,52,0,7524,'content',0,1362,279,1743,1521627149,1,1780),(206,16,52,0,7525,'content',0,1781,280,1780,1521627149,1,1362),(206,16,52,0,7526,'content',0,1360,281,1362,1521627149,1,1781),(206,16,52,0,7527,'content',0,1782,282,1781,1521627149,1,1360),(206,16,52,0,7528,'content',0,1756,283,1360,1521627149,1,1782),(206,16,52,0,7529,'content',0,1728,284,1782,1521627149,1,1756),(206,16,52,0,7530,'content',0,1742,285,1756,1521627149,1,1728),(206,16,52,0,7531,'content',0,1749,286,1728,1521627149,1,1742),(206,16,52,0,7532,'content',0,1724,287,1742,1521627149,1,1749),(206,16,52,0,7533,'content',0,1743,288,1749,1521627149,1,1724),(206,16,52,0,7534,'content',0,1371,289,1724,1521627149,1,1743),(206,16,52,0,7535,'content',0,1738,290,1743,1521627149,1,1371),(206,16,52,0,7536,'content',0,1783,291,1371,1521627149,1,1738),(206,16,52,0,7537,'content',0,1761,292,1738,1521627149,1,1783),(206,16,52,0,7538,'content',0,1731,293,1783,1521627149,1,1761),(206,16,52,0,7539,'content',0,1337,294,1761,1521627149,1,1731),(206,16,52,0,7540,'content',0,1784,295,1731,1521627149,1,1337),(206,16,52,0,7541,'content',0,1371,296,1337,1521627149,1,1784),(206,16,52,0,7542,'content',0,1730,297,1784,1521627149,1,1371),(206,16,52,0,7543,'content',0,1371,298,1371,1521627149,1,1730),(206,16,52,0,7544,'content',0,1780,299,1730,1521627149,1,1371),(206,16,52,0,7545,'content',0,1726,300,1371,1521627149,1,1780),(206,16,52,0,7546,'content',0,1742,301,1780,1521627149,1,1726),(206,16,52,0,7547,'content',0,1763,302,1726,1521627149,1,1742),(206,16,52,0,7548,'content',0,1784,303,1742,1521627149,1,1763),(206,16,52,0,7549,'content',0,1336,304,1763,1521627149,1,1784),(206,16,52,0,7550,'content',0,1386,305,1784,1521627149,1,1336),(206,16,52,0,7551,'content',0,1352,306,1336,1521627149,1,1386),(206,16,52,0,7552,'content',0,1740,307,1386,1521627149,1,1352),(206,16,52,0,7553,'content',0,1741,308,1352,1521627149,1,1740),(206,16,52,0,7554,'content',0,1367,309,1740,1521627149,1,1741),(206,16,52,0,7555,'content',0,1394,310,1741,1521627149,1,1367),(206,16,52,0,7556,'content',0,1731,311,1367,1521627149,1,1394),(206,16,52,0,7557,'content',0,1785,312,1394,1521627149,1,1731),(206,16,52,0,7558,'content',0,1353,313,1731,1521627149,1,1785),(206,16,52,0,7559,'content',0,1393,314,1785,1521627149,1,1353),(206,16,52,0,7560,'content',0,1728,315,1353,1521627149,1,1393),(206,16,52,0,7561,'content',0,1357,316,1393,1521627149,1,1728),(206,16,52,0,7562,'content',0,1721,317,1728,1521627149,1,1357),(206,16,52,0,7563,'content',0,1337,318,1357,1521627149,1,1721),(206,16,52,0,7564,'content',0,1348,319,1721,1521627149,1,1337),(206,16,52,0,7565,'content',0,1785,320,1337,1521627149,1,1348),(206,16,52,0,7566,'content',0,1364,321,1348,1521627149,1,1785),(206,16,52,0,7567,'content',0,1348,322,1785,1521627149,1,1364),(206,16,52,0,7568,'content',0,1747,323,1364,1521627149,1,1348),(206,16,52,0,7569,'content',0,1738,324,1348,1521627149,1,1747),(206,16,52,0,7570,'content',0,1786,325,1747,1521627149,1,1738),(206,16,52,0,7571,'content',0,1732,326,1738,1521627149,1,1786),(206,16,52,0,7572,'content',0,1764,327,1786,1521627149,1,1732),(206,16,52,0,7573,'content',0,1743,328,1732,1521627149,1,1764),(206,16,52,0,7574,'content',0,1723,329,1764,1521627149,1,1743),(206,16,52,0,7575,'content',0,1728,330,1743,1521627149,1,1723),(206,16,52,0,7576,'content',0,1378,331,1723,1521627149,1,1728),(206,16,52,0,7577,'content',0,1753,332,1728,1521627149,1,1378),(206,16,52,0,7578,'content',0,1354,333,1378,1521627149,1,1753),(206,16,52,0,7579,'content',0,1362,334,1753,1521627149,1,1354),(206,16,52,0,7580,'content',0,1374,335,1354,1521627149,1,1362),(206,16,52,0,7581,'content',0,1728,336,1362,1521627149,1,1374),(206,16,52,0,7582,'content',0,1351,337,1374,1521627149,1,1728),(206,16,52,0,7583,'content',0,1352,338,1728,1521627149,1,1351),(206,16,52,0,7584,'content',0,1769,339,1351,1521627149,1,1352),(206,16,52,0,7585,'content',0,1747,340,1352,1521627149,1,1769),(206,16,52,0,7586,'content',0,1727,341,1769,1521627149,1,1747),(206,16,52,0,7587,'content',0,1787,342,1747,1521627149,1,1727),(206,16,52,0,7588,'content',0,1366,343,1727,1521627149,1,1787),(206,16,52,0,7589,'content',0,1727,344,1787,1521627149,1,1366),(206,16,52,0,7590,'content',0,1366,345,1366,1521627149,1,1727),(206,16,52,0,7591,'content',0,1779,346,1727,1521627149,1,1366),(206,16,52,0,7592,'content',0,1788,347,1366,1521627149,1,1779),(206,16,52,0,7593,'content',0,1370,348,1779,1521627149,1,1788),(206,16,52,0,7594,'content',0,1476,349,1788,1521627149,1,1370),(206,16,52,0,7595,'content',0,1475,350,1370,1521627149,1,1476),(206,16,52,0,7596,'content',0,1702,351,1476,1521627149,1,1475),(206,16,52,0,7597,'content',0,970,352,1475,1521627149,1,1702),(206,16,52,0,7598,'content',0,1703,353,1702,1521627149,1,970),(206,16,52,0,7599,'content',0,1789,354,970,1521627149,1,1703),(206,16,52,0,7600,'content',0,0,355,1703,1521627149,1,1789);
/*!40000 ALTER TABLE `ezsearch_object_word_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsearch_search_phrase`
--

DROP TABLE IF EXISTS `ezsearch_search_phrase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsearch_search_phrase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phrase` varchar(250) DEFAULT NULL,
  `phrase_count` int(11) DEFAULT '0',
  `result_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ezsearch_search_phrase_phrase` (`phrase`),
  KEY `ezsearch_search_phrase_count` (`phrase_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsearch_search_phrase`
--

LOCK TABLES `ezsearch_search_phrase` WRITE;
/*!40000 ALTER TABLE `ezsearch_search_phrase` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezsearch_search_phrase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsearch_word`
--

DROP TABLE IF EXISTS `ezsearch_word`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsearch_word` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_count` int(11) NOT NULL DEFAULT '0',
  `word` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezsearch_word_obj_count` (`object_count`),
  KEY `ezsearch_word_word_i` (`word`)
) ENGINE=InnoDB AUTO_INCREMENT=1790 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsearch_word`
--

LOCK TABLES `ezsearch_word` WRITE;
/*!40000 ALTER TABLE `ezsearch_word` DISABLE KEYS */;
INSERT INTO `ezsearch_word` VALUES (970,5,'to'),(975,1,'the'),(984,1,'setup'),(996,1,'for'),(1029,3,'user'),(1050,3,'com'),(1060,1,'main'),(1061,2,'group'),(1062,3,'users'),(1063,2,'anonymous'),(1064,1,'guest'),(1065,1,'accounts'),(1066,2,'administrator'),(1067,1,'editors'),(1068,1,'media'),(1069,1,'images'),(1070,1,'files'),(1071,1,'multimedia'),(1335,2,'lorem'),(1336,2,'ipsum'),(1337,2,'dolor'),(1338,2,'sit'),(1339,2,'amet'),(1340,2,'consectetur'),(1341,2,'adipiscing'),(1342,2,'elit'),(1343,2,'integer'),(1344,2,'bibendum'),(1345,2,'ornare'),(1346,2,'enim'),(1347,2,'fringilla'),(1348,2,'sed'),(1349,2,'ante'),(1350,2,'hendrerit'),(1351,2,'tempor'),(1352,2,'odio'),(1353,2,'ut'),(1354,2,'lacinia'),(1355,2,'dui'),(1356,2,'nunc'),(1357,2,'elementum'),(1358,2,'vehicula'),(1359,2,'viverra'),(1360,2,'libero'),(1361,2,'ligula'),(1362,2,'ac'),(1363,2,'ullamcorper'),(1364,2,'massa'),(1365,2,'ultricies'),(1366,2,'vitae'),(1367,2,'pellentesque'),(1368,2,'sollicitudin'),(1369,2,'mattis'),(1370,2,'erat'),(1371,2,'nec'),(1372,2,'porttitor'),(1373,2,'sagittis'),(1374,2,'phasellus'),(1375,2,'quis'),(1376,2,'risus'),(1377,2,'scelerisque'),(1378,2,'lobortis'),(1379,2,'convallis'),(1380,2,'et'),(1381,2,'maecenas'),(1382,2,'semper'),(1383,2,'aliquam'),(1384,2,'porta'),(1385,2,'quam'),(1386,2,'non'),(1387,2,'nibh'),(1388,2,'commodo'),(1389,2,'pharetra'),(1390,2,'eget'),(1391,2,'felis'),(1392,2,'suspendisse'),(1393,2,'consequat'),(1394,2,'tellus'),(1462,1,'global'),(1463,1,'configuration'),(1464,1,'0c687e'),(1465,1,'1692b0'),(1466,1,'hello'),(1467,3,'code'),(1468,1,'9'),(1469,1,'rue'),(1470,1,'tronchet'),(1471,1,'69006'),(1472,1,'lyon'),(1473,1,'homepage'),(1474,2,'subtitle'),(1475,2,'videos'),(1476,2,'youtube'),(1626,2,'inside'),(1627,2,'gtkpt8auqhw'),(1628,1,'normal'),(1629,1,'width'),(1702,2,'call'),(1703,2,'action'),(1704,1,'links'),(1705,1,'simple'),(1706,1,'link'),(1707,5,'example'),(1708,1,'lien'),(1712,1,'primary'),(1713,4,'color'),(1714,4,'button'),(1715,4,'with'),(1716,1,'secondary'),(1717,1,'black'),(1718,1,'white'),(1719,1,'demo'),(1720,1,'page'),(1721,1,'id'),(1722,1,'nisi'),(1723,1,'tortor'),(1724,1,'posuere'),(1725,1,'mauris'),(1726,1,'vel'),(1727,1,'sem'),(1728,1,'eu'),(1729,1,'tristique'),(1730,1,'rhoncus'),(1731,1,'velit'),(1732,1,'cras'),(1733,1,'leo'),(1734,1,'nisl'),(1735,1,'tincidunt'),(1736,1,'a'),(1737,1,'efficitur'),(1738,1,'suscipit'),(1739,1,'fusce'),(1740,1,'gravida'),(1741,1,'dignissim'),(1742,1,'est'),(1743,1,'metus'),(1744,1,'at'),(1745,1,'nulla'),(1746,1,'cursus'),(1747,1,'justo'),(1748,1,'magna'),(1749,1,'maximus'),(1750,1,'diam'),(1751,1,'vestibulum'),(1752,1,'venenatis'),(1753,1,'ex'),(1754,1,'nam'),(1755,1,'lectus'),(1756,1,'vivamus'),(1757,1,'augue'),(1758,1,'fermentum'),(1759,1,'nullam'),(1760,1,'auctor'),(1761,1,'in'),(1762,1,'placerat'),(1763,1,'praesent'),(1764,1,'mollis'),(1765,1,'orci'),(1766,1,'lacus'),(1767,1,'aenean'),(1768,1,'egestas'),(1769,1,'donec'),(1770,1,'curabitur'),(1771,1,'faucibus'),(1772,1,'urna'),(1773,1,'turpis'),(1774,1,'volutpat'),(1775,1,'facilisi'),(1776,1,'morbi'),(1777,1,'pulvinar'),(1778,1,'quisque'),(1779,1,'pretium'),(1780,1,'blandit'),(1781,1,'accumsan'),(1782,1,'varius'),(1783,1,'arcu'),(1784,1,'eleifend'),(1785,1,'luctus'),(1786,1,'finibus'),(1787,1,'dictum'),(1788,1,'aliquet'),(1789,1,'buttons');
/*!40000 ALTER TABLE `ezsearch_word` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsection`
--

DROP TABLE IF EXISTS `ezsection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) DEFAULT NULL,
  `locale` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `navigation_part_identifier` varchar(100) DEFAULT 'ezcontentnavigationpart',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsection`
--

LOCK TABLES `ezsection` WRITE;
/*!40000 ALTER TABLE `ezsection` DISABLE KEYS */;
INSERT INTO `ezsection` VALUES (1,'standard','','Standard','ezcontentnavigationpart'),(2,'users','','Users','ezusernavigationpart'),(3,'media','','Media','ezmedianavigationpart'),(4,'setup','','Setup','ezsetupnavigationpart'),(5,'design','','Design','ezvisualnavigationpart');
/*!40000 ALTER TABLE `ezsection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsession`
--

DROP TABLE IF EXISTS `ezsession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsession` (
  `data` longtext NOT NULL,
  `expiration_time` int(11) NOT NULL DEFAULT '0',
  `session_key` varchar(32) NOT NULL DEFAULT '',
  `user_hash` varchar(32) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_key`),
  KEY `expiration_time` (`expiration_time`),
  KEY `ezsession_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsession`
--

LOCK TABLES `ezsession` WRITE;
/*!40000 ALTER TABLE `ezsession` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezsession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsite_data`
--

DROP TABLE IF EXISTS `ezsite_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsite_data` (
  `name` varchar(60) NOT NULL DEFAULT '',
  `value` longtext NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsite_data`
--

LOCK TABLES `ezsite_data` WRITE;
/*!40000 ALTER TABLE `ezsite_data` DISABLE KEYS */;
INSERT INTO `ezsite_data` VALUES ('ezpublish-release','1'),('ezpublish-version','6.4.0');
/*!40000 ALTER TABLE `ezsite_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezstarrating`
--

DROP TABLE IF EXISTS `ezstarrating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezstarrating` (
  `contentobject_id` int(11) NOT NULL,
  `contentobject_attribute_id` int(11) NOT NULL,
  `rating_average` float NOT NULL,
  `rating_count` int(11) NOT NULL,
  PRIMARY KEY (`contentobject_id`,`contentobject_attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezstarrating`
--

LOCK TABLES `ezstarrating` WRITE;
/*!40000 ALTER TABLE `ezstarrating` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezstarrating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezstarrating_data`
--

DROP TABLE IF EXISTS `ezstarrating_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezstarrating_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `session_key` varchar(32) NOT NULL,
  `rating` float NOT NULL,
  `contentobject_id` int(11) NOT NULL,
  `contentobject_attribute_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_session_key` (`user_id`,`session_key`),
  KEY `contentobject_id_contentobject_attribute_id` (`contentobject_id`,`contentobject_attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezstarrating_data`
--

LOCK TABLES `ezstarrating_data` WRITE;
/*!40000 ALTER TABLE `ezstarrating_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezstarrating_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsubtree_notification_rule`
--

DROP TABLE IF EXISTS `ezsubtree_notification_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsubtree_notification_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_id` int(11) NOT NULL DEFAULT '0',
  `use_digest` int(11) DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezsubtree_notification_rule_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsubtree_notification_rule`
--

LOCK TABLES `ezsubtree_notification_rule` WRITE;
/*!40000 ALTER TABLE `ezsubtree_notification_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezsubtree_notification_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eztipafriend_counter`
--

DROP TABLE IF EXISTS `eztipafriend_counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eztipafriend_counter` (
  `count` int(11) NOT NULL DEFAULT '0',
  `node_id` int(11) NOT NULL DEFAULT '0',
  `requested` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`node_id`,`requested`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eztipafriend_counter`
--

LOCK TABLES `eztipafriend_counter` WRITE;
/*!40000 ALTER TABLE `eztipafriend_counter` DISABLE KEYS */;
/*!40000 ALTER TABLE `eztipafriend_counter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eztipafriend_request`
--

DROP TABLE IF EXISTS `eztipafriend_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eztipafriend_request` (
  `created` int(11) NOT NULL DEFAULT '0',
  `email_receiver` varchar(100) NOT NULL DEFAULT '',
  KEY `eztipafriend_request_created` (`created`),
  KEY `eztipafriend_request_email_rec` (`email_receiver`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eztipafriend_request`
--

LOCK TABLES `eztipafriend_request` WRITE;
/*!40000 ALTER TABLE `eztipafriend_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `eztipafriend_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eztrigger`
--

DROP TABLE IF EXISTS `eztrigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eztrigger` (
  `connect_type` char(1) NOT NULL DEFAULT '',
  `function_name` varchar(200) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(200) NOT NULL DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `workflow_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `eztrigger_def_id` (`module_name`(50),`function_name`(50),`connect_type`),
  KEY `eztrigger_fetch` (`name`(25),`module_name`(50),`function_name`(50))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eztrigger`
--

LOCK TABLES `eztrigger` WRITE;
/*!40000 ALTER TABLE `eztrigger` DISABLE KEYS */;
/*!40000 ALTER TABLE `eztrigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezurl`
--

DROP TABLE IF EXISTS `ezurl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezurl` (
  `created` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_valid` int(11) NOT NULL DEFAULT '1',
  `last_checked` int(11) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  `original_url_md5` varchar(32) NOT NULL DEFAULT '',
  `url` longtext,
  PRIMARY KEY (`id`),
  KEY `ezurl_url` (`url`(255))
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezurl`
--

LOCK TABLES `ezurl` WRITE;
/*!40000 ALTER TABLE `ezurl` DISABLE KEYS */;
INSERT INTO `ezurl` VALUES (1448832197,23,1,0,1448832197,'f76e41d421b2a72232264943026a6ee5','https://doc.ez.no/display/USER/'),(1448832277,24,1,0,1505717756,'a00ab36edb35bb641cc027eb27410934','https://doc.ezplatform.com/en/latest/'),(1448832412,25,1,0,1505717756,'03c4188f5fdcb679192e25a7dad09c2d','https://doc.ezplatform.com/en/latest/tutorials/platform_beginner/building_a_bicycle_route_tracker_in_ez_platform/'),(1448832436,26,1,0,1505717756,'5af7e9599b5583ccd0103a400ddacfdd','https://doc.ez.no/display/MAIN/Transitioning+from+eZ+Publish+to+eZ+Platform%3A+Feature+Comparison'),(1448832661,27,1,0,1448832661,'f9bf96304c434139d0ff5773b6eee157','https://github.com/ezsystems'),(1448832661,28,1,0,1510739262,'265d537bfba0e5ed4e85fbcd7f30ea84','https://discuss.ezplatform.com'),(1448832661,29,1,0,1448832661,'7441963094866aa9d1cbb8a59cca541c','http://ez-community-on-slack.herokuapp.com/'),(1521628627,30,1,0,1521628627,'f3cf28b913e9400df892a56e9862701f','https://www.facebook.com/comtocode/'),(1521628627,31,1,0,1521628627,'25dd904fae8d9c5d3ce67e7a8a1b438b','https://www.twitter.com/comtocode/'),(1521628627,32,1,0,1521628627,'38115160c70cf3fc46f2ceb49f90de96','https://www.instagram.com/comtocode/'),(1521633090,33,1,0,1521633090,'dcdbea3f97a3797ab853047f2eb538ae','https://www.com-to-code.com/');
/*!40000 ALTER TABLE `ezurl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezurl_object_link`
--

DROP TABLE IF EXISTS `ezurl_object_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezurl_object_link` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT '0',
  `contentobject_attribute_version` int(11) NOT NULL DEFAULT '0',
  `url_id` int(11) NOT NULL DEFAULT '0',
  KEY `ezurl_ol_coa_id` (`contentobject_attribute_id`),
  KEY `ezurl_ol_coa_version` (`contentobject_attribute_version`),
  KEY `ezurl_ol_url_id` (`url_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezurl_object_link`
--

LOCK TABLES `ezurl_object_link` WRITE;
/*!40000 ALTER TABLE `ezurl_object_link` DISABLE KEYS */;
INSERT INTO `ezurl_object_link` VALUES (104,9,23),(104,9,24),(104,9,25),(104,9,26),(104,9,27),(104,9,28),(104,9,29),(104,9,23),(104,9,24),(104,9,25),(104,9,26),(104,9,27),(104,9,28),(104,9,29),(201,1,30),(202,1,31),(203,1,32),(201,1,30),(202,1,31),(203,1,32),(239,1,33),(244,1,33),(244,2,33),(244,3,33),(244,3,33),(249,1,33),(254,1,33),(259,1,33);
/*!40000 ALTER TABLE `ezurl_object_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezurlalias`
--

DROP TABLE IF EXISTS `ezurlalias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezurlalias` (
  `destination_url` longtext NOT NULL,
  `forward_to_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_imported` int(11) NOT NULL DEFAULT '0',
  `is_internal` int(11) NOT NULL DEFAULT '1',
  `is_wildcard` int(11) NOT NULL DEFAULT '0',
  `source_md5` varchar(32) DEFAULT NULL,
  `source_url` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ezurlalias_desturl` (`destination_url`(200)),
  KEY `ezurlalias_forward_to_id` (`forward_to_id`),
  KEY `ezurlalias_imp_wcard_fwd` (`is_imported`,`is_wildcard`,`forward_to_id`),
  KEY `ezurlalias_source_md5` (`source_md5`),
  KEY `ezurlalias_source_url` (`source_url`(255)),
  KEY `ezurlalias_wcard_fwd` (`is_wildcard`,`forward_to_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezurlalias`
--

LOCK TABLES `ezurlalias` WRITE;
/*!40000 ALTER TABLE `ezurlalias` DISABLE KEYS */;
INSERT INTO `ezurlalias` VALUES ('content/view/full/2',0,12,1,1,0,'d41d8cd98f00b204e9800998ecf8427e',''),('content/view/full/5',0,13,1,1,0,'9bc65c2abec141778ffaa729489f3e87','users'),('content/view/full/12',0,15,1,1,0,'02d4e844e3a660857a3f81585995ffe1','users/guest_accounts'),('content/view/full/13',0,16,1,1,0,'1b1d79c16700fd6003ea7be233e754ba','users/administrator_users'),('content/view/full/14',0,17,1,1,0,'0bb9dd665c96bbc1cf36b79180786dea','users/editors'),('content/view/full/15',0,18,1,1,0,'f1305ac5f327a19b451d82719e0c3f5d','users/administrator_users/administrator_user'),('content/view/full/43',0,20,1,1,0,'62933a2951ef01f4eafd9bdf4d3cd2f0','media'),('content/view/full/44',0,21,1,1,0,'3ae1aac958e1c82013689d917d34967a','users/anonymous_users'),('content/view/full/45',0,22,1,1,0,'aad93975f09371695ba08292fd9698db','users/anonymous_users/anonymous_user'),('content/view/full/48',0,25,1,1,0,'a0f848942ce863cf53c0fa6cc684007d','setup'),('content/view/full/51',0,28,1,1,0,'38985339d4a5aadfc41ab292b4527046','media/images'),('content/view/full/52',0,29,1,1,0,'ad5a8c6f6aac3b1b9df267fe22e7aef6','media/files'),('content/view/full/53',0,30,1,1,0,'562a0ac498571c6c3529173184a2657c','media/multimedia');
/*!40000 ALTER TABLE `ezurlalias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezurlalias_ml`
--

DROP TABLE IF EXISTS `ezurlalias_ml`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezurlalias_ml` (
  `action` longtext NOT NULL,
  `action_type` varchar(32) NOT NULL DEFAULT '',
  `alias_redirects` int(11) NOT NULL DEFAULT '1',
  `id` int(11) NOT NULL DEFAULT '0',
  `is_alias` int(11) NOT NULL DEFAULT '0',
  `is_original` int(11) NOT NULL DEFAULT '0',
  `lang_mask` bigint(20) NOT NULL DEFAULT '0',
  `link` int(11) NOT NULL DEFAULT '0',
  `parent` int(11) NOT NULL DEFAULT '0',
  `text` longtext NOT NULL,
  `text_md5` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`parent`,`text_md5`),
  KEY `ezurlalias_ml_act_org` (`action`(32),`is_original`),
  KEY `ezurlalias_ml_actt_org_al` (`action_type`,`is_original`,`is_alias`),
  KEY `ezurlalias_ml_id` (`id`),
  KEY `ezurlalias_ml_par_act_id_lnk` (`action`(32),`id`,`link`,`parent`),
  KEY `ezurlalias_ml_par_lnk_txt` (`parent`,`text`(32),`link`),
  KEY `ezurlalias_ml_text` (`text`(32),`id`,`link`),
  KEY `ezurlalias_ml_text_lang` (`text`(32),`lang_mask`,`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezurlalias_ml`
--

LOCK TABLES `ezurlalias_ml` WRITE;
/*!40000 ALTER TABLE `ezurlalias_ml` DISABLE KEYS */;
INSERT INTO `ezurlalias_ml` VALUES ('eznode:55','eznode',0,39,0,1,2,39,0,'Global-configuration','1bf71427cd8b09bb36c44c78a4887e03'),('eznode:48','eznode',1,13,0,1,3,13,0,'Setup2','475e97c0146bfb1c490339546d9e72ee'),('nop:','nop',1,17,0,0,1,17,0,'media2','50e2736330de124f6edea9b008556fe6'),('eznode:54','eznode',1,38,0,1,2,38,0,'Demo-page','5e3a4fa77ef36101f3b38cb9be619c6e'),('eznode:43','eznode',1,9,0,1,3,9,0,'Media','62933a2951ef01f4eafd9bdf4d3cd2f0'),('nop:','nop',1,21,0,0,1,21,0,'setup3','732cefcf28bf4547540609fb1a786a30'),('nop:','nop',1,3,0,0,1,3,0,'users2','86425c35a33507d479f71ade53a669aa'),('eznode:5','eznode',1,2,0,1,3,2,0,'Users','9bc65c2abec141778ffaa729489f3e87'),('eznode:2','eznode',1,1,0,1,2,1,0,'','d41d8cd98f00b204e9800998ecf8427e'),('eznode:14','eznode',1,6,0,1,3,6,2,'Editors','a147e136bfa717592f2bd70bd4b53b17'),('eznode:44','eznode',1,10,0,1,3,10,2,'Anonymous-Users','c2803c3fa1b0b5423237b4e018cae755'),('eznode:12','eznode',1,4,0,1,3,4,2,'Guest-accounts','e57843d836e3af8ab611fde9e2139b3a'),('eznode:13','eznode',1,5,0,1,3,5,2,'Administrator-users','f89fad7f8a3abc8c09e1deb46a420007'),('nop:','nop',1,11,0,0,1,11,3,'anonymous_users2','505e93077a6dde9034ad97a14ab022b1'),('eznode:12','eznode',1,26,0,0,1,4,3,'guest_accounts','70bb992820e73638731aa8de79b3329e'),('eznode:14','eznode',1,29,0,0,1,6,3,'editors','a147e136bfa717592f2bd70bd4b53b17'),('nop:','nop',1,7,0,0,1,7,3,'administrator_users2','a7da338c20bf65f9f789c87296379c2a'),('eznode:13','eznode',1,27,0,0,1,5,3,'administrator_users','aeb8609aa933b0899aa012c71139c58c'),('eznode:44','eznode',1,30,0,0,1,10,3,'anonymous_users','e9e5ad0c05ee1a43715572e5cc545926'),('eznode:15','eznode',1,8,0,1,3,8,5,'Administrator-User','5a9d7b0ec93173ef4fedee023209cb61'),('eznode:15','eznode',1,28,0,0,0,8,7,'administrator_user','a3cca2de936df1e2f805710399989971'),('eznode:53','eznode',1,20,0,1,3,20,9,'Multimedia','2e5bc8831f7ae6a29530e7f1bbf2de9c'),('eznode:52','eznode',1,19,0,1,3,19,9,'Files','45b963397aa40d4a0063e0d85e4fe7a1'),('eznode:61','eznode',0,50,0,1,2,50,9,'Call-to-action-links','577aebdd436237c329b566aa3f1dcb2a'),('eznode:51','eznode',1,18,0,1,3,18,9,'Images','59b514174bffe4ae402b3d63aad79fe0'),('eznode:59','eznode',0,45,0,1,2,45,9,'Youtube-videos','623c1620e4e8feae3e4e489e8dd17c53'),('eznode:45','eznode',1,12,0,1,3,12,10,'Anonymous-User','ccb62ebca03a31272430bc414bd5cd5b'),('eznode:45','eznode',1,31,0,0,1,12,11,'anonymous_user','c593ec85293ecb0e02d50d4c5c6c20eb'),('nop:','nop',1,15,0,0,1,15,14,'images','59b514174bffe4ae402b3d63aad79fe0'),('eznode:53','eznode',1,34,0,0,1,20,17,'multimedia','2e5bc8831f7ae6a29530e7f1bbf2de9c'),('eznode:52','eznode',1,33,0,0,1,19,17,'files','45b963397aa40d4a0063e0d85e4fe7a1'),('eznode:51','eznode',1,32,0,0,1,18,17,'images','59b514174bffe4ae402b3d63aad79fe0'),('eznode:60','eznode',0,48,0,1,2,48,45,'Inside-Com-to-code-normal-width','4cb91eb81b3e809313184b2e7d376ab4'),('eznode:58','eznode',1,46,0,1,2,46,45,'Inside-Com-to-code','77191fb8de3dece7e6785e30e435c9eb'),('eznode:60','eznode',0,49,0,0,2,48,45,'Inside-Com-to-code2','c677ae0cca5e8f99b2948af9d9a4f3fb'),('eznode:62','eznode',0,51,0,1,2,51,50,'Simple-link-example','01dd6a6c628437bd79f7f894b42a8926'),('eznode:63','eznode',0,53,0,0,2,52,50,'Orange-button-example','6c975b91f915eab4ff593886a659cc83'),('eznode:63','eznode',0,52,0,1,2,52,50,'Primary-color-button-example','901c9648aa7573b045e94b7aea088515'),('eznode:64','eznode',0,54,0,1,2,54,50,'Secondary-color-button-example','b2f0996e269b014a01d96cccd1f28f62'),('eznode:65','eznode',0,55,0,1,2,55,50,'Black-button-example','c6cd596a86aabbe7492fef6598775e0b'),('eznode:66','eznode',0,56,0,1,2,56,50,'White-button-example','f0af76abcb3c621b815a970b020503eb');
/*!40000 ALTER TABLE `ezurlalias_ml` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezurlalias_ml_incr`
--

DROP TABLE IF EXISTS `ezurlalias_ml_incr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezurlalias_ml_incr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezurlalias_ml_incr`
--

LOCK TABLES `ezurlalias_ml_incr` WRITE;
/*!40000 ALTER TABLE `ezurlalias_ml_incr` DISABLE KEYS */;
INSERT INTO `ezurlalias_ml_incr` VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(24),(25),(26),(27),(28),(29),(30),(31),(32),(33),(34),(35),(36),(37),(38),(39),(40),(41),(42),(43),(44),(45),(46),(47),(48),(49),(50),(51),(52),(53),(54),(55),(56);
/*!40000 ALTER TABLE `ezurlalias_ml_incr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezurlwildcard`
--

DROP TABLE IF EXISTS `ezurlwildcard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezurlwildcard` (
  `destination_url` longtext NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_url` longtext NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezurlwildcard`
--

LOCK TABLES `ezurlwildcard` WRITE;
/*!40000 ALTER TABLE `ezurlwildcard` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezurlwildcard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezuser`
--

DROP TABLE IF EXISTS `ezuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezuser` (
  `contentobject_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(150) NOT NULL DEFAULT '',
  `login` varchar(150) NOT NULL DEFAULT '',
  `password_hash` varchar(255) DEFAULT NULL,
  `password_hash_type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`contentobject_id`),
  UNIQUE KEY `ezuser_login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezuser`
--

LOCK TABLES `ezuser` WRITE;
/*!40000 ALTER TABLE `ezuser` DISABLE KEYS */;
INSERT INTO `ezuser` VALUES (10,'nospam@ez.no','anonymous','$2y$10$35gOSQs6JK4u4whyERaeUuVeQBi2TUBIZIfP7HEj7sfz.MxvTuOeC',7),(14,'nospam@ez.no','admin','$2y$10$FDn9NPwzhq85cLLxfD5Wu.L3SL3Z/LNCvhkltJUV0wcJj7ciJg2oy',7);
/*!40000 ALTER TABLE `ezuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezuser_accountkey`
--

DROP TABLE IF EXISTS `ezuser_accountkey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezuser_accountkey` (
  `hash_key` varchar(32) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `hash_key` (`hash_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezuser_accountkey`
--

LOCK TABLES `ezuser_accountkey` WRITE;
/*!40000 ALTER TABLE `ezuser_accountkey` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezuser_accountkey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezuser_discountrule`
--

DROP TABLE IF EXISTS `ezuser_discountrule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezuser_discountrule` (
  `contentobject_id` int(11) DEFAULT NULL,
  `discountrule_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezuser_discountrule`
--

LOCK TABLES `ezuser_discountrule` WRITE;
/*!40000 ALTER TABLE `ezuser_discountrule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezuser_discountrule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezuser_role`
--

DROP TABLE IF EXISTS `ezuser_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezuser_role` (
  `contentobject_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `limit_identifier` varchar(255) DEFAULT '',
  `limit_value` varchar(255) DEFAULT '',
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezuser_role_contentobject_id` (`contentobject_id`),
  KEY `ezuser_role_role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezuser_role`
--

LOCK TABLES `ezuser_role` WRITE;
/*!40000 ALTER TABLE `ezuser_role` DISABLE KEYS */;
INSERT INTO `ezuser_role` VALUES (13,32,'Subtree','/1/2/',3),(13,33,'Subtree','/1/43/',3),(13,35,'','',4),(12,38,'','',2),(11,39,'','',1),(42,40,'','',1);
/*!40000 ALTER TABLE `ezuser_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezuser_setting`
--

DROP TABLE IF EXISTS `ezuser_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezuser_setting` (
  `is_enabled` int(11) NOT NULL DEFAULT '0',
  `max_login` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezuser_setting`
--

LOCK TABLES `ezuser_setting` WRITE;
/*!40000 ALTER TABLE `ezuser_setting` DISABLE KEYS */;
INSERT INTO `ezuser_setting` VALUES (1,1000,10),(1,10,14);
/*!40000 ALTER TABLE `ezuser_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezuservisit`
--

DROP TABLE IF EXISTS `ezuservisit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezuservisit` (
  `current_visit_timestamp` int(11) NOT NULL DEFAULT '0',
  `failed_login_attempts` int(11) NOT NULL DEFAULT '0',
  `last_visit_timestamp` int(11) NOT NULL DEFAULT '0',
  `login_count` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  KEY `ezuservisit_co_visit_count` (`current_visit_timestamp`,`login_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezuservisit`
--

LOCK TABLES `ezuservisit` WRITE;
/*!40000 ALTER TABLE `ezuservisit` DISABLE KEYS */;
INSERT INTO `ezuservisit` VALUES (1301057720,0,1301057720,0,14);
/*!40000 ALTER TABLE `ezuservisit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezvatrule`
--

DROP TABLE IF EXISTS `ezvatrule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezvatrule` (
  `country_code` varchar(255) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vat_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezvatrule`
--

LOCK TABLES `ezvatrule` WRITE;
/*!40000 ALTER TABLE `ezvatrule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezvatrule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezvatrule_product_category`
--

DROP TABLE IF EXISTS `ezvatrule_product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezvatrule_product_category` (
  `product_category_id` int(11) NOT NULL DEFAULT '0',
  `vatrule_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`vatrule_id`,`product_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezvatrule_product_category`
--

LOCK TABLES `ezvatrule_product_category` WRITE;
/*!40000 ALTER TABLE `ezvatrule_product_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezvatrule_product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezvattype`
--

DROP TABLE IF EXISTS `ezvattype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezvattype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `percentage` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezvattype`
--

LOCK TABLES `ezvattype` WRITE;
/*!40000 ALTER TABLE `ezvattype` DISABLE KEYS */;
INSERT INTO `ezvattype` VALUES (1,'Std',0);
/*!40000 ALTER TABLE `ezvattype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezview_counter`
--

DROP TABLE IF EXISTS `ezview_counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezview_counter` (
  `count` int(11) NOT NULL DEFAULT '0',
  `node_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezview_counter`
--

LOCK TABLES `ezview_counter` WRITE;
/*!40000 ALTER TABLE `ezview_counter` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezview_counter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezwaituntildatevalue`
--

DROP TABLE IF EXISTS `ezwaituntildatevalue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezwaituntildatevalue` (
  `contentclass_attribute_id` int(11) NOT NULL DEFAULT '0',
  `contentclass_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_event_id` int(11) NOT NULL DEFAULT '0',
  `workflow_event_version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`workflow_event_id`,`workflow_event_version`),
  KEY `ezwaituntildateevalue_wf_ev_id_wf_ver` (`workflow_event_id`,`workflow_event_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezwaituntildatevalue`
--

LOCK TABLES `ezwaituntildatevalue` WRITE;
/*!40000 ALTER TABLE `ezwaituntildatevalue` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezwaituntildatevalue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezwishlist`
--

DROP TABLE IF EXISTS `ezwishlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezwishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productcollection_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezwishlist`
--

LOCK TABLES `ezwishlist` WRITE;
/*!40000 ALTER TABLE `ezwishlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezwishlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezworkflow`
--

DROP TABLE IF EXISTS `ezworkflow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezworkflow` (
  `created` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_enabled` int(11) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  `modifier_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `version` int(11) NOT NULL DEFAULT '0',
  `workflow_type_string` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezworkflow`
--

LOCK TABLES `ezworkflow` WRITE;
/*!40000 ALTER TABLE `ezworkflow` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezworkflow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezworkflow_assign`
--

DROP TABLE IF EXISTS `ezworkflow_assign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezworkflow_assign` (
  `access_type` int(11) NOT NULL DEFAULT '0',
  `as_tree` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_id` int(11) NOT NULL DEFAULT '0',
  `workflow_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezworkflow_assign`
--

LOCK TABLES `ezworkflow_assign` WRITE;
/*!40000 ALTER TABLE `ezworkflow_assign` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezworkflow_assign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezworkflow_event`
--

DROP TABLE IF EXISTS `ezworkflow_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezworkflow_event` (
  `data_int1` int(11) DEFAULT NULL,
  `data_int2` int(11) DEFAULT NULL,
  `data_int3` int(11) DEFAULT NULL,
  `data_int4` int(11) DEFAULT NULL,
  `data_text1` varchar(255) DEFAULT NULL,
  `data_text2` varchar(255) DEFAULT NULL,
  `data_text3` varchar(255) DEFAULT NULL,
  `data_text4` varchar(255) DEFAULT NULL,
  `data_text5` longtext,
  `description` varchar(50) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `placement` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  `workflow_id` int(11) NOT NULL DEFAULT '0',
  `workflow_type_string` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`version`),
  KEY `wid_version_placement` (`workflow_id`,`version`,`placement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezworkflow_event`
--

LOCK TABLES `ezworkflow_event` WRITE;
/*!40000 ALTER TABLE `ezworkflow_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezworkflow_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezworkflow_group`
--

DROP TABLE IF EXISTS `ezworkflow_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezworkflow_group` (
  `created` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modified` int(11) NOT NULL DEFAULT '0',
  `modifier_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezworkflow_group`
--

LOCK TABLES `ezworkflow_group` WRITE;
/*!40000 ALTER TABLE `ezworkflow_group` DISABLE KEYS */;
INSERT INTO `ezworkflow_group` VALUES (1024392098,14,1,1024392098,14,'Standard');
/*!40000 ALTER TABLE `ezworkflow_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezworkflow_group_link`
--

DROP TABLE IF EXISTS `ezworkflow_group_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezworkflow_group_link` (
  `group_id` int(11) NOT NULL DEFAULT '0',
  `group_name` varchar(255) DEFAULT NULL,
  `workflow_id` int(11) NOT NULL DEFAULT '0',
  `workflow_version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`workflow_id`,`group_id`,`workflow_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezworkflow_group_link`
--

LOCK TABLES `ezworkflow_group_link` WRITE;
/*!40000 ALTER TABLE `ezworkflow_group_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezworkflow_group_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezworkflow_process`
--

DROP TABLE IF EXISTS `ezworkflow_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezworkflow_process` (
  `activation_date` int(11) DEFAULT NULL,
  `content_id` int(11) NOT NULL DEFAULT '0',
  `content_version` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `event_id` int(11) NOT NULL DEFAULT '0',
  `event_position` int(11) NOT NULL DEFAULT '0',
  `event_state` int(11) DEFAULT NULL,
  `event_status` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last_event_id` int(11) NOT NULL DEFAULT '0',
  `last_event_position` int(11) NOT NULL DEFAULT '0',
  `last_event_status` int(11) NOT NULL DEFAULT '0',
  `memento_key` varchar(32) DEFAULT NULL,
  `modified` int(11) NOT NULL DEFAULT '0',
  `node_id` int(11) NOT NULL DEFAULT '0',
  `parameters` longtext,
  `process_key` varchar(32) NOT NULL DEFAULT '',
  `session_key` varchar(32) NOT NULL DEFAULT '0',
  `status` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `workflow_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezworkflow_process_process_key` (`process_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezworkflow_process`
--

LOCK TABLES `ezworkflow_process` WRITE;
/*!40000 ALTER TABLE `ezworkflow_process` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezworkflow_process` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-21 14:46:04
